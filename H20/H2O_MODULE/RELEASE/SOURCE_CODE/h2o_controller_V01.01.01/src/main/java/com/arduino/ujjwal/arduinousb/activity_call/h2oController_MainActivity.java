package com.arduino.ujjwal.arduinousb.activity_call;



import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;

import com.arduino.ujjwal.arduinousb.h2o_controller;
import com.srishti.shsfw_v00_01_01.SHSFW_FrameWorkActivity;
import com.srishti.shsfw_v00_01_01.interfaces.SHSFW_InputInterface;
import com.srishti.shsfw_v00_01_01.interfaces.SHSFW_OutputInterface;
import com.srishti.shsfw_v00_01_01.util.SHSFW_OperationMap.SHSFW_Operation;
import com.srishti.shsfw_v00_01_01.util.SHSFW_PrimaryState;
import com.srishti.sprn.SPRN_Attributes;
import com.srishti.sprn.SPRN_Print;
import com.srishtiesdm.sconnect_mc.S_CuC;

import java.util.ArrayList;


public class h2oController_MainActivity extends SHSFW_FrameWorkActivity implements SHSFW_InputInterface, SHSFW_OutputInterface {

	//*********************************Global variables************************************************//

    //****microcontroller connect******//
	public static S_CuC scuc = null; //Create a empty object of connect microcontroller library
	private int h2o_mc_product_id=29987; //Arduino nano
	private int h2o_vendor_id=6790;      //Arduino nano
	private boolean h2o_init_on_atach=false;
	private String h2o_msg_break_char = "\r";
	private int h2o_msg_in_buffer_size=20;
	//**********************************//


	//***********Printer**************//
	//Printer
	public static SPRN_Print h2o_print =null;   //Printer object
	static SPRN_Attributes stp;          //to hold printer attributes
	static ArrayList<SPRN_Attributes> h2o_print_AL;  //Aray list to hold printer attaributes
	public static int sprn_printer_state=-10;            //State of printer after trying to initialise
	//********************************//

	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.d("d_act_lifecycle", "oncreate");

		SHSFW_PrimaryState.getInstance(this).setSHSFW_state(SHSFW_PrimaryState.SHSFW_PrimaryStates.SECONDARY);
		super.onCreate(savedInstanceState);

		SHSFW_configureCallback(this);
		parent.setBackgroundColor(Color.LTGRAY);
		header.setBackgroundColor(Color.BLUE);


		SHSFW_configureMessages("", "H2O_Controller", "", 2);
		SHSFW_configureHotKey(SHSFW_Operation.HOME, true);
		SHSFW_configureHotKey(SHSFW_Operation.LOCK, true);
		SHSFW_configureHotKey(SHSFW_Operation.CUSTOM, true);
		SHSFW_configureHotKey(SHSFW_Operation.PRIMARY, true);
		SHSFW_configureHotKey(SHSFW_Operation.LOGOUT, false);
		SHSFW_configureHotKey(SHSFW_Operation.LOGIN, false);
		SHSFW_configureHotKey(SHSFW_Operation.MORE, true);
		SHSFW_configureHotKey(SHSFW_Operation.REPORT, false);
		SHSFW_configureHotKey(SHSFW_Operation.SECONDARY, true);

		init_sequence();  //Initialise all hard ware //Currently only printer , arduino initialised in hardfware check module
		//Get the instance of scuc
		scuc = S_CuC.SCuC_getInstance(getApplicationContext());//get h2o instance
//getFragmentMangager().findFragmentByTag(FRAGMENT_NAME); use for calling old fragment
		h2o_controller cal_h2o_controller = new h2o_controller();
		fragManager.beginTransaction().addToBackStack("h2o_controller").replace(child.getId(), cal_h2o_controller, "").commit();

	}


	@Override
	public void SHSFW_onClick(SHSFW_Operation SHSFW_opType) {

		if (SHSFW_opType == SHSFW_Operation.REPORT)
		{
			// do something
		}
		else if (SHSFW_opType == SHSFW_Operation.CUSTOM)
		{
			init_sequence();

		}
		else if (SHSFW_opType == SHSFW_Operation.PRIMARY) {
			// do something
		}
	}

	@Override
	public void SHSFW_IConfigMessages(Object SHSFW_msg1, Object SHSFW_msg2,
									  Object SHSFW_msg3, int SHSFW_fontColor) {
		SHSFW_configureMessages(SHSFW_msg1, SHSFW_msg2, SHSFW_msg3, SHSFW_fontColor);
	}

	@Override
	public void SHSFW_IConfigHotKey(SHSFW_Operation SHSFW_hotKey,
									boolean SHSFW_isEnabled) {
		SHSFW_configureHotKey(SHSFW_hotKey, SHSFW_isEnabled);
	}

	@Override
	public void SHSFW_IConfigLogo(int SHSFW_logo, int SHSFW_logoBGColor) {
		SHSFW_configureLogo(SHSFW_logo, SHSFW_logoBGColor);
	}

	@Override
	public void SHSFW_IConfigureCustomHotKeyIcon(int SHSFW_iconID) {
		SHSFW_configureCustomHotKeyIcon(SHSFW_iconID);
	}


	@Override
	public void onPause() {
		super.onPause();
		Log.d("d_act_lifecycle", "onpause");


	}

	@Override
	public void onResume() {
		super.onResume();
		Log.d("d_act_lifecycle", "onresume");
	}

	@Override
	public void onStop() {
		super.onStop();
		Log.d("d_act_lifecycle", "onstop");
	}

	@Override
	public void onRestart() {
		super.onRestart();
		Log.d("d_act_lifecycle", "onrestart");

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.d("d_act_lifecycle", "ondestroy");
	}

//***************H2o_CB initialisation****************************************************************************//

	private void h2o_initialise_arduino()
	{
		new Thread(new Runnable()
		{
			public void run()
			{

				//Get the instance of scuc
				scuc = S_CuC.SCuC_getInstance(getApplicationContext());
				//Pre initialise
				scuc.SCuC_preInitialise(h2o_mc_product_id, h2o_vendor_id, h2o_init_on_atach, h2o_msg_break_char, h2o_msg_in_buffer_size);
				//Initialise the micro controller
				int temp_stre_stat = scuc.SCuC_initialise();
			}
		}).start();


	}
//***************************************************************************************************************//
	//*******************************printer_init*****************************************************************//
	private void init_thrmal_printer()
	{

		//Get the instance
		h2o_print=SPRN_Print.SPRN_getInstance(this);


		//THread is mandatory to iinitialise
		new Thread(new Runnable() {
			@Override
			public void run()
			{
				try {
					Thread.sleep(7000); //Wait for 10 sec
				}catch(Exception e){}
				//create object of printer initialistaion
				int h2o_printer_resp = h2o_print.SPRN_initializePrinter();

				if(h2o_printer_resp == 0)
				{
					sprn_printer_state=0;

				}
				else if(h2o_printer_resp == -1 )
				{
					sprn_printer_state=-1;
				}
				else
				{
					sprn_printer_state=-2;
				}

			}
		}).start();

	}

	//To print on printer
	public static void send_to_printer(String msg)
	{
		String temp_to_p= "\n"+"SRISHTI ESDM PVT LTD"+
				          "\n"+"\n"+"H2O Distribution and monitor"+
				          "\n"+"\n"+"S/No"+"   "+"Litres"+"   "+"Cost(Rs)"+
				          "\n"+"\n"+msg+"\n"+"\n"+"\n"+"\n";
		stp = new SPRN_Attributes(2,temp_to_p,null,null,0,false,Color.BLACK,0,10,10);
		h2o_print_AL = new ArrayList<SPRN_Attributes>();
		h2o_print_AL.add(stp);

		h2o_print.SPRN_printText(h2o_print_AL);

	}
//*************************************Initialisation sequentior*******************************************************//
	private void init_sequence()
	{
		//Initialise micro controller
		h2o_initialise_arduino();

		//Initialise printer
		init_thrmal_printer();
	}
//***********************************************************************************************************************//



}








