package com.arduino.ujjwal.arduinousb;


import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;



public class h2o_con_fragment_litre_num extends Fragment
{
    private String litres_flown1="";
    private String litres_flown2="";
    private String litres_flown3="";
    private String litres_flown4="";
    private String color_tap1_num_dis="#0099ff";
    private String color_tap2_num_dis="#0099ff";
    private String color_tap3_num_dis="#0099ff";
    private String color_tap4_num_dis="#0099ff";
    private Float h2o_screen_factor;


    public h2o_con_fragment_litre_num()
    {

    }



    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        ViewGroup numberContainer = (ViewGroup) getActivity().findViewById(R.id.f_fragment_ltext);
        if( numberContainer != null)
        {
            numberContainer.removeAllViews();
        }

    }
    @Override
    public void onStop()
    {
        super.onStop();
        return;
    }
    @Override
    public void onDestroyView()
    {
        super.onDestroyView();
        //Remove all views
      /* ViewGroup numberContainer = (ViewGroup) getActivity().findViewById(R.id.f_fragment_ltext);
        if( numberContainer != null)
        {
            numberContainer.removeAllViews();
        }*/
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
         litres_flown1 = this.getArguments().getString("litres_flown_tap1");
        litres_flown2 = this.getArguments().getString("litres_flown_tap2");
        litres_flown3 = this.getArguments().getString("litres_flown_tap3");
        litres_flown4 = this.getArguments().getString("litres_flown_tap4");
        color_tap1_num_dis=this.getArguments().getString("tap1_col");
        color_tap2_num_dis=this.getArguments().getString("tap2_col");
        color_tap3_num_dis=this.getArguments().getString("tap3_col");
        color_tap4_num_dis=this.getArguments().getString("tap4_col");
        h2o_screen_factor=this.getArguments().getFloat("h2o_screen_factor");
        return num_litre_updater();
    }

    private View num_litre_updater()
    {
        RelativeLayout num_litre_update = new RelativeLayout(getActivity());
        RelativeLayout.LayoutParams num_litre_update_lr = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,RelativeLayout.LayoutParams.WRAP_CONTENT);
        num_litre_update.setLayoutParams(num_litre_update_lr);

        RelativeLayout.LayoutParams t_litre_flown_lr1 = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        RelativeLayout.LayoutParams t_litre_flown_lr2 = new   RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        RelativeLayout.LayoutParams t_litre_flown_lr3 = new  RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        RelativeLayout.LayoutParams t_litre_flown_lr4 = new  RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        //t_litre flown 1
        TextView t_litre_flown1 = new TextView(getActivity());
        //  t_litre_flown_lr1.setMargins(dpx(5),dpx(10),0,0);
        t_litre_flown1.setId(R.id.t_litre_flown1);
        t_litre_flown1.setTextSize(spx(25));
        t_litre_flown1.setTypeface(null, Typeface.BOLD);        // for Bold only
        t_litre_flown1.setText(litres_flown1);
        t_litre_flown1.setTextColor(Color.parseColor( color_tap1_num_dis));
        t_litre_flown1.setLayoutParams(t_litre_flown_lr1);

        //t_litre flown 2
        t_litre_flown_lr2.addRule(RelativeLayout.RIGHT_OF,R.id.t_litre_flown1);
        t_litre_flown_lr2.setMargins(dpx(50),0,0,0);
        TextView t_litre_flown2 = new TextView(getActivity());
        t_litre_flown2.setId(R.id.t_litre_flown2);
        t_litre_flown2.setTypeface(null, Typeface.BOLD);        // for Bold only
        t_litre_flown2.setTextSize(spx(25));
        t_litre_flown2.setText(litres_flown2);
        t_litre_flown2.setTextColor(Color.parseColor( color_tap2_num_dis));
        t_litre_flown2.setLayoutParams(t_litre_flown_lr2);

        //t_litre flown 3
        t_litre_flown_lr3.addRule(RelativeLayout.RIGHT_OF,R.id.t_litre_flown2);
        t_litre_flown_lr3.setMargins(dpx(50),0,0,0);
        TextView t_litre_flown3 = new TextView(getActivity());
        t_litre_flown3.setId(R.id.t_litre_flown3);
        t_litre_flown3.setTypeface(null, Typeface.BOLD);        // for Bold only
        t_litre_flown3.setTextSize(spx(25));
        t_litre_flown3.setText(litres_flown3);
        t_litre_flown3.setTextColor(Color.parseColor( color_tap3_num_dis));
        t_litre_flown3.setLayoutParams(t_litre_flown_lr3);

        //t_litre flown 4
        t_litre_flown_lr4.addRule(RelativeLayout.RIGHT_OF,R.id.t_litre_flown3);
        t_litre_flown_lr4.setMargins(dpx(55),0,0,0);
        TextView t_litre_flown4 = new TextView(getActivity());
        t_litre_flown4.setId(R.id.t_litre_flown4);
        t_litre_flown4.setTypeface(null, Typeface.BOLD);        // for Bold only
        t_litre_flown4.setTextSize(spx(25));
        t_litre_flown4.setText(litres_flown4);
        t_litre_flown4.setTextColor(Color.parseColor( color_tap4_num_dis));
        t_litre_flown4.setLayoutParams(t_litre_flown_lr4);

        num_litre_update.addView(t_litre_flown1);
        num_litre_update.addView(t_litre_flown2);
        num_litre_update.addView(t_litre_flown3);
        num_litre_update.addView(t_litre_flown4);

        return(num_litre_update);


    }

    //dpi to px converter
    private int dpx(int dp)
    {
        float px=0;
        px=(float) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, getResources().getDisplayMetrics());
        px=px*(float)h2o_screen_factor;
        return((int)px);
    }

    //sp to px converter
    private int spx(int sp)
    {
        float px=0;
        px=(float)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_FRACTION, sp, getResources().getDisplayMetrics());
        px=px*((float)0.5*h2o_screen_factor);
        return((int)px);
    }

}
