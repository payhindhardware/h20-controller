package com.arduino.ujjwal.arduinousb;


import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import org.achartengine.ChartFactory;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;
import java.text.DecimalFormat;


public class h2o_con_fragment_line extends android.support.v4.app.Fragment
{


     private XYSeries[] h2o_flown_tap ;
     private XYSeriesRenderer[] renderer_line ;
     private View line_graph; //Main view
     private XYMultipleSeriesRenderer multirenderer; //Main renderer
     private XYMultipleSeriesDataset msd; //Main data set
     private float y_axis_max=0;
     private int lg_linechart_timer_counter=0;
     private float[][] lg_h2o_flown_litre_time;
     private float[] lg_h2o_up_time;
     private int lg_no_of_taps;
     private DecimalFormat lg_precision = new DecimalFormat("0.00");  //To round number to two decimal digit


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

            ViewGroup graphContainer = (ViewGroup) getActivity().findViewById(R.id.f_fragment_graph);
            if( graphContainer != null)
            {
                graphContainer.removeAllViews();
            }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        lg_linechart_timer_counter=this.getArguments().getInt("linechart_timer_counter");
       // lg_h2o_flown_litre_time=this.getArguments().get("h2o_flown_litre_time"); //two dimensional array ,not posible directly

        lg_h2o_up_time=this.getArguments().getFloatArray("h2o_up_time");
        lg_no_of_taps=this.getArguments().getInt("no_of_taps");

        //After getting number of taps
        h2o_flown_tap = new XYSeries[lg_no_of_taps];  //Data series for line graph
        renderer_line = new XYSeriesRenderer[lg_no_of_taps]; //Renderer

        return line_graph() ;
    }

    private View line_graph()
    {
        //Renderer color array
        String[] renderer_color = new String[lg_no_of_taps];
        renderer_color[0] = "#800000";
        renderer_color[1] = "#808000";
        renderer_color[2] = "#008000";
        renderer_color[3] = "#008080";

        //add data to the esries
        for(int temp_tap=0; temp_tap<lg_no_of_taps; temp_tap++) {
            //Create initial dataset
            h2o_flown_tap[temp_tap] = new XYSeries("Tap" + (temp_tap+1));
            for (int temp_litre = 0; temp_litre < lg_linechart_timer_counter; temp_litre++)
            {
               //add to dataset
                h2o_flown_tap[temp_tap].add(lg_h2o_up_time[temp_litre],h2o_controller.h2o_flown_litre_time[temp_tap][temp_litre]);

                if(y_axis_max< h2o_controller.h2o_flown_litre_time[temp_tap][temp_litre]) //Find maximum value to set y axis max
                {
                    y_axis_max=h2o_controller.h2o_flown_litre_time[temp_tap][temp_litre];
                }
            }
        }

        //Create main dataset
        msd = new XYMultipleSeriesDataset();
        msd.clear();
        //add dataset to main data set
        for (int temp = 0; temp < lg_no_of_taps; temp++) {
            msd.addSeries(h2o_flown_tap[temp]);
        }

        //Renderer for line chart
        for (int temp = 0; temp < lg_no_of_taps; temp++)
        {
            renderer_line[temp] = new XYSeriesRenderer();
            renderer_line[temp].setColor(Color.parseColor(renderer_color[temp])); //Change this to renderer color
            renderer_line[temp].setLineWidth(2);
           renderer_line[temp].setDisplayChartValues(true);
            renderer_line[temp].setChartValuesFormat(lg_precision);
        }

        //Multi series renderer
        multirenderer = new XYMultipleSeriesRenderer();
        multirenderer.setChartTitle("Total Water Flown");
        multirenderer.setXTitle("Minutes");
        multirenderer.setYTitle("WaterFlown (L)");

        //Max and min limits
        multirenderer.setYAxisMin(0);
        multirenderer.setXAxisMin(-0.3);
        multirenderer.setBackgroundColor(Color.parseColor("#ffffff"));
        multirenderer.setShowAxes(true);
        multirenderer.setPanEnabled(true, true);
        multirenderer.setDisplayValues(true);
        multirenderer.setLabelsColor(Color.parseColor("#000000"));
        multirenderer.setMarginsColor(Color.parseColor("#ffffff"));
        multirenderer.setZoomEnabled(true,true);
        multirenderer.setZoomButtonsVisible(true);
        multirenderer.setXAxisMax((lg_linechart_timer_counter*1.5));//Dynamic x axis max
        multirenderer.setYAxisMax(y_axis_max*1.5); //Dynamic y axis max

        //add renderer to main renderer
        for (int temp = 0; temp < lg_no_of_taps; temp++) {
            multirenderer.addSeriesRenderer(renderer_line[temp]);//add to main renderer
        }


            line_graph = ChartFactory.getLineChartView(getActivity(), msd, multirenderer);
            return line_graph;

    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();
        //Remove all views
     /*   ViewGroup graphContainer = (ViewGroup) getActivity().findViewById(R.id.f_fragment_graph);
        if( graphContainer != null)
        {
            graphContainer.removeAllViews();
        }*/
    }

    @Override
    public void onStop()
    {
        super.onStop();
        return;
    }




}




