package com.arduino.ujjwal.arduinousb;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.InputType;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.arduino.ujjwal.arduinousb.activity_call.h2oController_MainActivity;
import com.srishti.sprn.SPRN_Constants;

import java.text.DecimalFormat;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Pattern;




public class h2o_controller extends Fragment implements SPRN_Constants
{
    //System constants
    private final static int no_of_taps = 4;      //Constats //Number of taps
    private final double msg_interval = 80;  // time between two msg in ms at start (Count down)
    private final int request_interval_readstatus = 120; //Time between two read status request
    private final int responce_process_interval = 100; //Time between processing of two responces
    private final String[] tap_name = new String[4];  //Tap names , default tap1 , tap2 etc...
    private final float h2o_set_ymax_bar = 50;          //Max y axis value for bar ggraph in litre
    private final float h2o_set_ymax_line = 500;       //Max y axis value for line graph in litre
    private final long linechart_update_time = 60000; //Default 1 min //Update line chart time
    private final long bargraph_update_time=160; //Time to update bar graph


    //Gui
    private Button b_tap1, b_tap2, b_tap3, b_tap4, b_done, b_cancel,b_print_reciept, b_bar_graph, b_line_graph;
    private EditText el;
    private TextView t_tap_no, t_con_status, t_con_tp_status, t_graph_status, t_el_emsg;
    private TextView t_dis_sl, t_dis_pl;
    private boolean ss_flag = false;  //to indiacate start and stop of system
    private boolean[] tap_cancel_pressed = new boolean[no_of_taps];
    private boolean[] tap_sl_pressed = new boolean[no_of_taps];   //To indiacte that done button pressed for timer

    //Colour of number fields
    private String tap1_col = "#0099ff";
    private String tap2_col = "#0099ff";
    private String tap3_col = "#0099ff";
    private String tap4_col = "#0099ff";

    //Massage recieving
    private boolean[] tap_no_req_busy = new boolean[no_of_taps + 1]; //To indicate the busy status of tapno

    //msg process
    int msg_type = 0;    //To pass msg responce to  after execution in this async
    private String[] tokens = new String[20];    //To store the parsed msg chunks
    private boolean update_gui_flag = false;

    //Massage transmission
    private h2o_con_Commands commands = new h2o_con_Commands(); //Object of commands
    private int b_tap_no = 1;  //Tap no according to pressed button
    private float[] h2o_req_litre = new float[no_of_taps]; //Number of litres
    private float[] h2o_flown_litre = new float[no_of_taps]; //Litres flown
    private float[] total_h2o_flown = new float[no_of_taps]; //Total number of h2o flown
    private int rs_counter = 1;  //Counter to send readstatus commands

    //countdown timer
    private CountDownTimer cdt_start;  //Count down timer for start sequence
    private CountDownTimer cdt_stop;    // Count down timer for stop sequence
    private int counter_cdt = 0;                //For looping
    private double total_time_start = 0;    //Toatl time for countdown timers
    private double total_time_stop = 0;     //           ,, ,, ,,

    //Timers
    Timer timer_responceprocess;
    Timer timer_send_commands;
    Timer update_linechart;
    Timer  update_bar_graph;


    //USB communication
    private boolean flag_is_arduino_connected = false;
    private boolean flag_is_arduino_disconnected = false;
    private boolean flag_is_tp_connected = false;
    private boolean flag_is_tp_disconnected = false;


    //Misc
    private DecimalFormat precision = new DecimalFormat("0.00");  //To round number to two decimal digit
    private FrameLayout f_fragment_graph, f_fragment_ltext; // to use in fragment class for graph and litres in text
    private float[] h2o_up_time = new float[1440];          //Time for line graph
    public static float[][] h2o_flown_litre_time = new float[no_of_taps][1440];  //litres flown for line graph //24 hour
    private int linechart_timer_counter = 0;
    private float max_litre_flown = 0;
    private boolean flag_bar_graph_mode = true;
    private boolean flag_line_graph_mode = false;
    private float h2o_screen_factor;   //To hold screen coeficiant
    private boolean one_time_entry = false;
    private long serial_no_h2o_sold = 0;

    long pre_time=0;
    long cur_time=0;

    @Override
    public void onSaveInstanceState(Bundle save_state) {
        super.onSaveInstanceState(save_state);
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d("d_lifecycle", "on start");
    }

    @Override
    public void onResume() {
        super.onResume();
        one_time_entry = true;   //Fragment has been created once
        //Configure general timer
        config_general_timer();    //Configure and start the timers
        //Hide the print reipt button
        b_print_reciept.setEnabled(false);
        Log.d("d_lifecycle", "on resume");
    }

    @Override
    public void onPause() {
        super.onPause();
        flag_is_arduino_connected = false;  //Make these flags false as both are disconnected automatically
        flag_is_tp_connected = false;
        cancel_all_timers();          //Cancell all the timers to prevent ambuiguity to OS
        Log.d("d_lifecycle", "on pause");
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if (!one_time_entry) {  //Initialise only for one and only lifecycle of this fragment.
            //Initilise arrays
            init_array();
            //Initialise flags
            asiotpf_init_flags();
            //Get screen coeficiant(scaling)
            h2o_screen_coe();
        }
        // Create the layout for this fragment
        Log.d("d_lifecycle", "on createview");
        return (asiotpf_main_layout());
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //Keep screen awake
        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        //Initialise gui
        initialise_gui();
        //Configure gui
        configure_gui();
        //configure fragment initially
        config_fragment();
        //Initial gui settings
        disconnect_state_gui();
        //Configire countdown timer
        config_countdown_timer();


        Log.d("d_lifecycle", "on view created");
    }


    //***************************************Main layout section*****************************************************//
    private View asiotpf_main_layout() {
        Log.d("d_lst_act_mod", "asiotpf_main_layout");
        //Main view
        RelativeLayout ml_relativeLayout = new RelativeLayout(getActivity());
        ml_relativeLayout.setBackgroundColor(Color.parseColor("#ffffff"));
        RelativeLayout.LayoutParams ml_size = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT, RelativeLayout.LayoutParams.FILL_PARENT);
        ml_relativeLayout.setId(R.id.ml_relative);
        ml_relativeLayout.setLayoutParams(ml_size);
        //Connection status of H2OCB text field
        RelativeLayout.LayoutParams t_con_lr = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        t_con_lr.setMargins(dpx(13), 0, 0, 0);
        TextView t_con_status = new TextView(getActivity());
        t_con_status.setId(R.id.t_con_status);
        t_con_status.setTextSize(spx(32));
        t_con_status.setTypeface(null, Typeface.BOLD);        // for Bold only
        t_con_status.setText("H2OCB :Disconnected  ");
        t_con_status.setTextColor(Color.parseColor("#ff0000"));
        t_con_status.setLayoutParams(t_con_lr);
        //Connection status of printer
        //Connection status of H2OCB text field
        RelativeLayout.LayoutParams t_con_tp_lr = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        t_con_tp_lr.setMargins(dpx(13), 0, 0, 0);
        t_con_tp_lr.addRule(RelativeLayout.RIGHT_OF,R.id.t_con_status);
        TextView t_con_tp_status = new TextView(getActivity());
        t_con_tp_status.setId(R.id.t_con_tp_status);
        t_con_tp_status.setTextSize(spx(32));
        t_con_tp_status.setTypeface(null, Typeface.BOLD);        // for Bold only
        t_con_tp_status.setText("Printer : Disconnected");
        t_con_tp_status.setTextColor(Color.parseColor("#ff0000"));
        t_con_tp_status.setLayoutParams( t_con_tp_lr);
        //Tap no text field
        RelativeLayout.LayoutParams t_tapno_lr = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        t_tapno_lr.setMargins(dpx(13), 0, 0, 0);
        t_tapno_lr.addRule(RelativeLayout.BELOW, R.id.t_con_status);
        TextView t_tap_no = new TextView(getActivity());
        t_tap_no.setId(R.id.t_tap_no);
        t_tap_no.setTextSize(spx(32));
        t_tap_no.setTypeface(null, Typeface.BOLD);        // for Bold only
        t_tap_no.setText("Tap 1");
        t_tap_no.setTextColor(Color.parseColor("#0000FF"));
        t_tap_no.setLayoutParams(t_tapno_lr);


        //tap1 button
        RelativeLayout.LayoutParams b_tap1_lr = new RelativeLayout.LayoutParams(dpx(75), dpx(40));
        b_tap1_lr.addRule(RelativeLayout.ALIGN_LEFT, R.id.t_con_status);
        b_tap1_lr.addRule(RelativeLayout.BELOW, R.id.t_tap_no);
        b_tap1_lr.setMargins(0, dpx(20), 0, 0);
        Button b_tap1 = new Button(getActivity());
        b_tap1.setText(" TapNo1 ");
        b_tap1.setId(R.id.tap1);
        b_tap1.setTextColor(Color.parseColor("#000000"));
        b_tap1.setTextSize(spx(29));
        b_tap1.setTypeface(null, Typeface.BOLD);        // for Bold only
        b_tap1.setLayoutParams(b_tap1_lr);


        //tap2 buttton
        RelativeLayout.LayoutParams b_tap2_lr = new RelativeLayout.LayoutParams(dpx(75), dpx(40));
        b_tap2_lr.addRule(RelativeLayout.RIGHT_OF, R.id.tap1);
        b_tap2_lr.addRule(RelativeLayout.BELOW, R.id.t_tap_no);
        b_tap2_lr.setMargins(dpx(15), dpx(20), 0, 0);
        Button b_tap2 = new Button(getActivity());
        b_tap2.setText(" TapNo2 ");
        b_tap2.setId(R.id.tap2);
        b_tap2.setTextColor(Color.parseColor("#000000"));
        b_tap2.setTextSize(spx(29));
        b_tap2.setTypeface(null, Typeface.BOLD);        // for Bold only
        b_tap2.setLayoutParams(b_tap2_lr);

        //tap3 button
        RelativeLayout.LayoutParams b_tap3_lr = new RelativeLayout.LayoutParams(dpx(75), dpx(40));
        b_tap3_lr.addRule(RelativeLayout.RIGHT_OF, R.id.tap2);
        b_tap3_lr.addRule(RelativeLayout.BELOW, R.id.t_tap_no);
        b_tap3_lr.setMargins(dpx(15), dpx(20), 0, 0);
        Button b_tap3 = new Button(getActivity());
        b_tap3.setText(" TapNo3 ");
        b_tap3.setId(R.id.tap3);
        b_tap3.setTextColor(Color.parseColor("#000000"));
        b_tap3.setTextSize(spx(29));
        b_tap3.setTypeface(null, Typeface.BOLD);        // for Bold only
        b_tap3.setLayoutParams(b_tap3_lr);
        //tap4 button
        RelativeLayout.LayoutParams b_tap4_lr = new RelativeLayout.LayoutParams(dpx(75), dpx(40));
        b_tap4_lr.addRule(RelativeLayout.RIGHT_OF, R.id.tap3);
        b_tap4_lr.addRule(RelativeLayout.BELOW, R.id.t_tap_no);
        b_tap4_lr.setMargins(dpx(15), dpx(20), 0, 0);
        Button b_tap4 = new Button(getActivity());
        b_tap4.setText(" TapNo4 ");
        b_tap4.setId(R.id.tap4);
        b_tap4.setTextColor(Color.parseColor("#000000"));
        b_tap4.setTextSize(spx(29));
        b_tap4.setTypeface(null, Typeface.BOLD);        // for Bold only
        b_tap4.setLayoutParams(b_tap4_lr);

        //add tap buttons to relative layout
        ml_relativeLayout.addView(b_tap1);
        ml_relativeLayout.addView(b_tap2);
        ml_relativeLayout.addView(b_tap3);
        ml_relativeLayout.addView(b_tap4);

        //After tapno buttons , text view for display set litres and processed litres
        //text before set litre
        RelativeLayout.LayoutParams t_sl_lr = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        t_sl_lr.setMargins(dpx(13), dpx(30), 0, 0);
        t_sl_lr.addRule(RelativeLayout.BELOW, R.id.tap1);
        TextView t_sl = new TextView(getActivity());
        t_sl.setId(R.id.t_sl);
        t_sl.setTextSize(spx(32));
        t_sl.setTypeface(null, Typeface.BOLD);        // for Bold only
        t_sl.setText("Set Litre : ");
        t_sl.setTextColor(Color.parseColor("#0000ff"));
        t_sl.setLayoutParams(t_sl_lr);
        //Set litre dislpay text field
        RelativeLayout.LayoutParams t_dis_sl_lr = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        t_dis_sl_lr.addRule(RelativeLayout.BELOW, R.id.tap1);
        t_dis_sl_lr.addRule(RelativeLayout.RIGHT_OF, R.id.t_sl);
        t_dis_sl_lr.setMargins(dpx(13), dpx(30), 0, 0);
        TextView t_dis_sl = new TextView(getActivity());
        t_dis_sl.setId(R.id.t_dis_sl);
        t_dis_sl.setTextSize(spx(32));
        t_dis_sl.setTypeface(null, Typeface.BOLD);        // for Bold only
        t_dis_sl.setText("0.00");
        t_dis_sl.setTextColor(Color.parseColor("#ff0000"));
        t_dis_sl.setLayoutParams(t_dis_sl_lr);

        //text before processed litre
        RelativeLayout.LayoutParams t_pl_lr = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        t_pl_lr.setMargins(dpx(13), 0, 0, 0);
        t_pl_lr.addRule(RelativeLayout.BELOW, R.id.t_sl);
        TextView t_pl = new TextView(getActivity());
        t_pl.setId(R.id.t_pl);
        t_pl.setTextSize(spx(32));
        t_pl.setTypeface(null, Typeface.BOLD);        // for Bold only
        // t_pl.setText("Flown Litre : "); not used currently
        t_pl.setTextColor(Color.parseColor("#0000ff"));
        t_pl.setLayoutParams(t_pl_lr);
        //processed  litre display text field
        RelativeLayout.LayoutParams t_dis_pl_lr = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        t_dis_pl_lr.addRule(RelativeLayout.BELOW, R.id.t_dis_sl);
        t_dis_pl_lr.addRule(RelativeLayout.RIGHT_OF, R.id.t_pl);
        TextView t_dis_pl = new TextView(getActivity());
        t_dis_pl.setId(R.id.t_dis_pl);
        t_dis_pl.setTextSize(spx(32));
        t_dis_pl.setTypeface(null, Typeface.BOLD);        // for Bold only
        // t_dis_pl.setText("0.00");    //Not used currrently
        t_dis_pl.setTextColor(Color.parseColor("#ff0000"));
        t_dis_pl.setLayoutParams(t_dis_pl_lr);


        //Text to show enter the litres
        RelativeLayout.LayoutParams t_sl_el_lr = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        t_sl_el_lr.addRule(RelativeLayout.BELOW, R.id.t_pl);
        t_sl_el_lr.setMargins(dpx(13), dpx(25), 0, 0);
        TextView t_sl_el = new TextView(getActivity());
        t_sl_el.setText("Enter the litres : ");
        t_sl_el.setTextColor(Color.parseColor("#000000"));
        t_sl_el.setTextSize(spx(32));
        t_sl_el.setTypeface(null, Typeface.BOLD);        // for Bold only
        t_sl_el.setId(R.id.t_sl_el);
        t_sl_el.setLayoutParams(t_sl_el_lr);

        //Edit text
        RelativeLayout.LayoutParams e_el_lr = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        e_el_lr.addRule(RelativeLayout.BELOW, R.id.t_dis_pl);
        e_el_lr.addRule(RelativeLayout.RIGHT_OF, R.id.t_sl_el);
        e_el_lr.addRule(RelativeLayout.ALIGN_TOP, R.id.t_sl_el);
        EditText e_el = new EditText(getActivity());
        e_el.setMinHeight(dpx(1));
        e_el.setId(R.id.el);
        e_el.setHint("Litres");
        e_el.setTextSize(spx(32));
        e_el.setTypeface(null, Typeface.BOLD);        // for Bold only
        e_el.setGravity(View.TEXT_ALIGNMENT_CENTER);
        e_el.setTextColor(Color.parseColor("#000000"));
        e_el.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        e_el.setLayoutParams(e_el_lr);


        //Error text for edit ltre   //Not used currently  //Used toast instead
        RelativeLayout.LayoutParams t_el_emsg_lr = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        t_el_emsg_lr.addRule(RelativeLayout.BELOW, R.id.t_dis_pl);
        t_el_emsg_lr.addRule(RelativeLayout.RIGHT_OF, R.id.el);
        t_el_emsg_lr.setMargins(dpx(10), dpx(32), 0, 0);
        TextView t_el_emsg = new TextView(getActivity());
        t_el_emsg.setText("");
        t_el_emsg.setTextColor(Color.parseColor("#FF0000"));
        t_el_emsg.setTextSize(spx(30));
        t_el_emsg.setTypeface(null, Typeface.BOLD);        // for Bold only
        t_el_emsg.setId(R.id.t_el_emng);
        t_el_emsg.setLayoutParams(t_el_emsg_lr);

        //b_done
        RelativeLayout.LayoutParams b_done_lr = new RelativeLayout.LayoutParams(dpx(120), dpx(45));
        b_done_lr.addRule(RelativeLayout.BELOW, R.id.el);
        b_done_lr.setMargins(dpx(13), 0, 0, 0);
        Button b_done = new Button(getActivity());
        b_done.setText("Send request");
        b_done.setTextColor(Color.parseColor("#0000FF"));
        b_done.setId(R.id.done);
        b_done.setTextSize(spx(29));
        b_done.setTypeface(null, Typeface.BOLD);        // for Bold only
        b_done.setLayoutParams(b_done_lr);

        //b_cancel
        RelativeLayout.LayoutParams b_cancel_lr = new RelativeLayout.LayoutParams(dpx(120), dpx(45));
        b_cancel_lr.addRule(RelativeLayout.BELOW, R.id.el);
        b_cancel_lr.addRule(RelativeLayout.RIGHT_OF, R.id.done);
        b_cancel_lr.setMargins(0, 0, 0, 0);
        Button b_cancel = new Button(getActivity());
        b_cancel.setText("Done   Request");
        b_cancel.setId(R.id.cancel);
        b_cancel.setTextColor(Color.parseColor("#FF0000"));
        b_cancel.setTextSize(spx(29));
        b_cancel.setTypeface(null, Typeface.BOLD);        // for Bold only
        b_cancel.setLayoutParams(b_cancel_lr);

        //b_print_reciept
        RelativeLayout.LayoutParams b_printreciept_lr = new RelativeLayout.LayoutParams(dpx(120), dpx(45));
        b_printreciept_lr.addRule(RelativeLayout.BELOW, R.id.done);
        b_printreciept_lr.setMargins(dpx(13), 0, 0, 0);
        Button b_printreciept = new Button(getActivity());
        b_printreciept.setText("Print  Reciept");
        b_printreciept.setId(R.id.printReciept);
        b_printreciept.setTextColor(Color.parseColor("#000000"));
        b_printreciept.setTextSize(spx(29));
        b_printreciept.setTypeface(null, Typeface.BOLD);        // for Bold only
        b_printreciept.setLayoutParams( b_printreciept_lr);


        //Add text field and edit box to layout
        ml_relativeLayout.addView(t_sl_el);
        ml_relativeLayout.addView(e_el);

        //add done , cance and error msg fields to main layout
        ml_relativeLayout.addView(b_done);
        ml_relativeLayout.addView(b_cancel);
        ml_relativeLayout.addView(b_printreciept);
        ml_relativeLayout.addView(t_el_emsg);


        //Right side of screen
        //Graph type buttons
        //b_bar graph //Current litres flown //Button
        RelativeLayout.LayoutParams b_bar_graph_lr = new RelativeLayout.LayoutParams(dpx(90), dpx(45));
        b_bar_graph_lr.addRule(RelativeLayout.RIGHT_OF, R.id.tap4);
        b_bar_graph_lr.addRule(RelativeLayout.ALIGN_TOP, R.id.t_con_status);
        b_bar_graph_lr.setMargins(dpx(45), dpx(10), 0, 0);
        Button b_bar_graph = new Button(getActivity());
        b_bar_graph.setText("CurrentFlow");
        b_bar_graph.setTextSize(spx(25));
        b_bar_graph.setTypeface(null, Typeface.BOLD);        // for Bold only
        b_bar_graph.setId(R.id.b_bar_graph);
        b_bar_graph.setTextColor(Color.parseColor("#000000"));
        b_bar_graph.setLayoutParams(b_bar_graph_lr);

        //b_line_graph //Total litres flown //Button
        RelativeLayout.LayoutParams b_line_graph_lr = new RelativeLayout.LayoutParams(dpx(90), dpx(45));
        b_line_graph_lr.addRule(RelativeLayout.RIGHT_OF, R.id.b_bar_graph);
        b_bar_graph_lr.addRule(RelativeLayout.ALIGN_TOP, R.id.t_con_status);
        b_line_graph_lr.setMargins(0, dpx(10), 0, 0);
        Button b_line_graph = new Button(getActivity());
        b_line_graph.setText("TotalFlow");
        b_line_graph.setTextSize(spx(25));
        b_line_graph.setTypeface(null, Typeface.BOLD);        // for Bold only
        b_line_graph.setId(R.id.b_line_graph);
        b_line_graph.setTextColor(Color.parseColor("#000000"));
        b_line_graph.setLayoutParams(b_line_graph_lr);

        //T_graph_status //Text field
        RelativeLayout.LayoutParams t_graph_status_lr = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        t_graph_status_lr.addRule(RelativeLayout.RIGHT_OF, R.id.b_line_graph);
        b_bar_graph_lr.addRule(RelativeLayout.ALIGN_TOP, R.id.t_con_status);
        t_graph_status_lr.setMargins(dpx(20), dpx(25), 0, 0);
        TextView t_graph_status = new TextView(getActivity());
        t_graph_status.setText("CurrentFlow");
        t_graph_status.setTextColor(Color.parseColor("#0000FF"));
        t_graph_status.setTextSize(spx(32));
        t_graph_status.setTypeface(null, Typeface.BOLD);        // for Bold only
        t_graph_status.setId(R.id.t_graph_status);
        t_graph_status.setLayoutParams(t_graph_status_lr);

        //Fragment to dislpaly graph
        RelativeLayout.LayoutParams f_fragment_graph_lr = new RelativeLayout.LayoutParams(dpx(320), dpx(200)); //width and height
        f_fragment_graph_lr.addRule(RelativeLayout.RIGHT_OF, R.id.tap4);
        f_fragment_graph_lr.addRule(RelativeLayout.BELOW, R.id.b_bar_graph);
        f_fragment_graph_lr.setMargins(dpx(30), 0, 0, 0);
        f_fragment_graph = new FrameLayout(getActivity());
        f_fragment_graph.setId(R.id.f_fragment_graph);
        f_fragment_graph.setLayoutParams(f_fragment_graph_lr);


        //t_litre_flown
        RelativeLayout.LayoutParams t_litre_flown_lr = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        RelativeLayout.LayoutParams t_litre_flown_tap1_lr = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        RelativeLayout.LayoutParams t_litre_flown_tap2_lr = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        RelativeLayout.LayoutParams t_litre_flown_tap3_lr = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        RelativeLayout.LayoutParams t_litre_flown_tap4_lr = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        //t_litre_flown simple text
        t_litre_flown_lr.addRule(RelativeLayout.BELOW, R.id.f_fragment_graph);

        // t_litre_flown_lr.addRule(RelativeLayout.ALIGN_TOP,R.id.cancel);
        t_litre_flown_lr.addRule(RelativeLayout.RIGHT_OF, R.id.tap4);
        t_litre_flown_lr.setMargins(0, dpx(15), 0, 0);
        TextView t_litre_flown = new TextView(getActivity());
        t_litre_flown.setId(R.id.t_litre_flown);
        t_litre_flown.setTextSize(spx(30));
        t_litre_flown.setTypeface(null, Typeface.BOLD);        // for Bold only
        t_litre_flown.setText("CurrentFlow:   ");
        t_litre_flown.setTextColor(Color.parseColor("#0000FF"));
        t_litre_flown.setLayoutParams(t_litre_flown_lr);

        //tap number1 text
        t_litre_flown_tap1_lr.addRule(RelativeLayout.BELOW, R.id.f_fragment_graph);
        // t_litre_flown_tap1_lr.addRule(RelativeLayout.ALIGN_TOP,R.id.cancel);
        t_litre_flown_tap1_lr.addRule(RelativeLayout.RIGHT_OF, R.id.t_litre_flown);
        t_litre_flown_tap1_lr.setMargins(dpx(5), dpx(15), 0, 0);
        TextView t_litre_flown_tap1 = new TextView(getActivity());
        t_litre_flown_tap1.setId(R.id.t_litre_flown_tap1_text);
        t_litre_flown_tap1.setTextSize(spx(25));
        t_litre_flown_tap1.setText("Tap 1");
        t_litre_flown_tap1.setTextColor(Color.parseColor("#0000FF"));
        t_litre_flown_tap1.setLayoutParams(t_litre_flown_tap1_lr);

        //tap number2 text
        t_litre_flown_tap2_lr.addRule(RelativeLayout.BELOW, R.id.f_fragment_graph);
        // t_litre_flown_tap2_lr.addRule(RelativeLayout.ALIGN_TOP,R.id.cancel);
        t_litre_flown_tap2_lr.addRule(RelativeLayout.RIGHT_OF, R.id.t_litre_flown_tap1_text);
        t_litre_flown_tap2_lr.setMargins(dpx(37), dpx(15), 0, 0);
        TextView t_litre_flown_tap2 = new TextView(getActivity());
        t_litre_flown_tap2.setId(R.id.t_litre_flown_tap2_text);
        t_litre_flown_tap2.setTextSize(spx(25));
        t_litre_flown_tap2.setText("Tap 2");
        t_litre_flown_tap2.setTextColor(Color.parseColor("#0000FF"));
        t_litre_flown_tap2.setLayoutParams(t_litre_flown_tap2_lr);

        //tap number3 text
        //t_litre_flown_tap3_lr.addRule(RelativeLayout.ALIGN_TOP,R.id.cancel);
        t_litre_flown_tap3_lr.addRule(RelativeLayout.BELOW, R.id.f_fragment_graph);
        t_litre_flown_tap3_lr.addRule(RelativeLayout.RIGHT_OF, R.id.t_litre_flown_tap2_text);
        t_litre_flown_tap3_lr.setMargins(dpx(45), dpx(15), 0, 0);
        TextView t_litre_flown_tap3 = new TextView(getActivity());
        t_litre_flown_tap3.setId(R.id.t_litre_flown_tap3_text);
        t_litre_flown_tap3.setTextSize(spx(25));
        t_litre_flown_tap3.setText("Tap 3");
        t_litre_flown_tap3.setTextColor(Color.parseColor("#0000FF"));
        t_litre_flown_tap3.setLayoutParams(t_litre_flown_tap3_lr);

        //tap number4 text
        t_litre_flown_tap4_lr.addRule(RelativeLayout.BELOW, R.id.f_fragment_graph);
        // t_litre_flown_tap4_lr.addRule(RelativeLayout.ALIGN_TOP,R.id.cancel);
        t_litre_flown_tap4_lr.addRule(RelativeLayout.RIGHT_OF, R.id.t_litre_flown_tap3_text);
        t_litre_flown_tap4_lr.setMargins(dpx(45), dpx(15), 0, 0);
        TextView t_litre_flown_tap4 = new TextView(getActivity());
        t_litre_flown_tap4.setId(R.id.t_litre_flown_tap4_text);
        t_litre_flown_tap4.setTextSize(spx(25));
        t_litre_flown_tap4.setText("Tap 4");
        t_litre_flown_tap4.setTextColor(Color.parseColor("#0000FF"));
        t_litre_flown_tap4.setLayoutParams(t_litre_flown_tap4_lr);

        //Fragment to hold litres flown text
        RelativeLayout.LayoutParams f_fragment_ltext_lr = new RelativeLayout.LayoutParams(dpx(320), dpx(30)); //width and height
        f_fragment_ltext_lr.addRule(RelativeLayout.RIGHT_OF, R.id.t_litre_flown);
        f_fragment_ltext_lr.addRule(RelativeLayout.BELOW, R.id.t_litre_flown_tap4_text);
        f_fragment_ltext_lr.setMargins(dpx(5), dpx(10), 0, 0);
        f_fragment_ltext = new FrameLayout(getActivity());
        f_fragment_ltext.setId(R.id.f_fragment_ltext);
        f_fragment_ltext.setLayoutParams(f_fragment_ltext_lr);


        //Add tables and buttons to main layout
        ml_relativeLayout.addView(t_tap_no); //Tap no
        ml_relativeLayout.addView(t_con_status); //Connection status
        ml_relativeLayout.addView(t_con_tp_status); //Add printer status
        ml_relativeLayout.addView(t_sl); //text before set litre field
        ml_relativeLayout.addView(t_dis_sl); //set litre field
        ml_relativeLayout.addView(t_pl); //text before processed litre field
        ml_relativeLayout.addView(t_dis_pl); //processed litre field
        ml_relativeLayout.addView(f_fragment_graph); //Add frame layout\
        ml_relativeLayout.addView(f_fragment_ltext); //Add frame layout of litres in text field to main layout
        ml_relativeLayout.addView(b_bar_graph); //Add bar graph selection button
        ml_relativeLayout.addView(b_line_graph); //Add line graph selection button
        ml_relativeLayout.addView(t_graph_status); //Add graph ststus text view
        ml_relativeLayout.addView(t_litre_flown); //Litre flown tap  field
        ml_relativeLayout.addView(t_litre_flown_tap1); //Tap numbers right side
        ml_relativeLayout.addView(t_litre_flown_tap2);//   ,,
        ml_relativeLayout.addView(t_litre_flown_tap3);//   ,,
        ml_relativeLayout.addView(t_litre_flown_tap4);//  ,,
        return (ml_relativeLayout);  //add relative layout
    }

    //dpi to px converter
    private int dpx(int dp) {
        float px = 0;
        px = (float) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, getResources().getDisplayMetrics());
        px = px * (float) h2o_screen_factor;
        return ((int) px);
    }

    //sp to px converter
    private int spx(int sp) {
        float px = 0;
        px = (float) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_FRACTION, sp, getResources().getDisplayMetrics());
        px = px * ((float) 0.5 * h2o_screen_factor);
        return ((int) px);
    }


    private void h2o_screen_coe() {
        WindowManager windowManager = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
        //float width = (float)windowManager.getDefaultDisplay().getWidth();
        float height = (float) windowManager.getDefaultDisplay().getHeight();

        DisplayMetrics dm = getActivity().getResources().getDisplayMetrics();
        float densityDpi = (float) dm.densityDpi;
        float width_inch = (height / densityDpi);
        h2o_screen_factor = (width_inch / (float) 2.8);

    }

//*************************************************************************************************************//

    //********************************Initialise and configure gui ******************************************************//
    private void initialise_gui() {
        Log.d("d_lst_act_mod", "intialise gui");
        b_tap1 = (Button) getView().findViewById(R.id.tap1); //Tap button 1
        b_tap2 = (Button) getView().findViewById(R.id.tap2); //Tap button 2
        b_tap3 = (Button) getView().findViewById(R.id.tap3); //Tap button 3
        b_tap4 = (Button) getView().findViewById(R.id.tap4);//Tap button 4
        b_done = (Button) getView().findViewById(R.id.done);
        b_cancel = (Button) getView().findViewById(R.id.cancel);
        b_print_reciept=(Button) getView().findViewById(R.id.printReciept);
        b_bar_graph = (Button) getView().findViewById(R.id.b_bar_graph);
        b_line_graph = (Button) getView().findViewById(R.id.b_line_graph);
        el = (EditText) getView().findViewById(R.id.el);
        t_el_emsg = (TextView) getView().findViewById(R.id.t_el_emng);   //Error massage nearr text field
        t_tap_no = (TextView) getView().findViewById(R.id.t_tap_no);
        t_con_status = (TextView) getView().findViewById(R.id.t_con_status);
        t_con_tp_status=(TextView) getView().findViewById(R.id.t_con_tp_status);
        t_dis_sl = (TextView) getView().findViewById(R.id.t_dis_sl);
        t_dis_pl = (TextView) getView().findViewById(R.id.t_dis_pl);
        t_graph_status = (TextView) getView().findViewById(R.id.t_graph_status);

    }

    private void configure_gui() {
        Log.d("d_lst_act_mod", "configure_gui");
        //buttons
        View.OnClickListener buttons = new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (v.getId() == R.id.tap1) {
                    t_tap_no.setText("Tap1");   //tap1  button
                    b_tap_no = 1;
                    b_tap1.setEnabled(false);  //Button enable/disable
                    b_tap2.setEnabled(true);   //after selecting
                    b_tap3.setEnabled(true);
                    b_tap4.setEnabled(true);
                    en_dis_done_canc();      //enable disable done and cancel buttons
                } else if (v.getId() == R.id.tap2) {

                    t_tap_no.setText("Tap2");   //tap2  button
                    b_tap_no = 2;
                    b_tap1.setEnabled(true);
                    b_tap2.setEnabled(false);
                    b_tap3.setEnabled(true);
                    b_tap4.setEnabled(true);
                    en_dis_done_canc();      //enable disable done and cancel buttons
                } else if (v.getId() == R.id.tap3) {   //tap3 button
                    t_tap_no.setText("Tap3");
                    b_tap_no = 3;
                    b_tap1.setEnabled(true);
                    b_tap2.setEnabled(true);
                    b_tap3.setEnabled(false);
                    b_tap4.setEnabled(true);
                    en_dis_done_canc();      //enable disable done and cancel buttons
                } else if (v.getId() == R.id.tap4) {   //tap4 button
                    t_tap_no.setText("Tap4");
                    b_tap_no = 4;
                    b_tap1.setEnabled(true);
                    b_tap2.setEnabled(true);
                    b_tap3.setEnabled(true);
                    b_tap4.setEnabled(false);
                    en_dis_done_canc();      //enable disable done and cancel buttons


                } else if (v.getId() == R.id.done) {   //done button
                    String temp_el_in = el.getText().toString();  //timer will take care of sl command
                    if (TextUtils.isEmpty(temp_el_in)) {
                       // t_el_emsg.setText("Enter litre between 0.1 to 999.99");
                        display_toast("Enter litre between 0.1 to 999.99");
                    } else if (Float.parseFloat(temp_el_in) >= 0.1 && Float.parseFloat(temp_el_in) <= 999.99) {
                      //  t_el_emsg.setText("");
                        total_h2o_flown[b_tap_no - 1] = total_h2o_flown[b_tap_no - 1] + h2o_flown_litre[b_tap_no - 1];
                        h2o_req_litre[b_tap_no - 1] = Float.parseFloat(temp_el_in);
                        tap_sl_pressed[b_tap_no - 1] = true;
                        //Set colour of bar
                        //  h2o_con_fragment_bar.multirenderer.getSeriesRendererAt(b_tap_no - 1).setGradientStop(h2o_req_litre[b_tap_no - 1] + (h2o_req_litre[b_tap_no - 1] * 0.6), Color.parseColor("#FF0000"));
                        tap_no_req_busy[b_tap_no] = true;
                    } else {
                       // t_el_emsg.setText("Enter litre between 0.1 to 999.99");
                        display_toast("Enter litre between 0.1 to 999.99");
                    }
                    en_dis_done_canc();


                } else if (v.getId() == R.id.cancel) {  //Cancel button
                    total_h2o_flown[b_tap_no - 1] = total_h2o_flown[b_tap_no - 1] + h2o_flown_litre[b_tap_no - 1];
                    tap_cancel_pressed[b_tap_no - 1] = true;  //timer will take care of es command
                    tap_no_req_busy[b_tap_no] = false;
                    en_dis_done_canc();
                }

                else if (v.getId() == R.id.printReciept)  //Print reciept   //Button unhide only when user is done with request
                {
                         serial_no_h2o_sold++; //Increment reciept serial number

                        String to_be_printed = Long.toString(serial_no_h2o_sold) + "          " + Float.toString(h2o_flown_litre[b_tap_no - 1]) +
                                "  " + Float.toString(h2o_flown_litre[b_tap_no - 1] * (float) 0.33); //0.33 cost factor

                        h2oController_MainActivity.send_to_printer(to_be_printed); //Print

                    if(h2oController_MainActivity.sprn_printer_state != 0)   //If printer connected then state will be zero.
                    {
                        display_toast("Printer has not connected");
                    }

                }

                else if (v.getId() == R.id.b_bar_graph) {  //bar graph button
                    flag_bar_graph_mode = true;
                    flag_line_graph_mode = false;
                    t_graph_status.setText("Loading");
                    t_graph_status.setTextColor(Color.parseColor("#FF0000"));
                    b_bar_graph.setEnabled(false);
                    b_line_graph.setEnabled(true);
                    t_graph_status.setText("CurrentFlow");
                    t_graph_status.setTextColor(Color.parseColor("#000000"));

                   // new update_gui_bar().execute(1);  // 0 - start , 1 - replace
                    h2o_update_bar_graph(1);
                } else if (v.getId() == R.id.b_line_graph) {      //Linegraph button
                    flag_bar_graph_mode = false;
                    flag_line_graph_mode = true;
                    t_graph_status.setText("TotalFlow");
                    t_graph_status.setTextColor(Color.parseColor("#000000"));
                    b_bar_graph.setEnabled(true);
                    b_line_graph.setEnabled(false);

                  //  new update_gui_line().execute(0); // 0 - start , 1 - replace
                    h2o_update_line_graph(0);

                }

            }

        };
        b_tap1.setOnClickListener(buttons);
        b_tap2.setOnClickListener(buttons);
        b_tap3.setOnClickListener(buttons);
        b_tap4.setOnClickListener(buttons);
        b_done.setOnClickListener(buttons);
        b_cancel.setOnClickListener(buttons);
        b_print_reciept.setOnClickListener(buttons);
        b_bar_graph.setOnClickListener(buttons);
        b_line_graph.setOnClickListener(buttons);


    }

    //Update text field of litres flown at the right side
    private void gui_update(int g_tap_no, boolean start) {
        //  Log.d("d_lst_act_mod", "gui update");
        Bundle bundle_litres_num = new Bundle();

        if (!start) {           //If first time enter dont change the colours , use default
            if (h2o_flown_litre[g_tap_no - 1] >= h2o_req_litre[g_tap_no - 1])             //Check the overflow
            {
                switch (g_tap_no) {
                    case 1:
                        tap1_col = "#ff0000";
                        break;
                    case 2:
                        tap2_col = "#ff0000";
                        break;
                    case 3:
                        tap3_col = "#ff0000";
                        break;
                    case 4:
                        tap4_col = "#ff0000";
                        break;
                }

            } else {
                switch (g_tap_no) {
                    case 1:
                        tap1_col = "#0099ff";
                        break;
                    case 2:
                        tap2_col = "#0099ff";
                        break;
                    case 3:
                        tap3_col = "#0099ff";
                        break;
                    case 4:
                        tap4_col = "#0099ff";
                        break;

                }
            }
        }

        //Pass color of text red or violet to the fragment
        bundle_litres_num.putString("tap1_col", tap1_col);
        bundle_litres_num.putString("tap2_col", tap2_col);
        bundle_litres_num.putString("tap3_col", tap3_col);
        bundle_litres_num.putString("tap4_col", tap4_col);

        //Pass litres flown to the fragment
        bundle_litres_num.putString("litres_flown_tap1", precision.format(h2o_flown_litre[0]));
        bundle_litres_num.putString("litres_flown_tap2", precision.format(h2o_flown_litre[1]));
        bundle_litres_num.putString("litres_flown_tap3", precision.format(h2o_flown_litre[2]));
        bundle_litres_num.putString("litres_flown_tap4", precision.format(h2o_flown_litre[3]));

        //Pass screen factor to the fragment
        bundle_litres_num.putFloat("h2o_screen_factor", h2o_screen_factor);

        try {
            //Fragmnet manager
            FragmentManager fragmentManager = getChildFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            //Replace / add the fragment
            h2o_con_fragment_litre_num litre_num_field = new h2o_con_fragment_litre_num();

            litre_num_field.setArguments(bundle_litres_num);
            if (!start) {

                fragmentTransaction.replace(R.id.f_fragment_ltext, litre_num_field);
            } else {
                fragmentTransaction.add(R.id.f_fragment_ltext, litre_num_field);
            }

            fragmentTransaction.commit();
        } catch (Exception e) {
            Log.d("d_gui", "number_fragment_error");
        }
    }


    //configure fragment initially
    private void config_fragment() {
        Log.d("d_lst_act_mod", "config fragment");
        if (flag_bar_graph_mode && one_time_entry) {
           //Call to setup bar graph 0- Add 1- Replace
            h2o_update_bar_graph(1);
        } else if (flag_line_graph_mode && one_time_entry) {
          //Call to setup bar graph 0- Add 1- Replace
            h2o_update_line_graph(1);
        } else {
         //Call to setup bar graph 0- Add 1- Replace  //Absolute fisrt time creation of fragment
            h2o_update_bar_graph(0);
        }

        gui_update(1, true);// tap no , true is boolean flag to indicate start
    }


    //Gui states while system stopped/usb disconnected
    private void disconnect_state_gui() {
        Log.d("d_lst_act_mod", "disconnect state gui");
        b_cancel.setEnabled(false);
        b_done.setEnabled(false);
        b_print_reciept.setEnabled(false);
        b_tap1.setEnabled(false);
        b_tap2.setEnabled(false);
        b_tap3.setEnabled(false);
        b_tap4.setEnabled(false);
        t_tap_no.setText("Tap 1");
        t_tap_no.setTextColor(Color.parseColor("#FF0000"));
        b_bar_graph.setEnabled(false);
        b_line_graph.setEnabled(false);
    }

    //Gui states while system has started or usb connection has established
    private void connect_state_gui() {
        Log.d("d_lst_act_mod", "connect state gui");
        b_cancel.setEnabled(false);
        b_done.setEnabled(true);
        b_print_reciept.setEnabled(false);
        b_tap1.setEnabled(false);
        b_tap2.setEnabled(true);
        b_tap3.setEnabled(true);
        b_tap4.setEnabled(true);
        t_tap_no.setText("Tap 1");
        b_bar_graph.setEnabled(!flag_bar_graph_mode);
        b_line_graph.setEnabled(!flag_line_graph_mode);
    }

    //Done,cancel button enable disable per tap
    public void en_dis_done_canc() {
        t_dis_sl.setText(Float.toString(h2o_req_litre[b_tap_no - 1]));  //SET REQUIRED litre text field
        if (tap_no_req_busy[b_tap_no]) {           //SET DONE ,CANCEL BUTTON State.
            b_done.setText("    Busy   ");
            b_done.setTextColor(Color.parseColor("#FF5555"));
            b_done.setEnabled(false);
            b_cancel.setEnabled(true);
            b_print_reciept.setEnabled(false);
        } else {
            b_done.setText("Send request");
            b_done.setTextColor(Color.parseColor("#0000FF"));
            b_done.setEnabled(true);
            b_cancel.setEnabled(false);
            b_print_reciept.setEnabled(true);
        }


    }

    //Method to toast a msg on ui thread
    private void display_toast(final String Message) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getActivity(), Message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    //Method to change the text in the text view
    private void put_text_to_textview(final TextView ptt_textview, final String ptt_text, final String ptt_color) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    ptt_textview.setText(ptt_text);  //set the text
                    ptt_textview.setTextColor(Color.parseColor(ptt_color)); //Set color
                } catch (Exception e) {
                    ptt_textview.setText("Invalid text or color");  //In case of exception
                    ptt_textview.setTextColor(Color.parseColor("#000000"));
                }

            }
        });
    }

//*******************************************************************************************************************//

//*******************************Simple timers**********************************************************************//

    //cancel all timers
    private void cancel_all_timers() {
        Log.d("d_lst_act_mod", "cancel_all_timers");
        update_linechart.cancel();
        timer_responceprocess.cancel();
        timer_send_commands.cancel();
    }

    //Configure general timer
    private void config_general_timer()
    {
        Log.d("d_lst_act_mod", "config general timer");
        //responce process timer
        timer_responceprocess = new Timer();
        timer_responceprocess r_responce = new timer_responceprocess();
        timer_responceprocess.schedule(r_responce, 0, responce_process_interval);   //interval ddefined globally


        //read status command send timer
        timer_send_commands = new Timer();
        timer_send_commands r_status = new timer_send_commands();
        timer_send_commands.schedule(r_status, 0, request_interval_readstatus);  //interval defined globally

        //Timer to update line chart
        update_linechart = new Timer();
        timer_linechart update_line = new timer_linechart();
        update_linechart.schedule(update_line, 0, linechart_update_time);     ////interval defined globally

        //Timer to update bar_graph
        update_bar_graph = new Timer();
        timer_update_bar_graph update_bar = new timer_update_bar_graph();
        update_bar_graph.schedule(update_bar, 0, bargraph_update_time);     ////interval defined globally


    }


    //Timer task  for processing responces  //Internally runs async task which does the main job
    private class timer_responceprocess extends TimerTask
    {
        int temp_pas_tapno = 0;
        @Override
        public void run()
        {
                    msg_type = 0;    //reset to zero ,To pass msg responce to  after execution in this async
                    String responce_recvd = null;

                    if (ss_flag)
                    {
                        try
                        {
                            responce_recvd = h2oController_MainActivity.scuc.SCuC_read_data_buffer();  //Get the responce of H2o_CB(arduino)

                            //  process the responces periodically
                            if (responce_recvd != null)
                            {
                                if (responce_recvd.contains("\r"))
                                {
                                    tokens = responce_recvd.split(Pattern.quote(",")); //Split at commas
                                    tokens[1] = tokens[1].replaceAll("[\\p{Cc}\\p{Cf}\\p{Co}\\p{Cn}]", ""); //Remove hidden character if there are
                                    tokens[2] = tokens[2].replaceAll("[\\p{Cc}\\p{Cf}\\p{Co}\\p{Cn}]", "");
                                    tokens[3] = tokens[3].replaceAll("[\\p{Cc}\\p{Cf}\\p{Co}\\p{Cn}]", "");

                                    if (tokens.length > 5)
                                    {

                                        tokens[4] = tokens[4].replaceAll("[\\p{Cc}\\p{Cf}\\p{Co}\\p{Cn}]", "");
                                        tokens[5] = tokens[5].replaceAll("[\\p{Cc}\\p{Cf}\\p{Co}\\p{Cn}]", "");
                                        // t_tap_no.setText(tokens[0] +"'"+ tokens[1] +"'"+ tokens[2]+","+tokens[3]+","+tokens[4]+","+tokens[5]); // Set text to stop
                                    }

                                    switch (Integer.parseInt(tokens[1]))
                                    {


                                        case 0:
                                            if (tokens.length > 5 && (!(tokens[2].equalsIgnoreCase("7"))))     //If responce for read status
                                            {

                                                h2o_flown_litre[(Integer.parseInt(tokens[4])) - 1] = Float.parseFloat(precision.format(Float.parseFloat((tokens[5])))); //update litres flown
                                                update_gui_flag = true;

                                            } else if (tokens[2].equalsIgnoreCase("7"))    //Emergency stop
                                            {

                                                h2o_flown_litre[(Integer.parseInt(tokens[4])) - 1] = Float.parseFloat(precision.format(Float.parseFloat((tokens[5])))); //update litres flown
                                                tap_no_req_busy[(Integer.parseInt(tokens[4]))] = false;   //Set tap to not busy
                                                msg_type = 1;
                                                update_gui_flag = true;
                                            }
                                            break;
                                        case 2:  //Time out
                                            h2o_flown_litre[Integer.parseInt(tokens[4]) - 1] = Float.parseFloat(precision.format(Float.parseFloat((tokens[5])))); //update litres flown
                                            tap_no_req_busy[Integer.parseInt(tokens[4])] = false;   //Set tap to not busy
                                            msg_type = 2;
                                            update_gui_flag = true;
                                            break;

                                    }
                                    if (update_gui_flag) {
                                        temp_pas_tapno = Integer.parseInt(tokens[4]);
                                        // new async_responce_process().execute(temp_pas_tapno);  //Update gui right side
                                        gui_update(temp_pas_tapno, false);  //Update gui
                                        update_gui_flag = false;  //one time flag
                                    }


                                    for (int temp = 0; temp < tokens.length; temp++)  //Clear tokens
                                    {
                                        tokens[temp] = "";
                                    }

                                    //     Log.d("d_lst_act_mod", "timer- responce process")
                                }

                            }

                        }
                        catch(Exception e)
                        {
                                Log.d("d_timer", "responce process ", e);
                        }

                    }


        }
    }

    //Timer task for sending read status commands continously to monitor taps also to send commands.
    private class timer_send_commands extends TimerTask
    {
        @Override
        public void run()
        {
            check_usb_connection();  //Check the status of usb connection

            try {
                if (ss_flag)    //If system has started
                {
                    switch (rs_counter)
                    {
                                case 1:
                                    commands.read_Status(1);
                                    break;

                                case 2:
                                    commands.read_Status(2);
                                    break;

                                case 3:
                                    commands.read_Status(3);
                                    break;

                                case 4:
                                    commands.read_Status(4);
                                    break;

                                case 5:
                                    if (tap_sl_pressed[0]) {
                                        commands.set_Litre(1, h2o_req_litre[0]);
                                        tap_sl_pressed[0] = false;
                                        break;
                                    }

                                case 6:
                                    if (tap_sl_pressed[1]) {
                                        commands.set_Litre(2, h2o_req_litre[1]);
                                        tap_sl_pressed[1] = false;
                                        break;
                                    }

                                case 7:
                                    if (tap_sl_pressed[2]) {
                                        commands.set_Litre(3, h2o_req_litre[2]);
                                        tap_sl_pressed[2] = false;
                                        break;
                                    }

                                case 8:
                                    if (tap_sl_pressed[3]) {
                                        commands.set_Litre(4, h2o_req_litre[3]);
                                        tap_sl_pressed[3] = false;
                                        break;
                                    }

                                case 9:
                                    if (tap_cancel_pressed[0]) {
                                        commands.emergency_stop(1);
                                        tap_cancel_pressed[0] = false;
                                        break;
                                    }

                                case 10:
                                    if (tap_cancel_pressed[1]) {
                                        commands.emergency_stop(2);
                                        tap_cancel_pressed[1] = false;
                                        break;
                                    }

                                case 11:
                                    if (tap_cancel_pressed[2]) {
                                        commands.emergency_stop(3);
                                        tap_cancel_pressed[2] = false;
                                        break;
                                    }

                                case 12:
                                    if (tap_cancel_pressed[3]) {
                                        commands.emergency_stop(4);
                                        tap_cancel_pressed[3] = false;
                                        break;
                                    }
                            }

                            rs_counter = rs_counter + 1;
                            if (rs_counter > no_of_taps + 8) {
                                rs_counter = 1;
                            }
                }
            } catch (Exception e)
            {
                Log.d("d_timer", "send command ", e);
            }
        }

    }


    // timer to update line chart
    private class timer_linechart extends TimerTask
    {
        @Override
        public void run()
        {
                    if (ss_flag)
                    {
                        //  Log.d("d_lst_act_mod", "timer - line chart");

                        h2o_up_time[linechart_timer_counter] = linechart_timer_counter;// Minute count on x axis
                        max_litre_flown = total_h2o_flown[0];  // Initial max litre flown
                        for (int temp_tap = 0; temp_tap < no_of_taps; temp_tap++) {
                            h2o_flown_litre_time[temp_tap][linechart_timer_counter] = total_h2o_flown[temp_tap];
                            if (max_litre_flown < total_h2o_flown[temp_tap])
                            {
                                max_litre_flown = total_h2o_flown[temp_tap];
                            }

                        }
                        linechart_timer_counter++;   //counter to count minutes
                        if (linechart_timer_counter > 1440)  //Check for 24 hours/1440min then reset the minute
                        {
                            linechart_timer_counter = 0;
                        }
                        if (flag_line_graph_mode) {
                        //   new update_gui_line().execute(1);// async task start to update line graph
                            h2o_update_line_graph(1);
                        }

                    }
        }
    }

    private class timer_update_bar_graph extends TimerTask
    {
        @Override
        public void run()
        {
            if (ss_flag)    //If system has started
            {
                if (flag_bar_graph_mode)
                {   // update bar graph 0- Add , 1 - Replace
                    pre_time = System.currentTimeMillis();
                    // new update_gui_bar().execute(1);
                    cur_time = System.currentTimeMillis();
                      h2o_update_bar_graph(1);
                    Log.d("d_time_test", Long.toString(cur_time - pre_time));

                }
            }
        }
    }
    //******************************************************************************************************************//


//*****************************Configure count down timers **********************************************************//

    //Configure msg timer
    private void config_countdown_timer() {
        Log.d("d_lst_act_mod", "config count down timer");
        total_time_start = msg_interval * 15;  //Always give 5-6 interval overhead than actual value.
        total_time_stop = msg_interval * 15;    // to compensate Internal clock error android

        cdt_start = new mycdt_start((long) total_time_start, (long) msg_interval);
        cdt_stop = new mycdt_stop((long) total_time_stop, (long) msg_interval); //Not used currently
    }


    //Count down timer to send massages during start
    private class mycdt_start extends CountDownTimer {

        public mycdt_start(long startTime, long interval) {
            super(startTime, interval);
        }

        @Override
        public void onFinish() {

            counter_cdt = 0;
        }

        @Override
        public void onTick(long millisUntilFinished) {
            Log.d("d_lst_act_mod", "my_cdt_start");
            counter_cdt++;
            switch (counter_cdt) {
                case 1:        //Initialisation time //One interval
                    break;
                case 2:
                    commands.start_Monitor(1);
                    break;
                case 3:
                    commands.start_Monitor(2);
                    break;
                case 4:
                    commands.start_Monitor(3);
                    break;
                case 5:
                    commands.start_Monitor(4);
                    break;
                case 6:
                    ss_flag = true;
                    counter_cdt = 0;
                    cdt_start.cancel();
                    break;


            }


        }
    }

    //Count down timer to send massages during stop //Not used currently
    private class mycdt_stop extends CountDownTimer  //Not used currently
    {

        public mycdt_stop(long startTime, long interval) {
            super(startTime, interval);
        }

        @Override
        public void onFinish() {
            counter_cdt = 0;
        }

        @Override
        public void onTick(long millisUntilFinished) {
            Log.d("d_lst_act_mod", "my_cdt_stop");
            counter_cdt++;
            switch (counter_cdt) {
                case 1:
                    commands.stop_Monitor(1);
                    break;
                case 2:
                    commands.stop_Monitor(2);
                    break;
                case 3:
                    commands.stop_Monitor(3);
                    break;
                case 4:
                    commands.stop_Monitor(4);
                    counter_cdt = 0;
                    cdt_stop.cancel();
                    break;
            }


        }
    }
//*********************************************************************************************************//

    //**********************************Graph update routines****************************************************//
    private void h2o_update_bar_graph(int add_or_replace)
    {
        // Log.d("d_lst_act_mod", "async-bar_graph");
        //Bundle object to pass variable sto fragment
        Bundle bundle_bar = new Bundle();
        bundle_bar.putFloatArray("h2o_flown_litre", h2o_flown_litre);
        bundle_bar.putInt("no_of_taps", no_of_taps);
        bundle_bar.putStringArray("tap_name", tap_name);
        bundle_bar.putFloat("h2o_set_ymax_bar", h2o_set_ymax_bar);
        bundle_bar.putFloatArray("h2o_req_litre", h2o_req_litre);

        FragmentManager fragmentManager = getChildFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        h2o_con_fragment_bar bar_graph = new h2o_con_fragment_bar();
        bar_graph.setArguments(bundle_bar);
        try {
            if (add_or_replace == 0) {
                fragmentTransaction.add(R.id.f_fragment_graph, bar_graph); //0- add
            } else if (add_or_replace == 1) {
                fragmentTransaction.replace(R.id.f_fragment_graph, bar_graph); // 1- replace
            }
            fragmentTransaction.commit();
        } catch (Exception e) {
            Log.e("d_gui", "bargrapgh fragment error  ");
        }

    }




    private void h2o_update_line_graph(int add_or_replace)
    {
        //   Log.d("d_lst_act_mod", "async-linegrapf");
        //Bundle object to pass variable sto fragment
        Bundle bundle_line = new Bundle();
        bundle_line.putInt("linechart_timer_counter", linechart_timer_counter);
        // bundle_line.putFloatArray("h2o_flown_litre_time",h2o_flown_litre_time); //Two dimensional array not posible
        //So using static variable currently
        bundle_line.putFloatArray("h2o_up_time", h2o_up_time);
        bundle_line.putInt("no_of_taps", no_of_taps);

        //Replace baar graph with line graph
        FragmentManager fragmentManager = getChildFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        h2o_con_fragment_line line_graph = new h2o_con_fragment_line(); //New fragment instance
        line_graph.setArguments(bundle_line);  //Pass bundle argument
        try {
            if (add_or_replace == 0)  //0-add
            {
                fragmentTransaction.add(R.id.f_fragment_graph, line_graph);    //Add first time
            } else if (add_or_replace == 1) //1- replace
            {
                fragmentTransaction.replace(R.id.f_fragment_graph, line_graph);  //Replace from next time
            }
            fragmentTransaction.commit();
        } catch (Exception e) {
            Log.d("d_gui", "error in linegraph ,async task");
        }
    }

//*********************************************************************************************************//

//*********************************System start and stop fucntions*****************************************//

    private void h2o_system_start()  //When permission is granted this fuction mmust be called may be in usb broadcast
    {
        Log.d("d_lst_act_mod", "h2o_system_start");
        //Set ss_flag true
        ss_flag = true;
        //Start the timer for start sequence
        cdt_start.start();

        getActivity().runOnUiThread(new Runnable()  //All ui must run on ui thread
        {
            @Override
            public void run() {
                //change the state of gui
                connect_state_gui();
                b_tap_no = 1;          //Select tap 1 automatically
                en_dis_done_canc();  //Set appropriate button state

                t_con_status.setText("H2OCB :Connected  "); //device connected
                t_tap_no.setText("Tap 1");
                t_con_status.setTextColor(Color.parseColor("#00FF00"));//Set color green
            }
        });


    }

    private void h2o_system_stop()  //Whne system is stoppped this fuction mmust be called may be in usb broadcast
    {

        Log.d("d_lst_act_mod", "h2o_system_stop");
        getActivity().runOnUiThread(new Runnable()  //All ui must run on ui thread
        {
            @Override
            public void run() {
                if (ss_flag)               //If ss_flag is true make it false as system has stopped
                {
                    ss_flag = false;
                }
                t_con_status.setText("H2OCB :NotConnected  ");  //Since no device
                t_tap_no.setText("Press Connect");
                t_con_status.setTextColor(Color.parseColor("#FF0000"));//Set color red
                disconnect_state_gui();   //disable the buttons
            }
        });

    }

//*********************************************************************************************************//

//***********************************Initialise arrays and flags*******************************************//

    private void init_array()                        //Initialise arrays
    {
        Log.d("d_lst_act_mod", "init_array");
        h2o_up_time[0] = 0;
        for (int temp_tap = 0; temp_tap < no_of_taps; temp_tap++) {
            tap_name[temp_tap] = "tap " + temp_tap;
            h2o_flown_litre[temp_tap] = 0;
            h2o_req_litre[temp_tap] = 0;
            for (int temp_litre = 0; temp_litre < h2o_up_time.length; temp_litre++) {
                h2o_flown_litre_time[temp_tap][temp_litre] = h2o_flown_litre[temp_tap];
            }
        }

    }


    private void asiotpf_init_flags()                 //Initialise flags
    {
        Log.d("d_lst_act_mod", "init_flags");
        for (int temp = 0; temp < no_of_taps; temp++) {
            tap_sl_pressed[temp] = false;
            tap_cancel_pressed[temp] = false;
        }
    }


//*********************************************************************************************************//


    //***************************check_usb_connection***********************************************************//
    private void check_usb_connection()  //Check whether H2o_CB connected or not
    {
        if (h2oController_MainActivity.scuc.SCuC_get_connection_status() == 0 && !flag_is_arduino_connected ) //If arduino & printer has succesfully connected and for fisrt time only
        {
            flag_is_arduino_connected = true;
            flag_is_arduino_disconnected = false;
            h2o_system_start();
        }

        if (h2oController_MainActivity.scuc.SCuC_get_connection_status() != 0 && !flag_is_arduino_disconnected) //If arduino || printer is disconnected
        {
            flag_is_arduino_disconnected = true;
            flag_is_arduino_connected = false;
            h2o_system_stop();
        }

        if(h2oController_MainActivity.sprn_printer_state==0 && !flag_is_tp_connected) //If printer is connected
        {
            flag_is_tp_connected=true;
            flag_is_tp_disconnected=false;
            put_text_to_textview(t_con_tp_status,"Printer: connected","#00FF00"); //Put text on ui thread
        }
        if(h2oController_MainActivity.sprn_printer_state!=0 && !flag_is_tp_disconnected) //If printer is not connected
        {
            flag_is_tp_connected=false;
            flag_is_tp_disconnected=true;
            put_text_to_textview(t_con_tp_status,"Printer: Disconnected","#FF0000"); //Put text on ui thread
        }
    }
    //**********************************************************************************************************//
}

