package com.srishti.shsfw_v00_01_01.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import com.srishti.shsfw_v00_01_01.SHSFW_FrameWorkActivity;
import com.srishti.shsfw_v00_01_01.interfaces.SHSFW_InputInterface;
import com.srishti.shsfw_v00_01_01.util.SHSFW_OperationMap.SHSFW_Operation;
import com.srishti.ui.message_routines.SDISP_MessageRoutines;

public class SHSFW_MoreFragment extends Fragment{

	SHSFW_InputInterface passer;
	private SDISP_MessageRoutines SHSFW_msgRoutine;

	//this value may have to be changed based on screen size
	/****************************************Modified 09 Nov 15******************************/
	private int optionLayoutWidthInCells = 24;
	public static int OPTIONS_LAYOUT_WIDTH ; //px
	//public static final int OPTIONS_LAYOUT_WIDTH = 660; //px
	/****************************************************************************************/
	
	private RelativeLayout options;
	private FrameLayout optionsContainer;
	private FragmentManager fragManager;

	private static final int FL_OPTIONSCONTAINER_ID = 2;
	
	/***********************Modified on 8th Dec 2015*************************/
	private String role = "";
	private String userRole="";
	private SharedPreferences sharedPref;
	
	public static final String ADMIN = "admin";
	public static final String SUPERVISOR = "supervisor";
	public static final String OPERATOR = "operator";
	/************************************************************************/ 

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		RelativeLayout baseLayout = new RelativeLayout(getActivity());
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		baseLayout.setLayoutParams(params);

		// fragment manager to manage the loading of the fragments
		fragManager = getChildFragmentManager();

		// variable to set the position, width and size parameters of the option buttons
		int optBtnX = 1;
		int optBtnY = 1;
		int optBtnW = 22;
		int optBtnH = 9;
		int optBtnFontSize = 1;
		
		//relative layout which contains the option buttons
		options = new RelativeLayout(getActivity());
		
		SHSFW_msgRoutine = SDISP_MessageRoutines.SDISP_getInstance(
				getActivity(), options, 0);
		OPTIONS_LAYOUT_WIDTH = (int) (optionLayoutWidthInCells * SHSFW_msgRoutine.SDISP_getXPixelsPerCell());
		
		RelativeLayout.LayoutParams paramsOptions = new RelativeLayout.LayoutParams(OPTIONS_LAYOUT_WIDTH, SHSFW_FrameWorkActivity.SCROLLVIEW_HEIGHT);
		options.setLayoutParams(paramsOptions);
		options.setBackgroundColor(Color.GRAY);
		baseLayout.addView(options);

		// fragment container to load the different fragments which any of the 
		// option buttons are clicked
		optionsContainer = new FrameLayout(getActivity());
		optionsContainer.setId(FL_OPTIONSCONTAINER_ID);
		RelativeLayout.LayoutParams paramsOptionsContainer = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, SHSFW_FrameWorkActivity.SCROLLVIEW_HEIGHT);
		paramsOptionsContainer.setMargins(OPTIONS_LAYOUT_WIDTH, 0, 0, 0);
		optionsContainer.setLayoutParams(paramsOptionsContainer);
		optionsContainer.setBackgroundColor(Color.LTGRAY);
		baseLayout.addView(optionsContainer);
		
		SHSFW_msgRoutine.SDISP_buttonIconText("1", optBtnX, optBtnY, optBtnW, optBtnH, 7, optBtnFontSize, 7, 7, 117, 53);
		Button pref = (Button) options.findViewWithTag("1");
		
		/***********************Modified on 8th Dec 2015*************************/
		sharedPref = getActivity().getSharedPreferences("ekycpref", Context.MODE_PRIVATE);
		role = sharedPref.getString("LoggedUserType", "NONE");
		userRole=sharedPref.getString("LoginUserLevel", "NONE");
		checkButtonStatus(pref,role,userRole);
		/************************************************************************/
		
		pref.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {	
				SHSFW_PreferenceFragment prefFrag = new SHSFW_PreferenceFragment();
				fragManager.beginTransaction().replace(optionsContainer.getId(), prefFrag).commit();
			}
		});

		SHSFW_msgRoutine.SDISP_buttonIconText("2", optBtnX, optBtnY += 12, optBtnW, optBtnH, 7, optBtnFontSize, 7, 7, 83, 47);	
		Button language = (Button) options.findViewWithTag("2");
		language.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				SHSFW_LanguageFragment langFrag = new SHSFW_LanguageFragment();
				fragManager.beginTransaction().replace(optionsContainer.getId(), langFrag).commit();
			}
		});

		SHSFW_msgRoutine.SDISP_buttonIconText("3", optBtnX, optBtnY += 12, optBtnW, optBtnH, 7, optBtnFontSize, 7, 7, 205, 55);
		Button hwCheck = (Button) options.findViewWithTag("3");
		hwCheck.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				SHSFW_HWCheckFragment hwFrag = new SHSFW_HWCheckFragment();
				fragManager.beginTransaction().replace(optionsContainer.getId(), hwFrag).commit();
			}
		});

		SHSFW_msgRoutine.SDISP_buttonMessage("done", optBtnX, optBtnY += 17, optBtnW, optBtnFontSize, 7, 7, 3);
		Button done = (Button) options.findViewWithTag("done");
		done.setText("Done");
		done.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				passer.SHSFW_IConfigHotKey(SHSFW_Operation.MORE, true);
				getActivity().getSupportFragmentManager().popBackStack();
			}
		});

		SHSFW_LanguageFragment langFrag = new SHSFW_LanguageFragment();
		fragManager.beginTransaction().replace(optionsContainer.getId(), langFrag).commit();

		return baseLayout;
	}

	private void checkButtonStatus(Button btn, String role, String userRole) {
		
		//Toast.makeText(getActivity(), "Role = "+role, Toast.LENGTH_SHORT).show();
		if(role.equalsIgnoreCase("admin")&&userRole.equalsIgnoreCase("L1")){
			btn.setEnabled(true);
		}else {
			btn.setEnabled(false);
		}
		
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {

			passer = (SHSFW_InputInterface) activity;

		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement SHSFW_InputInterface");
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		//passer.SHSFW_configHotKey(SHSFW_Operation.MORE, new SHSFW_OperationMap(SHSFW_Operation.MORE, 45, false, true));
		passer.SHSFW_IConfigHotKey(SHSFW_Operation.MORE, false);
	}

}
