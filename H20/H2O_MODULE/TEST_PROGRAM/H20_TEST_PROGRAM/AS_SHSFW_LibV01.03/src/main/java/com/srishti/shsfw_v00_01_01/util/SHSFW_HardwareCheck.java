package com.srishti.shsfw_v00_01_01.util;

import android.content.Context;


import com.srishti.sprn.SPRN_Print;
import com.srishti.sprn.SPRN_ReturnCode;

public class SHSFW_HardwareCheck {


	SPRN_Print prnObj;
	
	Context SHSFW_Context;
	
	public SHSFW_HardwareCheck(Context SHSFW_context) {
		this.SHSFW_Context = SHSFW_context;
		

		prnObj = SPRN_Print.SPRN_getInstance(SHSFW_context);
		
	}
	
	public void SHSFW_autoCheckHardware(boolean bool) throws Exception{
		initFP(bool);
		
	}
	
	private void initFP(final boolean bool) throws Exception{

		/*UpdateUI(fp, true);
		UpdateUI(iris, false);
		UpdateUI(printer, false);
		UpdateUI(settings, false);*/
		new Thread(new Runnable() {

			@Override
			public void run() {
				//Toast.makeText(SHSFW_Context, "INSIDE FP THREAD", Toast.LENGTH_SHORT).show();
				try {
					//UpdateUI(fpStatus, "Checking...", false);				

					//fpObj.SFP_disconnectDevice();


					/*UpdateUI(fp, true);
					UpdateUI(iris, true);
					UpdateUI(printer, true);
					UpdateUI(settings, true);*/
					if(bool){
						initIris(true);
					}
				} catch (Exception e) {
					//UpdateUI(fpStatus, "Status : Failure", false);
				}

			}
		}).start();

	}
	
	private void initIris(final boolean bool) throws Exception {

		/*UpdateUI(fp, false);
		UpdateUI(iris, true);
		UpdateUI(printer, false);
		UpdateUI(settings, false);*/
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					//UpdateUI(irisStatus, "Checking...", false);



					/*UpdateUI(fp, true);
					UpdateUI(iris, true);
					UpdateUI(printer, true);
					UpdateUI(settings, true);*/
					if(bool){
						initPrn(false);
					}
				} catch (Exception e) {
					//UpdateUI(irisStatus, "Status : Failure", false);
				}
			}
		}).start();
	}
	
	private void initPrn(final boolean bool) throws Exception {

		/*UpdateUI(fp, false);
		UpdateUI(iris, false);
		UpdateUI(printer, true);
		UpdateUI(settings, false);*/
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					//UpdateUI(prnStatus, "Checking...", false);
					int initRet = prnObj.SPRN_initializePrinter();

					if(initRet == SPRN_ReturnCode.SPRN_SUCCESS){
						//UpdateUI(prnStatus, "Status : Success", false);
						//Toast.makeText(SHSFW_Context, "Printer success", Toast.LENGTH_SHORT).show();
					}else {
						//UpdateUI(prnStatus, "Status : Failure", false);
						//Toast.makeText(SHSFW_Context, "Printer failure", Toast.LENGTH_SHORT).show();
					}

					/*UpdateUI(fp, true);
					UpdateUI(iris, true);
					UpdateUI(printer, true);
					UpdateUI(settings, true);*/
					if(bool){
						// do something
					}
				} catch (Exception e) {
					//UpdateUI(prnStatus, "Status : Failure", false);
				}
			}
		}).start();
	}
	
}
