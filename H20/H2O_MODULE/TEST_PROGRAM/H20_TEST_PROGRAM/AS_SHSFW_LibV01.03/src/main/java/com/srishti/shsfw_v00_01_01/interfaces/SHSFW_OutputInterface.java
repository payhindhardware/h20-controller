package com.srishti.shsfw_v00_01_01.interfaces;

import com.srishti.shsfw_v00_01_01.util.SHSFW_OperationMap.SHSFW_Operation;

public interface SHSFW_OutputInterface {

	public void SHSFW_onClick(SHSFW_Operation SHSFW_opType);

}
