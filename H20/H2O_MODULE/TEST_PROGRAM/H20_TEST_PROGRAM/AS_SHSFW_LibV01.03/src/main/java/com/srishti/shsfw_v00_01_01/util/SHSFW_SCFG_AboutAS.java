package com.srishti.shsfw_v00_01_01.util;

public class SHSFW_SCFG_AboutAS {
public static String SCFG_AbtAS_SWversion = "01.00.00";
public static String SCFG_AbtAS_HWversion = "01.00.00";
public static String SCFG_AbtAS_SWrelDate = "2016-05-03";
public static String SCFG_AbtAS_DeviceID = "123456";
public static String SCFG_AbtAS_AsoID = "654321";
public static String SCFG_AbtAS_LicenseSatus = "Active";
}
