package com.srishti.shsfw_v00_01_01.util;

import java.util.ArrayList;

import com.srishti.shsfw_v00_01_01.interfaces.SHSFW_OutputInterface;

public class SHSFW_InputParams {
		
	public SHSFW_InputParams() {
		
	}

	private ArrayList<SHSFW_OperationMap> SHSFW_operationMapping;
	private String SHSFW_message1Txt;
	private String SHSFW_message2Txt;
	private String SHSFW_message3Txt;
	private int SHSFW_message1ID;
	private int SHSFW_message2ID;
	private int SHSFW_message3ID;
	private int SHSFW_textSize;
	private int SHSFW_textColor;
	private int SHSFW_headerBGColor;
	private int SHSFW_logo;
	private int SHSFW_logoBGColor;
	private SHSFW_OutputInterface SHSFW_interface;
	
	public ArrayList<SHSFW_OperationMap> getSHSFW_operationMapping() {
		return SHSFW_operationMapping;
	}
	public void setSHSFW_operationMapping(
			ArrayList<SHSFW_OperationMap> sHSFW_operationMapping) {
		SHSFW_operationMapping = sHSFW_operationMapping;
	}
	public String getSHSFW_message1Txt() {
		return SHSFW_message1Txt;
	}
	public void setSHSFW_message1Txt(String sHSFW_message1Txt) {
		SHSFW_message1Txt = sHSFW_message1Txt;
	}
	public String getSHSFW_message2Txt() {
		return SHSFW_message2Txt;
	}
	public void setSHSFW_message2Txt(String sHSFW_message2Txt) {
		SHSFW_message2Txt = sHSFW_message2Txt;
	}
	public String getSHSFW_message3Txt() {
		return SHSFW_message3Txt;
	}
	public void setSHSFW_message3Txt(String sHSFW_message3Txt) {
		SHSFW_message3Txt = sHSFW_message3Txt;
	}
	public int getSHSFW_message1ID() {
		return SHSFW_message1ID;
	}
	public void setSHSFW_message1ID(int sHSFW_message1ID) {
		SHSFW_message1ID = sHSFW_message1ID;
	}
	public int getSHSFW_message2ID() {
		return SHSFW_message2ID;
	}
	public void setSHSFW_message2ID(int sHSFW_message2ID) {
		SHSFW_message2ID = sHSFW_message2ID;
	}
	public int getSHSFW_message3ID() {
		return SHSFW_message3ID;
	}
	public void setSHSFW_message3ID(int sHSFW_message3ID) {
		SHSFW_message3ID = sHSFW_message3ID;
	}
	public int getSHSFW_textSize() {
		return SHSFW_textSize;
	}
	public void setSHSFW_textSize(int sHSFW_textSize) {
		SHSFW_textSize = sHSFW_textSize;
	}
	public int getSHSFW_textColor() {
		return SHSFW_textColor;
	}
	public void setSHSFW_textColor(int sHSFW_textColor) {
		SHSFW_textColor = sHSFW_textColor;
	}
	public int getSHSFW_headerBGColor() {
		return SHSFW_headerBGColor;
	}
	public void setSHSFW_headerBGColor(int sHSFW_headerBGColor) {
		SHSFW_headerBGColor = sHSFW_headerBGColor;
	}
	public int getSHSFW_logo() {
		return SHSFW_logo;
	}
	public void setSHSFW_logo(int sHSFW_logo) {
		SHSFW_logo = sHSFW_logo;
	}
	public SHSFW_OutputInterface getSHSFW_interface() {
		return SHSFW_interface;
	}
	public void setSHSFW_interface(SHSFW_OutputInterface sHSFW_interface) {
		SHSFW_interface = sHSFW_interface;
	}
	public int getSHSFW_logoBGColor() {
		return SHSFW_logoBGColor;
	}
	public void setSHSFW_logoBGColor(int sHSFW_logoBGColor) {
		SHSFW_logoBGColor = sHSFW_logoBGColor;
	}
	
	@Override
	public String toString() {
		return "SHSFW_InputParams [SHSFW_operationMapping="
				+ SHSFW_operationMapping + ", SHSFW_message1Txt="
				+ SHSFW_message1Txt + ", SHSFW_message2Txt="
				+ SHSFW_message2Txt + ", SHSFW_message3Txt="
				+ SHSFW_message3Txt + ", SHSFW_message1ID=" + SHSFW_message1ID
				+ ", SHSFW_message2ID=" + SHSFW_message2ID
				+ ", SHSFW_message3ID=" + SHSFW_message3ID
				+ ", SHSFW_textSize=" + SHSFW_textSize + ", SHSFW_textColor="
				+ SHSFW_textColor + ", SHSFW_headerBGColor="
				+ SHSFW_headerBGColor + ", SHSFW_logo=" + SHSFW_logo
				+ ", SHSFW_logoBackgroundColor=" + SHSFW_logoBGColor
				+ ", SHSFW_interface=" + SHSFW_interface + "]";
	}
	
}
