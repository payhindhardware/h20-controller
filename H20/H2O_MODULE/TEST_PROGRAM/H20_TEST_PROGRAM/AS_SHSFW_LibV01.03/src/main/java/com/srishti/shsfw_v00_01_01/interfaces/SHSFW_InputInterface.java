package com.srishti.shsfw_v00_01_01.interfaces;

import com.srishti.shsfw_v00_01_01.util.SHSFW_OperationMap.SHSFW_Operation;


public interface SHSFW_InputInterface {
	
	/**
	 * This method is to be used to configure the entire header section at the same time.
	 * This includes Hot Keys, Messages and Logo.
	 * @param ipParams - object of {@link SHSFW_InputParams} class.
	 */
	/*public void SHSFW_IConfigParams(SHSFW_InputParams SHSFW_ipParams);*/
	
	/**
	 * This method is to be used to configure the 3 messages that needs to be displayed 
	 * on the screen. May either be {@link String}, {@link Integer} or <code>null</code>.
	 * If <code>null</code> - previous data is retained.
	 * If {@link Integer} - corresponding message as defined in SUIRES is displayed. This supports
	 * regional languages as well.
	 * If {@link String} - the message is directly displayed on the screen without any checks. 
	 * For regional language the text has to be provided as input.
	 * @param msg1 - data corresponding to line 1. 
	 * @param msg2 - data corresponding to line 2.
	 * @param msg3 - data corresponding to line 3.
	 */
	public void SHSFW_IConfigMessages(Object SHSFW_msg1, Object SHSFW_msg2, Object SHSFW_msg3, int SHSFW_fontColor);
	
	/**
	 * This method may be used to individually change the properties of Hot keys.
	 * @param current - The already existing hot key which you intend to change
	 * @param opMap - The properties of the new hot key to be place on screen.
	 */
	/*public void SHSFW_IConfigHotKey(SHSFW_Operation SHSFW_current, SHSFW_OperationMap SHSFW_opMap);*/
	
	/**
	 * This method may be used to individually enable or disable the hot keys.
	 * @param current - The already existing hot key which you intend to change
	 * @param opMap - The properties of the new hot key to be place on screen.
	 */
	public void SHSFW_IConfigHotKey(SHSFW_Operation SHSFW_hotKey, boolean SHSFW_isEnabled);
	
	/**
	 * 
	 * @param SHSFW_iconID
	 */
	public void SHSFW_IConfigureCustomHotKeyIcon(int SHSFW_iconID);
	
	/**
	 * This method may be used to change the logo if required.
	 * @param SHSFW_logo
	 * @param SHSFW_logoBGColor
	 */
	public void SHSFW_IConfigLogo(int SHSFW_logo, int SHSFW_logoBGColor);
		
}
