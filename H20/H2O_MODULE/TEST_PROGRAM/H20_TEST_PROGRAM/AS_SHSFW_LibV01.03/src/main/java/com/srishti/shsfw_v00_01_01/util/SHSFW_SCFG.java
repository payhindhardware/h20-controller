package com.srishti.shsfw_v00_01_01.util;

import android.os.Environment;

import com.srishti.suires.SUIRES_ROUTINE_DB;

public class SHSFW_SCFG{


	/*Usage: This variable is required to store the font size integer values*/
	public static int[] SHSFW_SDISP_fonts = new int[]{3,4,5,6};

	/*Usage: This variable is used to store the language code value */
	public static int SHSFW_SDISP_languageCode = 00;

	/*Usage: This variable is used to store the DB instance value */
	public static SUIRES_ROUTINE_DB SCFG_DB_instance = null;

	/*Usage: This variable is used to save the path to SUIRES */
	public static String SDISP_path = Environment.getExternalStorageDirectory()
			.getAbsolutePath();
	//Usage: This variable is used to get Location
	public static String SHSFW_SLOC_dispStrg = "";

	//used for storing button Tag
	public static String tag="00";

}
