package com.srishti.shsfw_v00_01_01.fragments;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.RelativeLayout;

import com.srishti.shsfw_v00_01_01.interfaces.SHSFW_InputInterface;
import com.srishti.shsfw_v00_01_01.util.SHSFW_OperationMap.SHSFW_Operation;

public class SHSFW_UnlockFragment extends Fragment{

	SHSFW_InputInterface passer;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		RelativeLayout rl = new RelativeLayout(getActivity());
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		rl.setLayoutParams(params);

		rl.setBackgroundColor(Color.CYAN);


		return rl;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			passer = (SHSFW_InputInterface) activity;

		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement SHSFW_InputInterface");
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		//passer.SHSFW_configHotKey(SHSFW_Operation.MORE, new SHSFW_OperationMap(SHSFW_Operation.MORE, 45, false, true));
		passer.SHSFW_IConfigHotKey(SHSFW_Operation.MORE, false);
		passer.SHSFW_IConfigHotKey(SHSFW_Operation.LOGOUT, true);
		passer.SHSFW_IConfigHotKey(SHSFW_Operation.LOGIN, false);
	}
}
