package com.srishti.shsfw_v00_01_01.util;

public class SHSFW_OperationMap {

	public static enum SHSFW_Operation {
		INVALID(0), HOME(1), LOGOUT(2), LOGIN(3), LOCK(4), UNLOCK(5), MORE(6),
		REPORT(7), PRIMARY(8), CUSTOM(9), SECONDARY(10);

		private int value;

		private SHSFW_Operation(int value) {
			this.value = value;
		}

		public int getValue() {
			return this.value;
		}
	}
	
	private SHSFW_Operation SHSFW_OperationType;
	private int SHSFW_displayResource;
	private boolean SHSFW_isEnabled;
	private boolean SHSFW_isVisible;

	public SHSFW_OperationMap() {

	}

	public SHSFW_OperationMap(SHSFW_Operation sHSFW_OperationType,
			int sHSFW_displayResource, boolean sHSFW_isEnabled, boolean sHSFW_isVisible) {

		this.SHSFW_OperationType = sHSFW_OperationType;
		this.SHSFW_displayResource = sHSFW_displayResource;
		this.SHSFW_isEnabled = sHSFW_isEnabled;
		this.SHSFW_isVisible = sHSFW_isVisible;
	}

	public int getSHSFW_displayResource() {
		return SHSFW_displayResource;
	}

	public void setSHSFW_displayResource(int sHSFW_displayResource) {
		SHSFW_displayResource = sHSFW_displayResource;
	}

	public SHSFW_Operation getSHSFW_OperationType() {
		return SHSFW_OperationType;
	}

	public void setSHSFW_OperationType(SHSFW_Operation sHSFW_OperationType) {
		SHSFW_OperationType = sHSFW_OperationType;
	}

	public boolean isSHSFW_isEnabled() {
		return SHSFW_isEnabled;
	}

	public void setSHSFW_isEnabled(boolean sHSFW_isEnabled) {
		SHSFW_isEnabled = sHSFW_isEnabled;
	}
	
	public boolean isSHSFW_isVisible() {
		return SHSFW_isVisible;
	}

	public void setSHSFW_isVisible(boolean sHSFW_isVisible) {
		SHSFW_isVisible = sHSFW_isVisible;
	}

	@Override
	public String toString() {
		return "SHSFW_OperationMap [SHSFW_OperationType=" + SHSFW_OperationType
				+ ", SHSFW_displayResource=" + SHSFW_displayResource
				+ ", SHSFW_isEnabled=" + SHSFW_isEnabled + ", SHSFW_isVisible="
				+ SHSFW_isVisible + "]";
	}
}
