package com.srishti.shsfw_v00_01_01.util;

import android.content.Context;
import android.content.res.AssetManager;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by princ on 20-07-2016.
 */
public class SHSFW_CopyAsesetstoLocal {

    private final Context myContext;
    private String SHSFW_loginpath=Environment.getExternalStorageDirectory()+"/Login/databases/";
    private String SHSFW_loginDataPath=SHSFW_loginpath+"srishti-login";


    public SHSFW_CopyAsesetstoLocal(Context context) {

      //  super(context,"srishti-login", null, 1);
        this.myContext = context;
        copyAssets();

    }

    //This Method used to create folder and copy database file for login
    private void copyAssets() {
        AssetManager assetManager = myContext.getAssets();
        File mfile = new File(SHSFW_loginpath);
        File loginDb=new File(SHSFW_loginDataPath);
        String[] files = null;
        try {
            files = assetManager.list("");
        } catch (IOException e) {
            Log.e("tag", "Failed to get asset file list.", e);
        }
        Log.i("Login",""+!loginDb.exists());
        System.out.println(" login path chek "+loginDb.exists());
        if (files != null&&!loginDb.exists()) for (String filename : files) {
            if (filename.equalsIgnoreCase("srishti-login")) {

                InputStream in = null;
                OutputStream out = null;

                System.out.println("before file.exits");
                if(!mfile.exists()){
                    System.out.println("no folders");
                    mfile.mkdirs();
                    System.out.println("created dirs");
                    //mfile = new File(dbCreationPath+DATABASE_NAME);
                    //System.out.println("pt 0");
                }
                try {
                    in = assetManager.open(filename);
                    File outFile = new File( SHSFW_loginpath, filename);
                    System.out.println("Output file name"+outFile);
                    out = new FileOutputStream(outFile);
                    copyFile(in, out);
                } catch(IOException e) {
                    Log.e("tag", "Failed to copy asset file: " + filename, e);
                }
                finally {
                    if (in != null) {
                        try {
                            in.close();
                        } catch (IOException e) {
                            // NOOP
                        }
                    }
                    if (out != null) {
                        try {
                            out.close();
                        } catch (IOException e) {
                            // NOOP
                        }
                    }
                }

            }
        }

    }
    private void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while((read = in.read(buffer)) != -1){
            out.write(buffer, 0, read);
        }
    }
}
