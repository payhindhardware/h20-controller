package com.srishti.shsfw_v00_01_01.util;

import com.srishti.shsfw_v00_01_01.util.SHSFW_OperationMap.SHSFW_Operation;

public class SHSFW_HotkeyState {

	private SHSFW_Operation SHSFW_tag;
	private boolean SHSFW_isEnabled;
	private boolean SHSFW_isVisible;
	
	public SHSFW_HotkeyState() {
		
	}
	
	public SHSFW_HotkeyState(SHSFW_Operation sHSFW_tag, boolean sHSFW_isEnabled,
			boolean sHSFW_isVisible) {
		super();
		SHSFW_tag = sHSFW_tag;
		SHSFW_isEnabled = sHSFW_isEnabled;
		SHSFW_isVisible = sHSFW_isVisible;
	}
	
	public boolean isSHSFW_isEnabled() {
		return SHSFW_isEnabled;
	}
	public void setSHSFW_isEnabled(boolean sHSFW_isEnabled) {
		SHSFW_isEnabled = sHSFW_isEnabled;
	}
	public boolean isSHSFW_isVisible() {
		return SHSFW_isVisible;
	}
	public void setSHSFW_isVisible(boolean sHSFW_isVisible) {
		SHSFW_isVisible = sHSFW_isVisible;
	}
	public SHSFW_Operation getSHSFW_tag() {
		return SHSFW_tag;
	}
	public void setSHSFW_tag(SHSFW_Operation sHSFW_tag) {
		SHSFW_tag = sHSFW_tag;
	}
	
	@Override
	public String toString() {
		return "SHSFW_HotkeyState [SHSFW_tag=" + SHSFW_tag
				+ ", SHSFW_isEnabled=" + SHSFW_isEnabled + ", SHSFW_isVisible="
				+ SHSFW_isVisible + "]";
	}
}
