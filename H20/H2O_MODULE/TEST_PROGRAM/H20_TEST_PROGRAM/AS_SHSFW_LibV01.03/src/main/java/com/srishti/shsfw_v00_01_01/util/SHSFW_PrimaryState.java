package com.srishti.shsfw_v00_01_01.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class SHSFW_PrimaryState {

	private static SharedPreferences SHSFW_pref;
	private static Editor SHSFW_editor;

	private static Context SHSFW_context;

	private static final String PREF_NAME = "primary_state";
	private static final String PREF_KEY = "state";

	public static enum SHSFW_PrimaryStates {
		PRIMARY(1), SECONDARY(2);

		private int value;

		private SHSFW_PrimaryStates(int value) {
			this.value = value;
		}

		public int getValue() {
			return this.value;
		}
	}

	/* Singleton begins */
	private static SHSFW_PrimaryState SHSFW_stateInstance = null;

	public static SHSFW_PrimaryState getInstance(Context SHSFW_context) {
		SHSFW_PrimaryState.SHSFW_context = SHSFW_context;
		SHSFW_pref = SHSFW_context.getSharedPreferences(PREF_NAME,
				Context.MODE_PRIVATE);

		if (SHSFW_stateInstance == null) {
			synchronized (SHSFW_PrimaryState.class) {
				if (SHSFW_stateInstance == null) {
					SHSFW_stateInstance = new SHSFW_PrimaryState();
				}
			}
		}
		return SHSFW_stateInstance;
	}

	public static void clearInstance() {
		if (SHSFW_stateInstance != null) {
			SHSFW_stateInstance = null;
		}
	}

	private SHSFW_PrimaryState() {

	}

	/* Singleton ends */

	private SHSFW_PrimaryStates SHSFW_state = SHSFW_PrimaryStates.PRIMARY;

	public SHSFW_PrimaryStates getSHSFW_state() {
		if (SHSFW_pref == null) {
			SHSFW_pref = SHSFW_context.getSharedPreferences(PREF_NAME,
					Context.MODE_PRIVATE);
		}

		String state = SHSFW_pref.getString(PREF_KEY,
				SHSFW_PrimaryStates.PRIMARY.toString());
		SHSFW_state = mapStateToEnum(state);
		return SHSFW_state;
	}

	public void setSHSFW_state(SHSFW_PrimaryStates sHSFW_state) {
		
		if (SHSFW_pref == null) {
			SHSFW_pref = SHSFW_context.getSharedPreferences(PREF_NAME,
					Context.MODE_PRIVATE);
			SHSFW_editor = SHSFW_pref.edit();
		}
		SHSFW_editor = SHSFW_pref.edit();
		
		SHSFW_editor.putString(PREF_KEY, sHSFW_state.toString());
		SHSFW_editor.apply();
		
		SHSFW_state = sHSFW_state;
	}
	
	private SHSFW_PrimaryStates mapStateToEnum(String state) {

		SHSFW_PrimaryStates mappedState;

		if (state.equalsIgnoreCase("PRIMARY")) {
			mappedState = SHSFW_PrimaryStates.PRIMARY;

		} else if (state.equalsIgnoreCase("SECONDARY")) {
			mappedState = SHSFW_PrimaryStates.SECONDARY;
		} else {
			mappedState = null;
		}

		return mappedState;
	}
}
