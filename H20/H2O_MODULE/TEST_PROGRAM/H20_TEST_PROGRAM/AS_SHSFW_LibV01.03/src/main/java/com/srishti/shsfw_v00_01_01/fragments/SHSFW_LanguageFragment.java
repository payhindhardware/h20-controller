package com.srishti.shsfw_v00_01_01.fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import android.app.Activity;
import android.content.res.Configuration;
import android.graphics.Color;
import android.opengl.Visibility;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.common.annotations.VisibleForTesting;

import com.srishti.shsfw_v00_01_01.interfaces.SHSFW_InputInterface;
import com.srishti.shsfw_v00_01_01.util.SHSFW_OperationMap.SHSFW_Operation;
import com.srishti.shsfw_v00_01_01.util.SHSFW_SCFG;
import com.srishti.shsfw_v00_01_01.util.SHSFW_SCFG_AboutAS;
import com.srishti.suires.SUIRES_ROUTINE_Resources;
import com.srishti.ui.message_routines.SDISP_MessageRoutines;

public class SHSFW_LanguageFragment extends Fragment implements
View.OnClickListener {

	SHSFW_InputInterface passer;
	private SDISP_MessageRoutines SHSFW_msgRoutine;
	SUIRES_ROUTINE_Resources SUIRES_routine_resources;

	TextView lang;
	HashMap<String, Integer> SHSFW_AvailableLanguages;
	List<String> SHSFW_AvailableLanguagesKey;
	// List<String> SHSFW_AvailableLanguagesKey


	SHSFW_LanguageFragment languagefragmentObject;
	Button btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9, btn10, btn11,
	btn12, btn13, btn14, btn15, btn16;

	//ranjan 
	ArrayList<Button> languageButtons=new ArrayList<Button>();
	static int tempCount,finalCount=0,countString=0;
	ArrayList<Button> emptyButtons=new ArrayList<Button>();

	RelativeLayout rl;
	String btntag;

	int wBtn = 16;
	int wIcon = 8;
	int fontColor = 7;
	int btnHeight = 11;
	int FontSize = 1;
	int BtnColor = 2;
	int hBtn = 9;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		SHSFW_resource_config();
		rl = new RelativeLayout(getActivity());
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		rl.setLayoutParams(params);

		
		SHSFW_msgRoutine = SDISP_MessageRoutines.SDISP_getInstance(
				getActivity(), rl, 0);
		// Note:- Resource config for activity

		SHSFW_SoftwareDetails();

		SHSFW_LanguageButton();
		//ranjan
		if(SHSFW_SCFG.tag.equals("00")){
			rl.findViewWithTag(""+finalCount).setBackgroundColor(Color.parseColor("#66CC00"));
			rl.findViewWithTag(""+finalCount).setOnClickListener(this);
			rl.findViewWithTag(""+finalCount).performClick();
		}else{
			rl.findViewWithTag(SHSFW_SCFG.tag).setBackgroundColor(Color.parseColor("#66CC00"));
		}

		return rl;
	}


	private void SHSFW_SoftwareDetails() {
		// TODO Auto-generated method stub

		int SHSFW_textColor = 7;
		int SHSFW_TextSize=1;

		SHSFW_msgRoutine.SDISP_message("txtLang", 3, 1, 1, 1, SHSFW_textColor);// 5th is size of parameter

		lang = (TextView) rl.findViewWithTag("txtLang");
		SUIRES_routine_resources = SUIRES_ROUTINE_Resources.SUIRES_getResourceInstance(getActivity());//SUIRES_getResourceInstance(this);
		String versionStr = SUIRES_routine_resources.SUIRES_getMessage(300)
				.getSUIRES_message();
		String deviceIDStr = SUIRES_routine_resources.SUIRES_getMessage(301)
				.getSUIRES_message();
		String asoId = SUIRES_routine_resources.SUIRES_getMessage(294)
				.getSUIRES_message();
		lang.setText(versionStr+":"+SHSFW_SCFG_AboutAS.SCFG_AbtAS_SWversion+
				", "+deviceIDStr+":"				+", "+asoId+":");

	}

	public void SHSFW_LanguageButton() {
		// Btn1

		btntag = "1";
		int xRef = -16;
		int yRef = 9;
		int xDelta = 18;
		int yDelta = 0;
		SHSFW_msgRoutine.SDISP_buttonMessage(btntag, xRef += xDelta,
				yRef += yDelta, wBtn, FontSize, fontColor, BtnColor, 0);
		btn1 = (Button) rl.findViewWithTag(btntag);
		btn1.setText(SHSFW_btnText(1));
		btn1.setOnClickListener(this);

		btntag = "2";
		int xRef1 = -16;
		int yRef1 = 9;
		int xDelta1 = 36;
		int yDelta1 = 0;
		SHSFW_msgRoutine.SDISP_buttonMessage(btntag, xRef1 += xDelta1,
				yRef1 += yDelta1, wBtn, FontSize, fontColor, BtnColor, 0);
		btn2 = (Button) rl.findViewWithTag(btntag);
		btn2.setText(SHSFW_btnText(2));
		btn2.setOnClickListener(this);

		btntag = "3";
		int xRef2 = -16;
		int yRef2 = 0;
		int xDelta2 = 54;
		int yDelta2 = 9;
		SHSFW_msgRoutine.SDISP_buttonMessage(btntag, xRef2 += xDelta2,
				yRef2 += yDelta2, wBtn, FontSize, fontColor, BtnColor, 0);
		btn3 = (Button) rl.findViewWithTag(btntag);
		btn3.setText(SHSFW_btnText(3));
		btn3.setOnClickListener(this);



		btntag = "4";
		int xRef3 = -16;
		int yRef3 = 9;
		int xDelta3 = 72;
		int yDelta3 = 0;
		SHSFW_msgRoutine.SDISP_buttonMessage(btntag, xRef3 += xDelta3,
				yRef3 += yDelta3, wBtn, FontSize, fontColor, BtnColor, 0);
		btn4 = (Button) rl.findViewWithTag(btntag);
		btn4.setText(SHSFW_btnText(4));
		btn4.setOnClickListener(this);


		btntag = "5";
		int xRef4 = -16;
		int yRef4 = 9;
		int xDelta4 = 18;
		int yDelta4 = 1;
		SHSFW_msgRoutine
		.SDISP_buttonMessage(btntag, xRef4 += xDelta4, yRef4 += yDelta4
		+ hBtn, wBtn, FontSize, fontColor, BtnColor, 0);
		btn5 = (Button) rl.findViewWithTag(btntag);
		btn5.setText(SHSFW_btnText(5));
		btn5.setOnClickListener(this);

		btntag = "6";
		int xRef5 = -16;
		int yRef5 = 9;
		int xDelta5 = 36;
		int yDelta5 = 1;
		SHSFW_msgRoutine
		.SDISP_buttonMessage(btntag, xRef5 += xDelta5, yRef5 += yDelta5
		+ hBtn, wBtn, FontSize, fontColor, BtnColor, 0);
		btn6 = (Button) rl.findViewWithTag(btntag);

		btn6.setText(SHSFW_btnText(6));
		btn6.setOnClickListener(this);

		btntag = "7";
		int xRef6 = -16;
		int yRef6 = 9;
		int xDelta6 = 54;
		int yDelta6 = 1;
		SHSFW_msgRoutine
		.SDISP_buttonMessage(btntag, xRef6 += xDelta6, yRef6 += yDelta6
		+ hBtn, wBtn, FontSize, fontColor, BtnColor, 0);
		btn7 = (Button) rl.findViewWithTag(btntag);

		btn7.setText(SHSFW_btnText(7));
		btn7.setOnClickListener(this);


		btntag = "8";
		int xRef7 = -16;
		int yRef7 = 9;
		int xDelta7 = 72;
		int yDelta7 = 1;
		SHSFW_msgRoutine
		.SDISP_buttonMessage(btntag, xRef7 += xDelta7, yRef7 += yDelta7
		+ hBtn, wBtn, FontSize, fontColor, BtnColor, 0);
		btn8 = (Button) rl.findViewWithTag(btntag);

		btn8.setText(SHSFW_btnText(8));
		btn8.setOnClickListener(this);

		btntag = "9";
		int xRef8 = -16;
		int yRef8 = 9;
		int xDelta8 = 18;
		int yDelta8 = 2;
		SHSFW_msgRoutine
		.SDISP_buttonMessage(btntag, xRef8 += xDelta8, yRef8 += yDelta8
		+ hBtn+hBtn, wBtn, FontSize, fontColor, BtnColor, 0);
		btn9 = (Button) rl.findViewWithTag(btntag);
		btn9.setText(SHSFW_btnText(9));
		btn9.setOnClickListener(this);

		btntag = "10";
		int xRef9 = -16;
		int yRef9 = 9;
		int xDelta9 = 36;
		int yDelta9 = 2;
		SHSFW_msgRoutine
		.SDISP_buttonMessage(btntag, xRef9 += xDelta9, yRef9 += yDelta9
		+ hBtn+hBtn, wBtn, FontSize, fontColor, BtnColor, 0);
		btn10 = (Button) rl.findViewWithTag(btntag);
		btn10.setText(SHSFW_btnText(10));
		btn10.setOnClickListener(this);

		btntag = "11";
		int xRef10 = -16;
		int yRef10 = 9;
		int xDelta10 = 54;
		int yDelta10 = 2;
		SHSFW_msgRoutine
		.SDISP_buttonMessage(btntag, xRef10 += xDelta10, yRef10 += yDelta10
		+ hBtn+hBtn, wBtn, FontSize, fontColor, BtnColor, 0);
		btn11 = (Button) rl.findViewWithTag(btntag);
		btn11.setText(SHSFW_btnText(11));
		btn11.setOnClickListener(this);

		btntag = "12";
		int xRef11 = -16;
		int yRef11 = 9;
		int xDelta11 = 72;
		int yDelta11 = 2;
		SHSFW_msgRoutine
		.SDISP_buttonMessage(btntag, xRef11 += xDelta11, yRef11 += yDelta11
		+ hBtn+hBtn, wBtn, FontSize, fontColor, BtnColor, 0);
		btn12 = (Button) rl.findViewWithTag(btntag);
		btn12.setText(SHSFW_btnText(12));
		btn12.setOnClickListener(this);


		btntag = "13";
		int xRef12 = -16;
		int yRef12 = 9;
		int xDelta12 = 18;
		int yDelta12 = 3;
		SHSFW_msgRoutine
		.SDISP_buttonMessage(btntag, xRef12 += xDelta12, yRef12 += yDelta12
		+ hBtn+hBtn+hBtn, wBtn, FontSize, fontColor, BtnColor, 0);
		btn13 = (Button) rl.findViewWithTag(btntag);
		btn13.setText(SHSFW_btnText(13));
		btn13.setOnClickListener(this);

		btntag = "14";
		int xRef13 = -16;
		int yRef13 = 9;
		int xDelta13 = 36;
		int yDelta13 = 3;
		SHSFW_msgRoutine
		.SDISP_buttonMessage(btntag, xRef13 += xDelta13, yRef13 += yDelta13
		+ hBtn+hBtn+hBtn, wBtn, FontSize, fontColor, BtnColor, 0);
		btn14 = (Button) rl.findViewWithTag(btntag);
		btn14.setText(SHSFW_btnText(14));
		btn14.setOnClickListener(this);

		btntag = "15";
		int xRef14 = -16;
		int yRef14 = 9;
		int xDelta14 = 54;
		int yDelta14 = 3;
		SHSFW_msgRoutine
		.SDISP_buttonMessage(btntag, xRef14 += xDelta14, yRef14 += yDelta14
		+ hBtn+hBtn+hBtn, wBtn, FontSize, fontColor, BtnColor, 0);
		btn15 = (Button) rl.findViewWithTag(btntag);
		btn15.setText(SHSFW_btnText(15));
		btn15.setOnClickListener(this);

		btntag = "16";
		int xRef15 = -16;
		int yRef15 = 9;
		int xDelta15 = 72;
		int yDelta15 = 3;
		SHSFW_msgRoutine
		.SDISP_buttonMessage(btntag, xRef15 += xDelta15, yRef15 += yDelta15
		+ hBtn+hBtn +hBtn, wBtn, FontSize, fontColor, BtnColor, 0);
		btn16 = (Button) rl.findViewWithTag(btntag);
		btn16.setText(SHSFW_btnText(16));
		btn16.setOnClickListener(this);

	}


	//Note:- Used for set text on button

	public String SHSFW_btnText(int btntag) {
		String btnText = "";
		System.out.println("" + btntag);
		int sizeOfAvailableLanguages = SHSFW_AvailableLanguages.size();
		if (btntag <= sizeOfAvailableLanguages) {
			btnText = SHSFW_AvailableLanguagesKey.get(btntag - 1);
		}
		//ranjan
		if(btnText.contains("English")){
			finalCount=tempCount+1;
		}else{
			tempCount+=1;
		}
		countCharacters(btnText);
		if(countString>3){
			languageButtons.add((Button)rl.findViewWithTag(this.btntag));
		}else{
			emptyButtons.add((Button)rl.findViewWithTag(this.btntag));
		}

		// System.out.println("befor return"+btnText);
		return btnText;
	}

	private void countCharacters(String btnText) {
		// TODO Auto-generated method stub
		countString=0;
		for(int i=0;i<btnText.length();i++){
			countString+=1;
		}
		//return countString;

	}

	private void changeLanguage(int langCode, String locale) {
		// changing lang in GRID

		System.out.println("Inside langCode changeLanguage" + langCode);
		System.out.println("Inside langCode changeLanguage" + locale);
		SHSFW_SCFG.SHSFW_SDISP_languageCode = langCode;
		SDISP_MessageRoutines.SDISP_configure(
				SHSFW_SCFG.SHSFW_SDISP_languageCode, SHSFW_SCFG.SDISP_path);

		// changing lang in XML
		Locale locale1 = new Locale(locale);
		Locale.setDefault(locale1);
		Configuration config1 = new Configuration();
		config1.locale = locale1;
		getActivity()
		.getBaseContext()
		.getResources()
		.updateConfiguration(
				config1,
				getActivity().getBaseContext().getResources()
				.getDisplayMetrics());



		//Note:-Show version number and 

		String versionStr = SUIRES_routine_resources.SUIRES_getMessage(300)
				.getSUIRES_message();
		String deviceIDStr = SUIRES_routine_resources.SUIRES_getMessage(301)
				.getSUIRES_message();
		String asoId = SUIRES_routine_resources.SUIRES_getMessage(294)
				.getSUIRES_message();
		lang.setText(versionStr+":"+SHSFW_SCFG_AboutAS.SCFG_AbtAS_SWversion+
				", "+deviceIDStr+":"
				+", "+asoId+":");

		/*
		 * lang.setText("Current language - " +
		 * getLanguageStr(SHSFW_SCFG.SHSFW_SDISP_languageCode));
		 */
	}

	/*private String getLanguageStr(int languageCode) {
		String lang = "";

		switch (languageCode) {
		case 00:
			lang = "English";
			break;

		case 01:
			lang = "Assamese";
			break;

		case 02:
			lang = "Bangla";
			break;

		case 05:
			lang = "Gujarati";
			break;

		case 06:

			lang = "Hindi";
			break;

		case 07:
			lang = "Kannada";
			break;

		case 11:
			lang = "Malayalam";
			break;

		case 12:
			lang = "Manipuri";
			break;

		case 13:
			lang = "Marathi";
			break;

		case 15:
			lang = "Oriya";
			break;

		case 16:
			lang = "Punjabi";
			break;

		case 20:
			lang = "Tamil";
			break;

		case 21:
			lang = "Telugu";
			break;

		default:
			break;
		}

		return lang;
	}
	 */
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			passer = (SHSFW_InputInterface) activity;

		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement SHSFW_InputInterface");
		}
	}

	@Override
	public void onStart() {
		super.onStart();

		// passer.SHSFW_configHotKey(SHSFW_Operation.MORE, new
		// SHSFW_OperationMap(SHSFW_Operation.MORE, 45, false, true));
		passer.SHSFW_IConfigHotKey(SHSFW_Operation.MORE, false);
	}

	// Note:- 5 apr 2014
	// Used to return language value in integer from hash map like: key is
	// English and code is 00 or 0
	public int SHSFW_AvailableLanguagesvalue(String key) {
		int value = (int) SHSFW_AvailableLanguages.get(key);
		return value;
	}

	// Note:- 5 apr 2014
	// Used to return language code from hard coded system. here there is 13
	// case for is

	public String SHSFW_LanguagesCode(int value) {
		// System.out.println(value);
		String langCode = "";
		switch (value) {
		case 00:
			langCode = "en";
			break;

		case 01:
			langCode = "as";
			break;

		case 02:
			langCode = "bn";
			break;

		case 05:
			langCode = "gu";
			break;

		case 06:
			langCode = "hi";
			break;

		case 07:
			langCode = "kn";
			break;

		case 11:
			langCode = "ml";
			break;

		case 12:
			langCode = "";
			break;

		case 13:
			langCode = "mr";
			break;

		case 15:
			langCode = "or";
			break;

		case 16:
			langCode = "pa";
			break;

		case 20:
			langCode = "ta";
			break;

		case 21:
			langCode = "te";
			break;

		default:
			// System.out.println("default"+value);
			break;
		}
		// System.out.println("06 "+value +"before return "+langCode);
		return langCode;

	}

	public void SHSFW_resource_config() {
		SUIRES_ROUTINE_Resources resource = SUIRES_ROUTINE_Resources
				.SUIRES_getResourceInstance(getActivity());
		SHSFW_AvailableLanguages = resource.SUIRES_getAvailableLanguages()
				.getSUIRES_availableLanguages();
		SHSFW_AvailableLanguagesKey = new ArrayList<String>(
				SHSFW_AvailableLanguages.keySet());
	}

	//Note:- Change language on click of button

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		int sizeOfAvailableLanguages = SHSFW_AvailableLanguages.size();
		String buttonTag = v.getTag().toString();
		//ranjan
		removeColor();
		for(int i=0;i<languageButtons.size();i++){
			if(languageButtons.get(i).getTag().equals(v.getTag())){
				v.setBackgroundColor(Color.parseColor("#66CC00"));
				SHSFW_SCFG.tag=v.getTag().toString();
			}
		} 
		for(int i=0;i<emptyButtons.size();i++){
			if(emptyButtons.get(i).getTag().equals(v.getTag())){
				rl.findViewWithTag(SHSFW_SCFG.tag).setBackgroundColor(Color.parseColor("#66CC00"));
			}
		} 
		
		//System.out.println("INSIDE ONCLICK" + buttonTag);
		int buttonNumber=Integer.parseInt(buttonTag);
		if( buttonNumber<= sizeOfAvailableLanguages){
			changeLanguage(
					SHSFW_AvailableLanguages.get(SHSFW_AvailableLanguagesKey
							.get(buttonNumber-1)),
							SHSFW_LanguagesCode(SHSFW_AvailableLanguages
									.get(SHSFW_AvailableLanguagesKey.get(buttonNumber-1))));
		}
	}

	private void removeColor() {
		for(int i=0;i<languageButtons.size();i++){
			languageButtons.get(i).setBackgroundColor(Color.parseColor("#90CAF9"));
		}
	}
}
