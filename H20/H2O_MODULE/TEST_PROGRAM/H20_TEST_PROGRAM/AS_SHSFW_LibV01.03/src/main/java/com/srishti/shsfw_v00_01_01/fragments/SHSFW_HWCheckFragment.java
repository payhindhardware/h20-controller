package com.srishti.shsfw_v00_01_01.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.srishti.shsfw_v00_01_01.interfaces.SHSFW_InputInterface;
import com.srishti.sprn.SPRN_Print;
import com.srishti.sprn.SPRN_ReturnCode;
import com.srishti.ui.message_routines.SDISP_MessageRoutines;

public class SHSFW_HWCheckFragment extends Fragment{

	SHSFW_InputInterface passer;
	private SDISP_MessageRoutines SHSFW_msgRoutine;

	Button fp, iris, printer, settings, exitToAndroid;
	TextView fpStatus, irisStatus, prnStatus;
	private String userRole="";

	SPRN_Print prnObj;

	private String role = "";
	private SharedPreferences sharedPref;

	static{
		System.loadLibrary("startek_jni");
		System.loadLibrary("startek");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		prnObj = SPRN_Print.SPRN_getInstance(getActivity());

		RelativeLayout baseLayout = new RelativeLayout(getActivity());
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		baseLayout.setLayoutParams(params);

		SHSFW_msgRoutine = SDISP_MessageRoutines.SDISP_getInstance(
				getActivity(), baseLayout, 0);

		int btnTextColor = 7;
		int btnBgColor = 2;
		int btnWidth = 20;
		int btnHeight = 10;
		int btnFontSize = 2;

		int txtMsgID = 143;
		int txtFontSize = 1;
		int txtTextColor = 1;

		sharedPref = getActivity().getSharedPreferences("ekycpref", Context.MODE_PRIVATE);
		role = sharedPref.getString("LoggedUserType", "NONE");
		userRole=sharedPref.getString("LoginUserLevel", "NONE");
		SHSFW_msgRoutine.SDISP_message("txtHW", 3, 1, 1, 3, btnTextColor);
		TextView hw = (TextView) baseLayout.findViewWithTag("txtHW");
		hw.setText("Hardware check");

		// FP
		SHSFW_msgRoutine.SDISP_buttonMessage("fp", 4, 10, btnWidth, btnHeight, btnFontSize, btnTextColor, btnBgColor, 3);
		fp = (Button) baseLayout.findViewWithTag("fp");
		fp.setText("Fingerprint");
		fp.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				try {
					initFP(false);	
				} catch (Exception e) {

				}

			}
		});
		SHSFW_msgRoutine.SDISP_message("txtfp", txtMsgID, 4, 21, txtFontSize, txtTextColor);
		fpStatus = (TextView) baseLayout.findViewWithTag("txtfp");
		fpStatus.setText(fpStatus.getText().toString()+": Failure");

		//iris
		SHSFW_msgRoutine.SDISP_buttonMessage("iris", 53, 10, btnWidth, btnHeight, btnFontSize, btnTextColor, btnBgColor, 3);
		iris = (Button) baseLayout.findViewWithTag("iris");
		iris.setText("Iris");
		iris.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				try {
					initIris(false);
				} catch (Exception e) {

				}
			}
		});
		SHSFW_msgRoutine.SDISP_message("txtiris", txtMsgID, 53, 21, txtFontSize, txtTextColor);
		irisStatus = (TextView) baseLayout.findViewWithTag("txtiris");
		irisStatus.setText(irisStatus.getText().toString()+": Failure");

		//printer
		SHSFW_msgRoutine.SDISP_buttonMessage("prn", 4, 30, btnWidth, btnHeight, btnFontSize, btnTextColor, btnBgColor, 3);
		printer = (Button) baseLayout.findViewWithTag("prn");
		printer.setText("Printer");
		printer.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				try {
					initPrn(false);
				} catch (Exception e) {

				}
			}
		});
		SHSFW_msgRoutine.SDISP_message("txtprn", txtMsgID, 4, 41, txtFontSize, txtTextColor);
		prnStatus = (TextView) baseLayout.findViewWithTag("txtprn");
		prnStatus.setText(prnStatus.getText().toString()+": Failure");

		//exit to android
		SHSFW_msgRoutine.SDISP_buttonMessage("exit", 29, 30, btnWidth, btnHeight, 1, btnTextColor, btnBgColor, 3);
		exitToAndroid = (Button) baseLayout.findViewWithTag("exit");
		exitToAndroid.setText("Exit to android");
		exitToAndroid.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				try {
					Toast.makeText(getActivity(), "Exit to android", Toast.LENGTH_SHORT).show();
					Intent startMain = new Intent(Intent.ACTION_MAIN);
					startMain.addCategory(Intent.CATEGORY_HOME);
					startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(startMain);
					android.os.Process.killProcess(android.os.Process.myPid());
					System.exit(0);
				} catch (Exception e) {

				}
			}
		});
		checkButtonStatus(exitToAndroid, role,userRole);

		//settings
		SHSFW_msgRoutine.SDISP_buttonMessage("setting", 53, 30, btnWidth, btnHeight, btnFontSize, btnTextColor, btnBgColor, 3);
		settings = (Button) baseLayout.findViewWithTag("setting");
		settings.setText("Settings");
		settings.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				//Toast.makeText(getActivity(), "Settings", Toast.LENGTH_SHORT).show();
				Intent intent = new Intent(Settings.ACTION_SETTINGS);				
				intent.putExtra("extra_prefs_show_button_bar", true);
				startActivity(intent);
			}
		});

		return baseLayout;
	}

private void checkButtonStatus(Button btn, String role, String userRole) {
		
		//Toast.makeText(getActivity(), "Role = "+role, Toast.LENGTH_SHORT).show();
		if(role.equalsIgnoreCase("admin")&&userRole.equalsIgnoreCase("L1")){
			btn.setEnabled(true);
			btn.setVisibility(View.VISIBLE);
		}else {
			btn.setVisibility(View.GONE);
			btn.setEnabled(false);
		}
		
	}

	public void autoCheck() {
		try {
			initFP(true);
		} catch (Exception e) {

		}

	}

	public void initFP(final boolean bool) throws Exception{

		UpdateUI(fp, true);
		UpdateUI(iris, false);
		UpdateUI(printer, false);
		UpdateUI(settings, false);
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					UpdateUI(fpStatus, "Checking...", false);				

					//fpObj.SFP_disconnectDevice();


					UpdateUI(fp, true);
					UpdateUI(iris, true);
					UpdateUI(printer, true);
					UpdateUI(settings, true);
					if(bool){
						initIris(true);
					}
				} catch (Exception e) {
					UpdateUI(fpStatus, "Status : Failure", false);
				}

			}
		}).start();

	}

	public void initIris(final boolean bool) throws Exception {

		UpdateUI(fp, false);
		UpdateUI(iris, true);
		UpdateUI(printer, false);
		UpdateUI(settings, false);
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					UpdateUI(irisStatus, "Checking...", false);


						UpdateUI(irisStatus, "Status : Success", false);


					UpdateUI(fp, true);
					UpdateUI(iris, true);
					UpdateUI(printer, true);
					UpdateUI(settings, true);
					if(bool){
						initPrn(false);
					}
				} catch (Exception e) {
					UpdateUI(irisStatus, "Status : Failure", false);
				}
			}
		}).start();

	}

	public void initPrn(final boolean bool) throws Exception {

		UpdateUI(fp, false);
		UpdateUI(iris, false);
		UpdateUI(printer, true);
		UpdateUI(settings, false);
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					UpdateUI(prnStatus, "Checking...", false);
					int initRet = prnObj.SPRN_initializePrinter();

					if(initRet == SPRN_ReturnCode.SPRN_SUCCESS){
						UpdateUI(prnStatus, "Status : Success", false);
					}else {
						UpdateUI(prnStatus, "Status : Failure", false);
					}

					UpdateUI(fp, true);
					UpdateUI(iris, true);
					UpdateUI(printer, true);
					UpdateUI(settings, true);
					if(bool){
						// do something
					}
				} catch (Exception e) {
					UpdateUI(prnStatus, "Status : Failure", false);
				}
			}
		}).start();
	}

	public void UpdateUI(final TextView txtview, final String str, final boolean append ){
		if(getActivity() != null){
			getActivity().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					if(txtview != null){
						if(append){
							txtview.append(str);
						}else {
							txtview.setText(str);
						}
					}
				}
			});
		}
	}

	public void UpdateUI(final Button btn, final boolean isEnabled){
		if(getActivity() != null){
			getActivity().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					if(btn != null){
						btn.setEnabled(isEnabled);
					}
				}
			});
		}
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			passer = (SHSFW_InputInterface) activity;

		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement SHSFW_InputInterface");
		}
	}

	@Override
	public void onStart() {
		super.onStart();

		autoCheck();

	}
}
