package com.srishti.shsfw_v00_01_01.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.widget.Toast;

public class SHSFW_LoginState {

	private static SharedPreferences SHSFW_pref;
	private static Editor SHSFW_editor;

	private static Context SHSFW_context;

	private static final String PREF_NAME = "login_state";
	private static final String PREF_KEY = "state";

	public static enum SHSFW_LoginStates {
		LOGGED_OUT(1), LOGGED_IN(2);

		private int value;

		private SHSFW_LoginStates(int value) {
			this.value = value;
		}

		public int getValue() {
			return this.value;
		}
	}

	/* Singleton begins */
	private static SHSFW_LoginState SHSFW_stateInstance = null;

	public static SHSFW_LoginState getInstance(Context SHSFW_context) {
		SHSFW_LoginState.SHSFW_context = SHSFW_context;
		SHSFW_pref = SHSFW_context.getSharedPreferences(PREF_NAME,
				Context.MODE_PRIVATE);

		if (SHSFW_stateInstance == null) {
			synchronized (SHSFW_LoginState.class) {
				if (SHSFW_stateInstance == null) {
					SHSFW_stateInstance = new SHSFW_LoginState();
				}
			}
		}
		return SHSFW_stateInstance;
	}

	public static void clearInstance() {
		if (SHSFW_stateInstance != null) {
			SHSFW_stateInstance = null;
		}
	}

	private SHSFW_LoginState() {

	}

	/* Singleton ends */

	private SHSFW_LoginStates SHSFW_state = SHSFW_LoginStates.LOGGED_OUT;

	public SHSFW_LoginStates getSHSFW_state() {
		if (SHSFW_pref == null) {
			SHSFW_pref = SHSFW_context.getSharedPreferences(PREF_NAME,
					Context.MODE_PRIVATE);
		}

		String state = SHSFW_pref.getString(PREF_KEY,
				SHSFW_LoginStates.LOGGED_OUT.toString());
		SHSFW_state = mapStateToEnum(state);
		return SHSFW_state;
	}

	public void setSHSFW_state(SHSFW_LoginStates sHSFW_state) {
		
		if (SHSFW_pref == null) {
			SHSFW_pref = SHSFW_context.getSharedPreferences(PREF_NAME,
					Context.MODE_PRIVATE);
			SHSFW_editor = SHSFW_pref.edit();
		}
		SHSFW_editor = SHSFW_pref.edit();
		
		SHSFW_editor.putString(PREF_KEY, sHSFW_state.toString());
		SHSFW_editor.apply();
		
		SHSFW_state = sHSFW_state;
	}
	
	private SHSFW_LoginStates mapStateToEnum(String state) {

		SHSFW_LoginStates mappedState;

		if (state.equalsIgnoreCase("LOGGED_OUT")) {
			mappedState = SHSFW_LoginStates.LOGGED_OUT;
		//	Toast.makeText(SHSFW_context, "Framwork SHSFW_LoginStates mappedState "+mappedState,Toast.LENGTH_SHORT).show();

		} else if (state.equalsIgnoreCase("LOGGED_IN")) {
			mappedState = SHSFW_LoginStates.LOGGED_IN;
		} else {
			mappedState = null;
		}

		return mappedState;
	}

}
