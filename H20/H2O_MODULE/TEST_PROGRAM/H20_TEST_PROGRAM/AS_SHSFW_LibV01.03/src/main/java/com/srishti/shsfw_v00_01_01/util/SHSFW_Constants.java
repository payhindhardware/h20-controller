package com.srishti.shsfw_v00_01_01.util;



public interface SHSFW_Constants {

	public static final String SHSFW_PREF = "shsfw_pref" ;
	public static final String SERVER_URL = "server_url";
	public static final String SERVER_RESPONSE_TIMEOUT = "server_timeout";
	public static final String SERVER_DEFAULT_URL = "http://scidr.azurewebsites.net/SrishtiServer3.01/services";
	public static final String SERVER_DEFAULT_RESPONSE_TIMEOUT = "60000";
	
}
