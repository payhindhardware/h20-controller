package com.srishti.shsfw_v00_01_01.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.srishti.shsfw_v00_01_01.interfaces.SHSFW_InputInterface;
import com.srishti.shsfw_v00_01_01.util.SHSFW_Constants;
import com.srishti.ui.message_routines.SDISP_MessageRoutines;

public class SHSFW_PreferenceFragment extends Fragment{

	SHSFW_InputInterface passer;
	private SDISP_MessageRoutines SHSFW_dispRoutine;
	private RelativeLayout SHSFW_parentRelativeLayout = null;
	SharedPreferences sharedpreferences;

	EditText SHSFW_etServerUrl;
	EditText SHSFW_etServerTimeout;
	Button SHSFW_btnSavePref;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		SHSFW_parentRelativeLayout = new RelativeLayout(getActivity());
		RelativeLayout.LayoutParams parentParams = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT);
		SHSFW_parentRelativeLayout.setLayoutParams(parentParams);

		SHSFW_dispRoutine = SDISP_MessageRoutines
				.SDISP_getInstance(getActivity(), SHSFW_parentRelativeLayout, 0);


		sharedpreferences = getActivity().getSharedPreferences(SHSFW_Constants.SHSFW_PREF, Context.MODE_PRIVATE);

		/**Hint to For entering server url*/
		String SDISP_textViewTag = "LOGIN_tvServerUrl";
		int SDISP_xCordinate = 5;
		int SDISP_yCordinate = 2;
		int SDISP_fontSize = 2;
		int SDISP_msgId2 = 139;
		int SDISP_fontColorCode = 7;

		SHSFW_dispRoutine.SDISP_message(SDISP_textViewTag, SDISP_msgId2, SDISP_xCordinate, 
				SDISP_yCordinate, SDISP_fontSize, SDISP_fontColorCode);


		/**Edit text for server url*/
		String TP_SDISP_etViewTag = "LOGIN_etServerUrl";
		int TP_SDISP_etViewXCordinate = 5;
		int TP_SDISP_etViewYCordinate = 9;
		int TP_SDISP_etViewWidth = 65;
		int TP_SDISP_etViewFontSize = 2;
		int TP_SDISP_etViewFontColorCode = 7;
		int TP_SDISP_etViewHeight = 8;
		int TP_SDISP_etViewHintMessageId = 139;
		int SDISP_etViewType = InputType.TYPE_CLASS_TEXT;
		int SDISP_charLimitURL = 100;


		SHSFW_dispRoutine.SDISP_editText(TP_SDISP_etViewTag, SDISP_etViewType, TP_SDISP_etViewXCordinate,
				TP_SDISP_etViewYCordinate, TP_SDISP_etViewWidth, TP_SDISP_etViewHeight, TP_SDISP_etViewFontSize,
				TP_SDISP_etViewFontColorCode, TP_SDISP_etViewHintMessageId, SDISP_charLimitURL);

		SHSFW_etServerUrl = (EditText) SHSFW_parentRelativeLayout.findViewWithTag(TP_SDISP_etViewTag);
		
		String url = sharedpreferences.getString(SHSFW_Constants.SERVER_URL, SHSFW_Constants.SERVER_DEFAULT_URL);
		SHSFW_etServerUrl.setText(url);






		/**Hint to For entering server response timeout*/
		SDISP_textViewTag = "LOGIN_tvServerTimeout";
		SDISP_xCordinate = 5;
		SDISP_yCordinate = 19;
		SDISP_fontSize = 2;
		SDISP_msgId2 = 149;
		SDISP_fontColorCode = 7;

		SHSFW_dispRoutine.SDISP_message(SDISP_textViewTag, SDISP_msgId2, SDISP_xCordinate, 
				SDISP_yCordinate, SDISP_fontSize, SDISP_fontColorCode);


		/**Edit text for server url*/
		TP_SDISP_etViewTag = "LOGIN_etServerTimeout";
		TP_SDISP_etViewXCordinate = 5;
		TP_SDISP_etViewYCordinate = 26;
		TP_SDISP_etViewWidth = 65;
		TP_SDISP_etViewFontSize = 2;
		TP_SDISP_etViewFontColorCode = 7;
		TP_SDISP_etViewHeight = 8;
		TP_SDISP_etViewHintMessageId = 149;
		SDISP_etViewType = InputType.TYPE_CLASS_NUMBER;
		int SDISP_charLimitTimeout = 10;


		SHSFW_dispRoutine.SDISP_editText(TP_SDISP_etViewTag, SDISP_etViewType, TP_SDISP_etViewXCordinate,
				TP_SDISP_etViewYCordinate, TP_SDISP_etViewWidth, TP_SDISP_etViewHeight, TP_SDISP_etViewFontSize,
				TP_SDISP_etViewFontColorCode, TP_SDISP_etViewHintMessageId, SDISP_charLimitTimeout);

		SHSFW_etServerTimeout = (EditText) SHSFW_parentRelativeLayout.findViewWithTag(TP_SDISP_etViewTag);

		String server_timeout = sharedpreferences.getString(SHSFW_Constants.SERVER_RESPONSE_TIMEOUT, SHSFW_Constants.SERVER_DEFAULT_RESPONSE_TIMEOUT);
		SHSFW_etServerTimeout.setText(server_timeout);


		String TP_SDISP_btnTextTag = "SDISP_btnSavePref";
		int TP_SDISP_btnTextXCordinate = 30;
		int TP_SDISP_btnTextYCordinate = 35;
		int TP_SDISP_btnTextWidth = 15;
		int TP_SDISP_btnTextfontSize = 2;
		int TP_SDISP_btnTextColorCode = 7;
		int TP_SDISP_btnTextBackgroundColorCode = 7;
		int TP_SDISP_btnTextmessageId = 134;

		/*
		 * Usage: Call button with message routine.
		 */
		SHSFW_dispRoutine.SDISP_buttonMessage(TP_SDISP_btnTextTag, TP_SDISP_btnTextXCordinate,
				TP_SDISP_btnTextYCordinate, TP_SDISP_btnTextWidth, TP_SDISP_btnTextfontSize, TP_SDISP_btnTextColorCode,
				TP_SDISP_btnTextBackgroundColorCode, TP_SDISP_btnTextmessageId);


		SHSFW_btnSavePref = (Button) SHSFW_parentRelativeLayout.findViewWithTag(TP_SDISP_btnTextTag);

		SHSFW_btnSavePref.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {


				save();

			}
		});


		return SHSFW_parentRelativeLayout;
	}

	private void save(){

		Editor editor = sharedpreferences.edit();
		editor.putString(SHSFW_Constants.SERVER_URL, SHSFW_etServerUrl.getText().toString().trim());
		editor.putString(SHSFW_Constants.SERVER_RESPONSE_TIMEOUT, SHSFW_etServerTimeout.getText().toString().trim());
		editor.commit();

	}


	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		try {
			passer = (SHSFW_InputInterface) activity;

		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement OnArticleSelectedListener");
		}
	}

	@Override
	public void onStart() {
		super.onStart();

		//passer.SHSFW_IConfigMessages(headerFunctionName, headerSubFunctionName, uidMsg, headerMsgFontColor);
	}
}
