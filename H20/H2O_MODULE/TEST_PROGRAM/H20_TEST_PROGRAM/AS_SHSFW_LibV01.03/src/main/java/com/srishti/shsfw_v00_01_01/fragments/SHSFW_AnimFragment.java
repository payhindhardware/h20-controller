package com.srishti.shsfw_v00_01_01.fragments;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

import com.srishti.shsfw_v00_01_01.R;
import com.srishti.shsfw_v00_01_01.interfaces.SHSFW_InputInterface;
import com.srishti.suires.SUIRES_ROUTINE_Resources;

public class SHSFW_AnimFragment extends Fragment{


	public int currentimageindex=0;
	
	SHSFW_InputInterface passer;
	//	Timer timer;
	//	TimerTask task;
	ImageView slidingimage;
	
	//Dell version changing place
	//int imgWidth = 30;
	int imgWidth = 150;

	private int[] IMAGE_IDS = {
		2, 5
	};
	private SUIRES_ROUTINE_Resources SUIRES_routine_resources;

	private RelativeLayout SHSFW_parentRelativeLayout;

	
	private static Timer timer = new Timer();

	//private static Timer timer = new Timer();

	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState){
		
		
		SHSFW_parentRelativeLayout = new RelativeLayout(getActivity());
		RelativeLayout.LayoutParams parentParams = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT);
		SHSFW_parentRelativeLayout.setLayoutParams(parentParams);
		
		SUIRES_routine_resources = SUIRES_ROUTINE_Resources.SUIRES_getResourceInstance(getActivity());
		
		slidingimage = new ImageView(getActivity());
		
		RelativeLayout.LayoutParams SDISP_params1 = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT);
		SDISP_params1.setMargins(0, 50, 0, 0);
		slidingimage.setLayoutParams(SDISP_params1);
		
		slidingimage.setImageBitmap(SUIRES_routine_resources.SUIRES_getImage(2, imgWidth).getSUIRES_image());
		
		SHSFW_parentRelativeLayout.addView(slidingimage);
		
		
		return SHSFW_parentRelativeLayout;
	}
	
	public void SHSFW_startAnimation(int[] SHSFW_imageIds) {

		IMAGE_IDS = SHSFW_imageIds;
		
		timer.cancel();
		timer = new Timer();

		final Handler mHandler = new Handler();

		// Create runnable for posting
		final Runnable mUpdateResults = new Runnable() {
			public void run() {

			AnimateandSlideShow();

			}
		};

		int delay = 1000; // delay for 1 sec.
		int period = 7000;
		//int period = 15000; // repeat every 4 sec.


		timer.scheduleAtFixedRate(new TimerTask() {

			public void run() {

				mHandler.post(mUpdateResults);

			}

		}, delay, period);

	}

	/**
	 * Helper method to start the animation on the splash screen
	 */
	private void AnimateandSlideShow() {


		slidingimage.setImageBitmap(SUIRES_routine_resources.SUIRES_getImage(IMAGE_IDS[currentimageindex%IMAGE_IDS.length], imgWidth).getSUIRES_image());

		currentimageindex++;

		Animation rotateimage = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in);
		slidingimage.startAnimation(rotateimage);



	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		try {
			passer = (SHSFW_InputInterface) activity;

		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement OnArticleSelectedListener");
		}
	}

	@Override
	public void onStart() {
		super.onStart();

		//SHSFW_startAnimation(new int[]{2});
		//passer.SHSFW_IConfigMessages(headerFunctionName, headerSubFunctionName, uidMsg, headerMsgFontColor);
	}
	
	
	
	
	
	
	
	
	

}
