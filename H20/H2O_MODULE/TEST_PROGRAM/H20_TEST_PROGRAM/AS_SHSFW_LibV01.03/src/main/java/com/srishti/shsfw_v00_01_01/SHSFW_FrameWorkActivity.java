package com.srishti.shsfw_v00_01_01;

import java.util.ArrayList;
import java.util.HashMap;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;


import com.srishti.shsfw_v00_01_01.fragments.SHSFW_AnimFragment;
import com.srishti.shsfw_v00_01_01.fragments.SHSFW_MoreFragment;
import com.srishti.shsfw_v00_01_01.interfaces.SHSFW_OutputInterface;
import com.srishti.shsfw_v00_01_01.util.SHSFW_HotkeyState;
import com.srishti.shsfw_v00_01_01.util.SHSFW_InputParams;
import com.srishti.shsfw_v00_01_01.util.SHSFW_LoginState;
import com.srishti.shsfw_v00_01_01.util.SHSFW_SCFG;
import com.srishti.shsfw_v00_01_01.util.SHSFW_LoginState.SHSFW_LoginStates;
import com.srishti.shsfw_v00_01_01.util.SHSFW_OperationMap;
import com.srishti.shsfw_v00_01_01.util.SHSFW_OperationMap.SHSFW_Operation;
import com.srishti.shsfw_v00_01_01.util.SHSFW_PrimaryState;
import com.srishti.shsfw_v00_01_01.util.SHSFW_PrimaryState.SHSFW_PrimaryStates;
import com.srishti.sprn.SPRN_Print;
import com.srishti.suires.SUIRES_ROUTINE_Resources;
import com.srishti.suires_returns.SUIRES_IconResponse;
import com.srishti.ui.message_routines.SDISP_MessageRoutines;
import com.srishti.ui.utils.SDISP_utils;

@SuppressLint("NewApi")
public abstract class SHSFW_FrameWorkActivity extends AppCompatActivity {
	
	Context SHSFW_context;

	public static String LoginClassName = "";
	public static String LogoutClassName = "";
	public static String HomeClassName = "";
	public static String PrimaryClassName = "";

	private static final int ALPHA_ENABLE = 255;
	private static final int ALPHA_DISABLE = 125;
	private static final int FL_CHILD_ID = 1;
	private static final String BG_COLORS_PATH = "/SUIRES/colors/backgroundcolors.csv";	
	private static final String LOG_TAG = "SHSFW_FrameWorkActivity";
	
	public RelativeLayout parent;
	public static FrameLayout child;
	public View header;
	public String SHSFW_unLockUser="";
	public String SHSFW_unLockPassword="";
	public String SHSFW_Responce;
	String UNnotfound;
	String UNPmismatch;
	String UNPnotfound;
	int wIcon = 7;
	

	SPRN_Print printer;


	/**************************************Modified 12 Nov 2015*******************************/
	ImageView tempImgView;

	/*************************************************************************************/

	/************************Modified 09 Nov 15 *********************************************/
	//public static int headerHeight ;
	private int headerHeightInCells = 10;
	public static int headerHeight;

	/*
	 * scrollViewHeightInCells variable controls the height of the display in the C of the
	 * framework. Total h = 60, framework = 11. Hence C = 49. We are considering 4 scrolls 
	 * as the maximum a user would like to scroll, hence limiting to 4 scrolls. Total value
	 * of data that can be displayed = 49 * 4 sections
	 */
	private int scrollViewHeightInCells = 49;
	public static int SCROLLVIEW_HEIGHT ;
	
	/****************************************************************************************/

	public FragmentManager fragManager;

	private HashMap<Integer, String> bgColorMap = null;
	private SDISP_MessageRoutines SHSFW_msgRoutine;
	private SHSFW_InputParams SHSFW_ipParams;
	private SHSFW_HotkeyState[] SHSFW_hotkeyState;

	private ImageView ivTempBtn;
	private TextView tvTempMsg1, tvTempMsg2, tvTempMsg3;
	private String SHSFW_btnTag;

	private WindowManager SBmanager;
	private SHSFW_DisableStatusBar SBview;

	private SHSFW_OutputInterface SHSFW_interface;
	SUIRES_ROUTINE_Resources SUIRES_routine_resources;

	/*String SHSFW_unLockUser;

	String SHSFW_unLockPassword;
*/
	// ICON ID constants
	private static final int ICON_HOME = 10;
	private static final int ICON_LOGIN = 14;
	private static final int ICON_LOGOUT = 15;
	private static final int ICON_LOCK = 48;
	private static final int ICON_UNLOCK = 49;
	private static final int ICON_MORE = 50;
	private static final int ICON_REPORT = 54;
	private static final int ICON_PRIMARY = 51;
	private static final int ICON_SECONDARY = 52;
	private static final int ICON_CUSTOM = 36;

	// ----------------------------------------------------------------------------------------------------
	// OVERRIDDEN METHODS OF THE SUPER CLASS
	// ----------------------------------------------------------------------------------------------------
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		SHSFW_context = this;

		// read the color values from SUIRES folder
		if (bgColorMap == null) {
			bgColorMap = SDISP_utils.SDISP_loadResourcesFromCSV(Environment
					.getExternalStorageDirectory().toString() + BG_COLORS_PATH);
		}

		fragManager = getSupportFragmentManager();

		// relative layout for GRID module.
		parent = new RelativeLayout(this);
		parent.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT));

		// configure GRID module object
		SHSFW_msgRoutine = SDISP_MessageRoutines.SDISP_getInstance(
				getBaseContext(), parent, 0);

		/************************Modified 09 Nov 15 *********************************************/
		headerHeight = (int) (headerHeightInCells*SHSFW_msgRoutine.SDISP_getYPixelsPerCell());
		Log.d("dj", "Header heighgt: "+headerHeight);
		/****************************************************************************************/

		SHSFW_hotkeyState = new SHSFW_HotkeyState[7];

		setUpGlobalExceptionHandler();
		setUpDefaultFramework();

		// This the view container for placing fragments. This is where the
		// modules will place
		// their content. The top margin is set at 70px. This may have to be
		// changed manually
		// to make it display properly for different screens. Currently 70px is
		// determined
		// based on New Aakash Tablet.
		RelativeLayout rlContentBody = new RelativeLayout(this);
		RelativeLayout.LayoutParams bodyParams = new RelativeLayout.LayoutParams(
				new LayoutParams(LayoutParams.MATCH_PARENT,
						LayoutParams.MATCH_PARENT));
		bodyParams.setMargins(0, headerHeight, 0, 0);
		rlContentBody.setLayoutParams(bodyParams);
		parent.addView(rlContentBody);

		ScrollView scView = new ScrollView(this);
		RelativeLayout.LayoutParams scViewParams = new RelativeLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		scViewParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		scViewParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		scView.setLayoutParams(scViewParams);
		rlContentBody.addView(scView);

		child = new FrameLayout(this);
		child.setId(FL_CHILD_ID);

		SCROLLVIEW_HEIGHT = (int) (scrollViewHeightInCells * SHSFW_msgRoutine.SDISP_getYPixelsPerCell());

		ScrollView.LayoutParams fmParams = new ScrollView.LayoutParams(
				LayoutParams.MATCH_PARENT, SCROLLVIEW_HEIGHT);
		child.setLayoutParams(fmParams);
		scView.addView(child);

		child.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				InputMethodManager inputMngr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				View  v1 =  (SHSFW_FrameWorkActivity.this).getCurrentFocus();
				if (v1 == null) 
					return;
				else
					inputMngr.hideSoftInputFromWindow(v1.getWindowToken(), 0);
			}
		});

		setContentView(parent);
	}

	@Override
	protected void onPause() {
		super.onPause();
		SHSFW_enableStatusBar();
	}

	@Override
	protected void onResume() {
		super.onResume();
		SHSFW_disableStatusBar();
	}

	// ----------------------------------------------------------------------------------------------------
	// METHODS EXPOSED AS PART OF THE INTERFACE
	// ----------------------------------------------------------------------------------------------------

	/**
	 * This method is used to register the callback of the framework with the
	 * activity
	 * 
	 * @param SHSFW_interface
	 */
	public void SHSFW_configureCallback(SHSFW_OutputInterface SHSFW_interface) {
		this.SHSFW_interface = SHSFW_interface;
	}

	/**
	 * This method is used to configure the logo in the top left corner
	 * 
	 * @param SHSFW_logo
	 * @param SHSFW_logoBGColor
	 */
	public void SHSFW_configureLogo(int SHSFW_logo, int SHSFW_logoBGColor) {
		int xRef = 1;
		int yRef = 0;
		int wLogo = 15;

		if (SHSFW_ipParams.getSHSFW_logo() != 0) {

			SHSFW_removeView("logo");
			SHSFW_msgRoutine
			.SDISP_images("logo", xRef, yRef, wLogo, SHSFW_logo);
			if (SHSFW_ipParams.getSHSFW_logoBGColor() != 0) {
				ImageView ivLogo = (ImageView) parent.findViewWithTag("logo");
				ivLogo.setBackgroundColor(Color.parseColor(bgColorMap.get(
						(Integer) SHSFW_logoBGColor).trim()));
			}
		}
	}

	/**
	 * This method is used to configure the hot key. Only enable or disable is
	 * possible.
	 * 
	 * @param current
	 * @param isEnabled
	 */
	public void SHSFW_configureHotKey(SHSFW_Operation SHSFW_current,
			boolean SHSFW_isEnabled) {

		SHSFW_msgRoutine = SDISP_MessageRoutines.SDISP_getInstance(
				getBaseContext(), parent, 0);

		ImageView ivBtn = (ImageView) parent.findViewWithTag(SHSFW_current
				.toString());		

		if (ivBtn != null) {

			if(ivBtn.isEnabled()){
				SHSFW_updateHotkeyStateMap(SHSFW_current, true);
			}else {
				SHSFW_updateHotkeyStateMap(SHSFW_current, false);
			}

			ivBtn.setEnabled(SHSFW_isEnabled);
			if (SHSFW_isEnabled) {
				ivBtn.setImageAlpha(ALPHA_ENABLE);
			} else {
				ivBtn.setImageAlpha(ALPHA_DISABLE);
			}
		}
	}

	/**
	 * This method is used to configure the icon for the hot key reserved for
	 * the module.
	 * 
	 * @param SHSFW_iconID
	 */
	public void SHSFW_configureCustomHotKeyIcon(int SHSFW_iconID) {

		SHSFW_msgRoutine = SDISP_MessageRoutines.SDISP_getInstance(
				getBaseContext(), parent, 0);

		SUIRES_ROUTINE_Resources temp = SUIRES_ROUTINE_Resources
				.SUIRES_getResourceInstance(this);
		SUIRES_IconResponse iconResp = temp.SUIRES_getIconModified(SHSFW_iconID, wIcon);
		final Bitmap bmp = iconResp.getSUIRES_icon();

		ImageView ivBtn = (ImageView) parent
				.findViewWithTag(SHSFW_Operation.CUSTOM.toString());
		if (ivBtn != null) {

			/**************************************Modified 12 Nov 2015*******************************/
			tempImgView = ivBtn;
			ViewTreeObserver mImgBtnTreeObserver = ivBtn.getViewTreeObserver();
			mImgBtnTreeObserver.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {

				@Override
				public void onGlobalLayout() {



					tempImgView.setImageBitmap(Bitmap.createScaledBitmap(bmp,
							tempImgView.getWidth(), tempImgView.getHeight(), true));
					tempImgView.setTag(SHSFW_Operation.CUSTOM.toString());
				}
			});
			/****************************************************************************************/
		}else{

		}
	}

	/**
	 * This method is used to set the messages that need to be displayed on the
	 * header.
	 * 
	 * @param SHSFW_msg1
	 * @param SHSFW_msg2
	 * @param SHSFW_msg3
	 * @param SHSFW_msgColor
	 */
	public void SHSFW_configureMessages(Object SHSFW_msg1, Object SHSFW_msg2,
			Object SHSFW_msg3, int SHSFW_msgColor) {

		int xRef = 17;
		int yRef = 0;
		int yDelta = 3;

		SHSFW_msgRoutine = SDISP_MessageRoutines.SDISP_getInstance(
				getBaseContext(), parent, 0);

		if (SHSFW_msg1 instanceof Integer) {
			int message1 = (Integer) SHSFW_msg1;
			if (message1 != 0) {
				SHSFW_removeView("msg1");
				SHSFW_msgRoutine.SDISP_message("msg1", message1, xRef,
						yRef - 1, SHSFW_ipParams.getSHSFW_textSize(),
						SHSFW_msgColor);
			}
		} else if (SHSFW_msg1 instanceof String) {
			String message1 = (String) SHSFW_msg1;
			if (message1 != null) {
				SHSFW_removeView("msg1");
				SHSFW_msgRoutine.SDISP_message("msg1", 0, xRef, yRef - 1,
						SHSFW_ipParams.getSHSFW_textSize(), SHSFW_msgColor);
				tvTempMsg1 = (TextView) parent.findViewWithTag("msg1");
				// To get Location Display String
				System.out.println("SLOC display String in SHSFW: "+SHSFW_SCFG.SHSFW_SLOC_dispStrg);
				tvTempMsg1.setText(SHSFW_SCFG.SHSFW_SLOC_dispStrg);//message1);
			}
		} else {

		}

		if (SHSFW_msg2 instanceof Integer) {
			int message2 = (Integer) SHSFW_msg2;
			if (message2 != 0) {
				SHSFW_removeView("msg2");
				SHSFW_msgRoutine.SDISP_message("msg2", message2, xRef,
						yRef += yDelta, SHSFW_ipParams.getSHSFW_textSize(),
						SHSFW_msgColor);
			}
		} else if (SHSFW_msg2 instanceof String) {
			String message2 = (String) SHSFW_msg2;
			if (message2 != null) {
				SHSFW_removeView("msg2");
				SHSFW_msgRoutine.SDISP_message("msg2", 0, xRef, yRef += yDelta,
						SHSFW_ipParams.getSHSFW_textSize(), SHSFW_msgColor);
				tvTempMsg2 = (TextView) parent.findViewWithTag("msg2");
				tvTempMsg2.setText(message2);
			}
		} else {
			yRef += yDelta;
		}

		if (SHSFW_msg3 instanceof Integer) {
			int message3 = (Integer) SHSFW_msg3;
			if (message3 != 0) {
				SHSFW_removeView("msg3");
				SHSFW_msgRoutine.SDISP_message("msg3", message3, xRef,
						yRef += yDelta, SHSFW_ipParams.getSHSFW_textSize(),
						SHSFW_msgColor);
			}
		} else if (SHSFW_msg3 instanceof String) {
			String message3 = (String) SHSFW_msg3;
			if (message3 != null) {
				SHSFW_removeView("msg3");
				SHSFW_msgRoutine.SDISP_message("msg3", 0, xRef, yRef += yDelta,
						SHSFW_ipParams.getSHSFW_textSize(), SHSFW_msgColor);
				tvTempMsg3 = (TextView) parent.findViewWithTag("msg3");
				tvTempMsg3.setText(message3);
			}
		} else {
			yRef += yDelta;
		}
	}

	// ----------------------------------------------------------------------------------------------------
	// PRIVATE METHODS
	// ----------------------------------------------------------------------------------------------------

	/**
	 * this method is used to handle uncaught exceptions.
	 */
	private void setUpGlobalExceptionHandler() {
		
		Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
			@Override
			public void uncaughtException(Thread paramThread, Throwable paramThrowable) {
				Log.e(LOG_TAG, "***** exception - "+paramThrowable.getMessage()+" *****");
				System.exit(2);
				

				printer = SPRN_Print.SPRN_getInstance(SHSFW_context);
			}
		});
	}

	/**
	 * This method sets up the initial framework.
	 */
	private void setUpDefaultFramework() {
		// set the header properties
		SHSFW_InputParams SHSFW_params = new SHSFW_InputParams();
		ArrayList<SHSFW_OperationMap> SHSFW_opMapping = new ArrayList<SHSFW_OperationMap>();
		
		SHSFW_opMapping.add(new SHSFW_OperationMap(SHSFW_Operation.MORE,
				ICON_MORE, true, true));
		SHSFW_opMapping.add(new SHSFW_OperationMap(SHSFW_Operation.LOCK,
				ICON_LOCK, true, true));
		SHSFW_opMapping.add(new SHSFW_OperationMap(SHSFW_Operation.HOME,
				ICON_HOME, true, true));

		// check if the state is logged in or logged out.
		SHSFW_LoginState SHSFW_loginState = SHSFW_LoginState.getInstance(this);
		if (SHSFW_loginState.getSHSFW_state().getValue() == SHSFW_LoginStates.LOGGED_IN
				.getValue()) {
			SHSFW_opMapping.add(new SHSFW_OperationMap(SHSFW_Operation.LOGOUT,
					ICON_LOGOUT, true, true));
		} else {
			SHSFW_opMapping.add(new SHSFW_OperationMap(SHSFW_Operation.LOGIN,
					ICON_LOGIN, true, true));
		}

		SHSFW_opMapping.add(new SHSFW_OperationMap(SHSFW_Operation.CUSTOM,
				ICON_CUSTOM, false, true));
		SHSFW_opMapping.add(new SHSFW_OperationMap(SHSFW_Operation.REPORT,
				ICON_REPORT, false, true));

		// check if the state is PRIMARY or SECONDARY.
		SHSFW_PrimaryState SHSFW_primaryState = SHSFW_PrimaryState.getInstance(this);
		if (SHSFW_primaryState.getSHSFW_state().getValue() == SHSFW_PrimaryStates.PRIMARY
				.getValue()) {
			SHSFW_opMapping.add(new SHSFW_OperationMap(SHSFW_Operation.PRIMARY,
					ICON_PRIMARY, false, true));
		} else {
			SHSFW_opMapping.add(new SHSFW_OperationMap(SHSFW_Operation.SECONDARY,
					ICON_SECONDARY, false, true));
		}

		SHSFW_params.setSHSFW_operationMapping(SHSFW_opMapping);

		SHSFW_params.setSHSFW_logo(1);

		SHSFW_params.setSHSFW_message1ID(3);
		SHSFW_params.setSHSFW_message2ID(4);
		SHSFW_params.setSHSFW_message3ID(5);

		SHSFW_params.setSHSFW_textColor(1);
		SHSFW_params.setSHSFW_textSize(1);
		SHSFW_params.setSHSFW_headerBGColor(16);
		SHSFW_params.setSHSFW_logoBGColor(24);
		SHSFW_setInputParams(SHSFW_params);

	}

	/**
	 * This method is used to configure all the properties of a hot key. This
	 * method is currently an internal API, may be made public later.
	 * 
	 *
	 *
	 */
	private void SHSFW_configureHotKey(SHSFW_Operation SHSFW_current,
			final SHSFW_OperationMap SHSFW_opMap) {

		SHSFW_msgRoutine = SDISP_MessageRoutines.SDISP_getInstance(
				getBaseContext(), parent, 0);

		SUIRES_ROUTINE_Resources temp = SUIRES_ROUTINE_Resources
				.SUIRES_getResourceInstance(this);
		SUIRES_IconResponse iconResp = temp.SUIRES_getIconModified(SHSFW_opMap
				.getSHSFW_displayResource(), wIcon);
		final Bitmap bmp = iconResp.getSUIRES_icon();
		if (!SHSFW_opMap.isSHSFW_isEnabled()) {
			/*bmp = SHSFW_imageTransform(bmp, 0);*/
		}

		ImageView ivBtn = (ImageView) parent.findViewWithTag(SHSFW_current
				.toString());		

		if (ivBtn != null) {

			if(ivBtn.isEnabled()){
				SHSFW_updateHotkeyStateMap(SHSFW_current, true);
				ivBtn.setImageAlpha(ALPHA_ENABLE);
			}else {
				SHSFW_updateHotkeyStateMap(SHSFW_current, false);
				ivBtn.setImageAlpha(ALPHA_DISABLE);
			}

			ivBtn.setEnabled(SHSFW_opMap.isSHSFW_isEnabled());
			/**************************************Modified 12 Nov 2015*******************************/
			tempImgView = ivBtn;
			ViewTreeObserver mImgBtnTreeObserver = ivBtn.getViewTreeObserver();
			mImgBtnTreeObserver.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {

				@Override
				public void onGlobalLayout() {
					tempImgView.setImageBitmap(Bitmap.createScaledBitmap(bmp,
							tempImgView.getWidth(), tempImgView.getHeight(), true));
					tempImgView.setTag(SHSFW_opMap.getSHSFW_OperationType().toString());
				}
			});
			/****************************************************************************************/

			if (SHSFW_opMap.isSHSFW_isVisible()) {
				ivBtn.setVisibility(View.VISIBLE);
			} else {
				ivBtn.setVisibility(View.INVISIBLE);
			}
		}
	}

	/**
	 * 
	 * @param SHSFW_inpParams
	 */
	private void SHSFW_setInputParams(SHSFW_InputParams SHSFW_inpParams) {
		int xRefRight = 92;
		int xRefLeft = 1;
		int xDelta = 8;
		int yRef = 1;
		int wIcon = 7;

		SHSFW_msgRoutine = SDISP_MessageRoutines.SDISP_getInstance(
				getBaseContext(), parent, 0);

		this.SHSFW_ipParams = SHSFW_inpParams;

		// header layout
		header = new View(this);
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, headerHeight);
		params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		header.setLayoutParams(params);
		header.setBackgroundColor(Color.parseColor(bgColorMap
				.get(SHSFW_ipParams.getSHSFW_headerBGColor())));
		parent.addView(header);

		// add logo
		if (SHSFW_ipParams.getSHSFW_logo() != 0) {

			SHSFW_removeView("logo");
			SHSFW_msgRoutine.SDISP_images("logo", xRefLeft, yRef, 15,
					SHSFW_ipParams.getSHSFW_logo());
			if (SHSFW_ipParams.getSHSFW_logoBGColor() != 0) {
				ImageView ivLogo = (ImageView) parent.findViewWithTag("logo");
				ivLogo.setBackgroundColor(Color.parseColor(bgColorMap
						.get(SHSFW_ipParams.getSHSFW_logoBGColor())));
			}
		}

		// add the buttons
		int SHSFW_size = SHSFW_ipParams.getSHSFW_operationMapping().size();
		if (SHSFW_size > 7) {
			SHSFW_size = 7;
		}
		for (int i = 0; i < SHSFW_size; i++) {
			SHSFW_btnTag = SHSFW_ipParams.getSHSFW_operationMapping().get(i)
					.getSHSFW_OperationType().toString();
			SHSFW_removeView(SHSFW_btnTag);
			if (i == 0) {
				SHSFW_msgRoutine.SDISP_buttonIcon(SHSFW_btnTag, xRefRight,
						yRef, wIcon, SHSFW_ipParams.getSHSFW_operationMapping()
						.get(i).getSHSFW_displayResource());

			} else {
				SHSFW_msgRoutine.SDISP_buttonIcon(SHSFW_btnTag,
						xRefRight -= xDelta, yRef, wIcon, SHSFW_ipParams
						.getSHSFW_operationMapping().get(i)
						.getSHSFW_displayResource());
			}

			ivTempBtn = (ImageView) parent.findViewWithTag(SHSFW_btnTag);
			ivTempBtn.setEnabled(SHSFW_ipParams.getSHSFW_operationMapping()
					.get(i).isSHSFW_isEnabled());

			if (!SHSFW_ipParams.getSHSFW_operationMapping().get(i)
					.isSHSFW_isEnabled()) {
				ivTempBtn.setImageAlpha(ALPHA_DISABLE);
			}

			if (SHSFW_ipParams.getSHSFW_operationMapping().get(i)
					.isSHSFW_isVisible()) {
				ivTempBtn.setVisibility(View.VISIBLE);
			} else {
				ivTempBtn.setVisibility(View.INVISIBLE);
			}

			ivTempBtn.setOnClickListener(new View.OnClickListener() {
				private long mLastClickTime=0;
				@Override
				public void onClick(View v) {
					//Clicking prevention, using threshold of 1000 ms
					if(SystemClock.elapsedRealtime()- mLastClickTime<1000){
						return;
					}
					mLastClickTime=SystemClock.elapsedRealtime();
					SHSFW_sendData(v.getTag().toString());
				}
			});
		}

		// add text messages
		if (SHSFW_ipParams.getSHSFW_message1ID() != 0) {
			SHSFW_removeView("msg1");
			SHSFW_msgRoutine.SDISP_message("msg1",
					SHSFW_ipParams.getSHSFW_message1ID(), xRefLeft += wIcon
					+ xDelta + 1, yRef -= 1,
					SHSFW_ipParams.getSHSFW_textSize(),
					SHSFW_ipParams.getSHSFW_textColor());
		}
		if (SHSFW_ipParams.getSHSFW_message1Txt() != null) {
			SHSFW_removeView("msg1");
			SHSFW_msgRoutine.SDISP_message("msg1",
					SHSFW_ipParams.getSHSFW_message1ID(), xRefLeft += wIcon
					+ xDelta + 1, yRef -= 1,
					SHSFW_ipParams.getSHSFW_textSize(),
					SHSFW_ipParams.getSHSFW_textColor());
			tvTempMsg1 = (TextView) parent.findViewWithTag("msg1");
			tvTempMsg1.setText(SHSFW_ipParams.getSHSFW_message1Txt());
		}

		if (SHSFW_ipParams.getSHSFW_message2ID() != 0) {
			SHSFW_removeView("msg2");
			SHSFW_msgRoutine.SDISP_message("msg2",
					SHSFW_ipParams.getSHSFW_message2ID(), xRefLeft, yRef += 3,
					SHSFW_ipParams.getSHSFW_textSize(),
					SHSFW_ipParams.getSHSFW_textColor());
		}
		if (SHSFW_ipParams.getSHSFW_message2Txt() != null) {
			SHSFW_removeView("msg2");
			SHSFW_msgRoutine.SDISP_message("msg2",
					SHSFW_ipParams.getSHSFW_message2ID(), xRefLeft, yRef += 3,
					SHSFW_ipParams.getSHSFW_textSize(),
					SHSFW_ipParams.getSHSFW_textColor());
			tvTempMsg2 = (TextView) parent.findViewWithTag("msg2");
			tvTempMsg2
			.setText(SHSFW_ipParams.getSHSFW_message2Txt().toString());
		}

		if (SHSFW_ipParams.getSHSFW_message3ID() != 0) {
			SHSFW_removeView("msg3");
			SHSFW_msgRoutine.SDISP_message("msg3",
					SHSFW_ipParams.getSHSFW_message3ID(), xRefLeft, yRef += 3,
					SHSFW_ipParams.getSHSFW_textSize(),
					SHSFW_ipParams.getSHSFW_textColor());
		}
		if (SHSFW_ipParams.getSHSFW_message3Txt() != null) {
			SHSFW_removeView("msg3");
			SHSFW_msgRoutine.SDISP_message("msg3",
					SHSFW_ipParams.getSHSFW_message3ID(), xRefLeft, yRef += 3,
					SHSFW_ipParams.getSHSFW_textSize(),
					SHSFW_ipParams.getSHSFW_textColor());
			tvTempMsg3 = (TextView) parent.findViewWithTag("msg3");
			tvTempMsg3
			.setText(SHSFW_ipParams.getSHSFW_message3Txt().toString());
		}

		// build the map of current state of the hot keys (ie) enabled ? visible
		// ? and so on
		SHSFW_buildHotkeyStateMap(SHSFW_ipParams);

	}

	/**
	 * This method is used to return control back to the activity by invoking
	 * the interface callback method.
	 * 
	 * @param tag
	 */
	private void SHSFW_sendData(String tag) {
		for (SHSFW_HotkeyState state : SHSFW_hotkeyState) {
			System.out.println(state.toString());
		}

		if (SHSFW_interface != null) {
			if (tag.equalsIgnoreCase(SHSFW_Operation.INVALID.toString())) {
				// do not invoke the interface callback method.
				// To be handled with-in the framework itself
				// SHSFW_interface.SHSFW_onClick(SHSFW_Operation.INVALID);

			} else if (tag.equalsIgnoreCase(SHSFW_Operation.HOME.toString())) {
				// do not invoke the interface callback method.
				// To be handled with-in the framework itself
				// SHSFW_interface.SHSFW_onClick(SHSFW_Operation.HOME);

				// always move to TP_HomeActivity
				SHSFW_displayDialogHome(this);

			} else if (tag.equalsIgnoreCase(SHSFW_Operation.LOCK.toString())) {
				// do not invoke the interface callback method.
				// To be handled with-in the framework itself
				// SHSFW_interface.SHSFW_onClick(SHSFW_Operation.LOCK);

				// when the lock button is pressed
				// disable all the other buttons.
				SHSFW_configureHotKey(SHSFW_Operation.PRIMARY, false);
				SHSFW_configureHotKey(SHSFW_Operation.REPORT, false);
				SHSFW_configureHotKey(SHSFW_Operation.HOME, false);
				SHSFW_configureHotKey(SHSFW_Operation.LOGIN, false);
				SHSFW_configureHotKey(SHSFW_Operation.LOGOUT, true);
				SHSFW_configureHotKey(SHSFW_Operation.MORE, false);
				SHSFW_configureHotKey(SHSFW_Operation.CUSTOM, false);

				// change the hot key to unlock
				SHSFW_configureHotKey(SHSFW_Operation.LOCK,
						new SHSFW_OperationMap(SHSFW_Operation.UNLOCK,
								ICON_UNLOCK, true, true));

				// load the lock fragment in the child fragment
				SHSFW_AnimFragment moreFrag = new SHSFW_AnimFragment();
				getSupportFragmentManager().beginTransaction()
				.replace(child.getId(), moreFrag).addToBackStack(null)
				.commit();

			} else if (tag.equalsIgnoreCase(SHSFW_Operation.UNLOCK.toString())) {
				// do not invoke the interface callback method.
				// To be handled with-in the framework itself
				// SHSFW_interface.SHSFW_onClick(SHSFW_Operation.UNLOCK);

				// when the unlock button is pressed, change it to 
				// lock and restore the previous state of the hotkeys 
				//Note:- problem number-
				
				
				
				/*for (SHSFW_HotkeyState state : SHSFW_hotkeyState) {
					SHSFW_configureHotKey(state.getSHSFW_tag(), state.isSHSFW_isEnabled());
				}

				SHSFW_configureHotKey(SHSFW_Operation.UNLOCK,
						new SHSFW_OperationMap(SHSFW_Operation.LOCK, ICON_LOCK,
								true, true));

				// go through the unlock mechanism and go back to the same point
				getSupportFragmentManager().popBackStack();
				
*/
				SHSFW_unlockDialogLogin(this);
				
				
			} else if (tag.equalsIgnoreCase(SHSFW_Operation.LOGIN.toString())) {
				// do not invoke the interface callback method.
				// To be handled with-in the framework itself
				// SHSFW_interface.SHSFW_onClick(SHSFW_Operation.LOGIN);

				// When login is pressed,
				// change state to LOGGED_IN
				// change icon to LOGOUT and
				SHSFW_LoginState SHSFW_state = SHSFW_LoginState.getInstance(this);
				SHSFW_state.setSHSFW_state(SHSFW_LoginStates.LOGGED_IN);

				Intent intent;
				try {
					intent = new Intent(this, Class.forName(LoginClassName));
					startActivity(intent);
				} catch (ClassNotFoundException e) {
					Log.v(LOG_TAG,
							"Login Activity not found. Exception - "
									+ e.getMessage());
				}

				SHSFW_configureHotKey(SHSFW_Operation.LOGIN,
						new SHSFW_OperationMap(SHSFW_Operation.LOGOUT,
								ICON_LOGOUT, true, true));

			} else if (tag.equalsIgnoreCase(SHSFW_Operation.LOGOUT.toString())) {
				// do not invoke the interface callback method.
				// To be handled with-in the framework itself
				// SHSFW_interface.SHSFW_onClick(SHSFW_Operation.LOGOUT);

				// When logout is pressed,
				// change state to LOGGED_OUT
				// change icon to LOGIN and
				// move to TP_MainActivity
				SHSFW_LoginState SHSFW_state = SHSFW_LoginState.getInstance(this);
				SHSFW_state.setSHSFW_state(SHSFW_LoginStates.LOGGED_OUT);

				SHSFW_displayDialogLogout(this);

			} else if (tag.equalsIgnoreCase(SHSFW_Operation.MORE.toString())) {
				// do not invoke the interface callback method.
				// To be handled with-in the framework itself
				// SHSFW_interface.SHSFW_onClick(SHSFW_Operation.MORE);

				SHSFW_configureHotKey(SHSFW_Operation.MORE, false);

				// load the MORE fragment
				SHSFW_MoreFragment moreFrag = new SHSFW_MoreFragment();
				getSupportFragmentManager()
				.beginTransaction()
				.replace(SHSFW_FrameWorkActivity.child.getId(),
						moreFrag).addToBackStack(null).commit();

			} else if (tag.equalsIgnoreCase(SHSFW_Operation.REPORT.toString())) {
				SHSFW_interface.SHSFW_onClick(SHSFW_Operation.REPORT);

			} else if (tag.equalsIgnoreCase(SHSFW_Operation.PRIMARY.toString())) {
				/*SHSFW_interface.SHSFW_onClick(SHSFW_Operation.PRIMARY);*/

				//change the icon to SECONDARY
				SHSFW_configureHotKey(SHSFW_Operation.PRIMARY,
						new SHSFW_OperationMap(SHSFW_Operation.SECONDARY, ICON_SECONDARY,
								true, true));

				//move to PRIMARY operation
				Intent intent;
				try {
					intent = new Intent(this, Class.forName(PrimaryClassName));
					startActivity(intent);
				} catch (ClassNotFoundException e) {
					Log.v(LOG_TAG, "Target activity - " + PrimaryClassName
							+ ", not found. Exception - " + e.getMessage());
				}

			} else if (tag.equalsIgnoreCase(SHSFW_Operation.SECONDARY.toString())) {

				//Change the icon to PRIMARY
				SHSFW_configureHotKey(SHSFW_Operation.SECONDARY,
						new SHSFW_OperationMap(SHSFW_Operation.PRIMARY, ICON_PRIMARY,
								true, true));

				Intent intent;
				try {
					intent = new Intent(this, Class.forName(HomeClassName));
					startActivity(intent);
				} catch (ClassNotFoundException e) {
					Log.v(LOG_TAG, "Target activity - " + HomeClassName
							+ ", not found. Exception - " + e.getMessage());
				}

			} else if (tag.equalsIgnoreCase(SHSFW_Operation.CUSTOM.toString())) {
				SHSFW_interface.SHSFW_onClick(SHSFW_Operation.CUSTOM);

			}
		}
	}

	/**
	 * 
	 * @param SHSFW_inpParams
	 */
	private void SHSFW_buildHotkeyStateMap(SHSFW_InputParams SHSFW_inpParams) {

		// get the array list from SHSFW_inpParmas
		ArrayList<SHSFW_OperationMap> hkProperties = SHSFW_inpParams
				.getSHSFW_operationMapping();

		for (int i = 0; i < hkProperties.size(); i++) {
			SHSFW_Operation tag = hkProperties.get(i).getSHSFW_OperationType();
			boolean isEnabled = hkProperties.get(i).isSHSFW_isEnabled();
			boolean isVisible = hkProperties.get(i).isSHSFW_isVisible();
			SHSFW_hotkeyState[i] = new SHSFW_HotkeyState(tag, isEnabled,
					isVisible);
		}

	}

	/**
	 * 
	 * @param SHSFW_current
	 * @param SHSFW_isEnabled
	 */
	private void SHSFW_updateHotkeyStateMap(SHSFW_Operation SHSFW_current,
			boolean SHSFW_isEnabled) {

		for (int i = 0; i < SHSFW_hotkeyState.length; i++) {
			if (SHSFW_hotkeyState[i].getSHSFW_tag().getValue() == SHSFW_current.getValue()) {
				SHSFW_hotkeyState[i].setSHSFW_isEnabled(SHSFW_isEnabled);
			}
		}

	}

	// ----------------------------------------------------------------------------------------------------
	// UTILITY METHODS
	// ----------------------------------------------------------------------------------------------------
	/**
	 * Internal API to remove a particular view.
	 * 
	 * @param tag
	 */
	private void SHSFW_removeView(String tag) {
		if (parent.findViewWithTag(tag) != null) {
			parent.removeView(parent.findViewWithTag(tag));
		}
	}

	/**
	 * 
	 * @param original
	 * @param imgView
	 * @return
	 */
	private Bitmap resizeBitmap(Bitmap original, ImageView imgView) {

		Bitmap resizedBitmap = Bitmap.createScaledBitmap(original,
				imgView.getWidth(), imgView.getHeight(), true);
		return resizedBitmap;
	}

	/**
	 * This method is used to display the confirmation dialog box - HOME
	 */
	public void SHSFW_displayDialogHome(final Context context) {

		final int TITLE = 31;
		final int MSG = 29;
		final int YES = 165;
		final int NO = 102;

		SUIRES_routine_resources = SUIRES_ROUTINE_Resources
				.SUIRES_getResourceInstance(this);
		String titleStr = SUIRES_routine_resources.SUIRES_getMessage(TITLE)
				.getSUIRES_message();
		String msgStr = SUIRES_routine_resources.SUIRES_getMessage(MSG)
				.getSUIRES_message();
		String yesStr = SUIRES_routine_resources.SUIRES_getMessage(YES)
				.getSUIRES_message();
		String noStr = SUIRES_routine_resources.SUIRES_getMessage(NO)
				.getSUIRES_message();

		final Dialog dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.shsfw_confirmdialog);
		TextView title = (TextView) dialog.findViewById(R.id.dialog_title);
		TextView message = (TextView) dialog.findViewById(R.id.dialog_message);
		Button yes = (Button) dialog.findViewById(R.id.dialog_yes);
		Button no = (Button) dialog.findViewById(R.id.dialog_no);

		dialog.setCancelable(false);
		dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM,
				WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
		
		
		title.setText(titleStr);
		message.setText(msgStr);
		yes.setText(yesStr);
		no.setText(noStr);

		no.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.cancel();
			}
		});

		yes.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent;
				try {
					intent = new Intent(context, Class.forName(HomeClassName));
					startActivity(intent);
				} catch (ClassNotFoundException e) {
					Log.v(LOG_TAG, "Target activity - " + HomeClassName
							+ ", not found. Exception - " + e.getMessage());
				}

			}
		});

		dialog.show();
	}

	/**
	 * This method is used to display the confirmation dialog box - LOGOUT
	 */
	public void SHSFW_displayDialogLogout(final Context context) {

		final int TITLE = 31;
		final int MSG = 28;
		final int YES = 165;
		final int NO = 102;

		SUIRES_routine_resources = SUIRES_ROUTINE_Resources
				.SUIRES_getResourceInstance(this);
		String titleStr = SUIRES_routine_resources.SUIRES_getMessage(TITLE)
				.getSUIRES_message();
		String msgStr = SUIRES_routine_resources.SUIRES_getMessage(MSG)
				.getSUIRES_message();
		String yesStr = SUIRES_routine_resources.SUIRES_getMessage(YES)
				.getSUIRES_message();
		String noStr = SUIRES_routine_resources.SUIRES_getMessage(NO)
				.getSUIRES_message();

		final Dialog dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.shsfw_confirmdialog);
		TextView title = (TextView) dialog.findViewById(R.id.dialog_title);
		TextView message = (TextView) dialog.findViewById(R.id.dialog_message);
		Button yes = (Button) dialog.findViewById(R.id.dialog_yes);
		Button no = (Button) dialog.findViewById(R.id.dialog_no);

		dialog.setCancelable(false);
		dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM,
				WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
		
		title.setText(titleStr);
		message.setText(msgStr);
		yes.setText(yesStr);
		no.setText(noStr);

		no.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.cancel();
			}
		});

		yes.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				SHSFW_configureHotKey(SHSFW_Operation.LOGOUT,
						new SHSFW_OperationMap(SHSFW_Operation.LOGIN,
								ICON_LOGIN, true, true));

				Intent intent;
				try {
					
					// SHACK - login module API must be called here to log out.
					SharedPreferences sharedPref = getSharedPreferences("ekycpref", Context.MODE_PRIVATE);
					Editor edit = sharedPref.edit();
					edit.putString("LoggedUserType", "NONE");
					edit.commit();
					
					intent = new Intent(context, Class.forName(LogoutClassName));
					startActivity(intent);
				} catch (ClassNotFoundException e) {
					Log.v(LOG_TAG, "Target activity - " + LogoutClassName
							+ ", not found. Exception - " + e.getMessage());
				}

			}
		});

		dialog.show();
	}

	/**
	 * The Disable Status Bar.
	 */
	private void SHSFW_disableStatusBar() {

		SBmanager = ((WindowManager) getApplicationContext().getSystemService(
				Context.WINDOW_SERVICE));

		WindowManager.LayoutParams localLayoutParams = new WindowManager.LayoutParams();
		localLayoutParams.type = WindowManager.LayoutParams.TYPE_SYSTEM_ERROR;
		localLayoutParams.gravity = Gravity.TOP;
		localLayoutParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
				|

				/** this is to enable the notification to receive touch events */
				WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL |

				/** Draws over status bar */
				WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN;

		localLayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
		localLayoutParams.height = (int) (30 * getResources()
				.getDisplayMetrics().scaledDensity);
		localLayoutParams.format = PixelFormat.TRANSPARENT;

		SBview = new SHSFW_DisableStatusBar(this);

		SBmanager.addView(SBview, localLayoutParams);
	}

	/**
	 * The Enable Status Bar method
	 */
	private void SHSFW_enableStatusBar() {
		SBmanager.removeView(SBview);
		SBmanager = null;
	}

	
	/**
	 * Date:- 27 April 2016
	 * Problem Number:- 
	 * Bref:-  
	 * This method is used to take in user name and password for unlock the screen 
	 */
	
	public void SHSFW_unlockDialogLogin(final Context context) {
		final int USER = 157;
		final int NAME = 98;
		final int ENTER = 58;
		final int PASSWORD = 109;
		final int UNLOCK=299;
		final int CANCEL=20;
		final int MISMATCH=226;
		final int NOTFOUND=672; 

		SUIRES_routine_resources = SUIRES_ROUTINE_Resources
				.SUIRES_getResourceInstance(this);
		String user = SUIRES_routine_resources.SUIRES_getMessage(USER)
				.getSUIRES_message();
		String name = SUIRES_routine_resources.SUIRES_getMessage(NAME)
				.getSUIRES_message();
		String password = SUIRES_routine_resources.SUIRES_getMessage(PASSWORD)
				.getSUIRES_message();
		String enter = SUIRES_routine_resources.SUIRES_getMessage(ENTER)
				.getSUIRES_message();
		String unlock=SUIRES_routine_resources.SUIRES_getMessage(UNLOCK)
				.getSUIRES_message();
		String cancel=SUIRES_routine_resources.SUIRES_getMessage(CANCEL)
				.getSUIRES_message();
		String mismatch=SUIRES_routine_resources.SUIRES_getMessage(MISMATCH)
				.getSUIRES_message();
		String notfound=SUIRES_routine_resources.SUIRES_getMessage(NOTFOUND)
				.getSUIRES_message();
		
		String username=user+name;
		String titleStr=enter +" "+username+" "+ password; 
		UNnotfound=username +" "+ notfound;
		UNPmismatch=username+" "+ password+ " "+mismatch;
		UNPnotfound=username+" "+ password+" "+ notfound;

		 AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
		 alertDialog.setTitle(titleStr);
		 /*AlertDialog dialog = alertDialog.create();
		 dialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextSize(25);
		 dialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextSize(25);
		 */

		 final EditText SHSFW_username = new EditText(this);
		 final EditText SHSFW_password= new EditText(this);
		
		 LinearLayout layout = new LinearLayout(getApplicationContext());
	     layout.setOrientation(LinearLayout.VERTICAL);
	   //  SHSFW_password.setInputType(PASSWORD);
		 SHSFW_username.setHint(username);
		 SHSFW_password.setHint(password);
		 SHSFW_password.setInputType(android.text.InputType.TYPE_CLASS_TEXT | android.text.InputType.TYPE_TEXT_VARIATION_PASSWORD);
		 SHSFW_username.setInputType(android.text.InputType.TYPE_CLASS_TEXT |android.text.InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD );
		 layout.addView(SHSFW_username);
		 layout.addView(SHSFW_password);
		 alertDialog.setView(layout);
		 
	

		 alertDialog.setPositiveButton(unlock,
		     new DialogInterface.OnClickListener() {
		         @SuppressWarnings("static-access")
				public void onClick(DialogInterface dialog, int which) {
		        	 
		        	 dialog.dismiss();
		        	 System.out.println("------------------------");
		             SHSFW_unLockUser = SHSFW_username.getText().toString();
		             System.out.println("------------------------"+SHSFW_unLockUser);
		             SHSFW_unLockPassword=SHSFW_password.getText().toString();
		            
		             System.out.println(""+SHSFW_unLockUser+ " " +SHSFW_unLockPassword);

		              System.out.println(""+SHSFW_unLockUser+ " " +SHSFW_unLockPassword+ " " +SHSFW_Responce);
		              if(SHSFW_Responce.equals("00")){
		            	 for (SHSFW_HotkeyState state : SHSFW_hotkeyState) {
		 					SHSFW_configureHotKey(state.getSHSFW_tag(), state.isSHSFW_isEnabled());
		 				}

		 				SHSFW_configureHotKey(SHSFW_Operation.UNLOCK,
		 						new SHSFW_OperationMap(SHSFW_Operation.LOCK, ICON_LOCK,
		 								true, true));

		 				// go through the unlock mechanism and go back to the same point
		 				getSupportFragmentManager().popBackStack();
		 				
		            	 
		             }else if(SHSFW_Responce.equals("01")){
		            	 
		            	 //note:- username not found
		            	 SHSFW_DialogMessage(context,UNnotfound);
		            	 dialog.cancel();
		            	 
		            	 
		             }else if(SHSFW_Responce.equals("02")){
		            	 
		            	 //note:- username and password not found
		            	 SHSFW_DialogMessage(context,UNPnotfound);
		            	 dialog.cancel();
		            	 
		             }else{
		            	 
		            	//note:- username and password mis match
		            	 SHSFW_DialogMessage(context,UNPmismatch);
		            	 dialog.cancel();
		            	 
		             }
	             
		             
		         }
		     });

		 alertDialog.setNegativeButton(cancel,
		     new DialogInterface.OnClickListener() {
		         public void onClick(DialogInterface dialog, int which) {
		             dialog.cancel();
		         }
		     });

		 
		 AlertDialog alert = alertDialog.create();
	        alert.show();
	        alert.getWindow().getAttributes();

		// alertDialog.show();
	        Button btn1 = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
	        Button btn2 = alert.getButton(DialogInterface.BUTTON_POSITIVE);
	        btn1.setTextSize(20);
	        btn1.setTypeface(null, Typeface.BOLD);
	        btn2.setTextSize(20);
	        btn2.setTypeface(null, Typeface.BOLD);
		 }

	@SuppressWarnings("deprecation")
	public void SHSFW_DialogMessage(final Context context,String dialogMessage) {
		final int ERROR = 62;
		final int OK = 105;
		
		SUIRES_routine_resources = SUIRES_ROUTINE_Resources
				.SUIRES_getResourceInstance(this);
		
		String error=SUIRES_routine_resources.SUIRES_getMessage(ERROR)
				.getSUIRES_message();
		
		String ok=SUIRES_routine_resources.SUIRES_getMessage(OK)
				.getSUIRES_message();
		
	
		String titleStr=error; 

		 AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		 alertDialog.setTitle(titleStr);
		 

		 alertDialog.setMessage(dialogMessage);
		 
		 
		// alertDialog.setButton(titleStr, listener)
		  alertDialog.setButton(ok, new DialogInterface.OnClickListener() {

	            public void onClick(DialogInterface dialog,int which)
	            {
	                // Write your code here to execute after dialog    closed
	               dialog.cancel();
	            }
	        });

		 
		 alertDialog.show();
		 	}
	
	
	@Override
	public void onBackPressed() {

	   

	}
	
	
	
}