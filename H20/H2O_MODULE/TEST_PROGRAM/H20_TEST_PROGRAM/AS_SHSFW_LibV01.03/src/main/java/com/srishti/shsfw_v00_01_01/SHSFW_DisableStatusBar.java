package com.srishti.shsfw_v00_01_01;

import android.content.Context;
import android.view.MotionEvent;
import android.view.ViewGroup;

public class SHSFW_DisableStatusBar extends ViewGroup {
	 
	public SHSFW_DisableStatusBar(Context context) {
		super(context);
	}

	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev) {
		
		return true;
	}
}