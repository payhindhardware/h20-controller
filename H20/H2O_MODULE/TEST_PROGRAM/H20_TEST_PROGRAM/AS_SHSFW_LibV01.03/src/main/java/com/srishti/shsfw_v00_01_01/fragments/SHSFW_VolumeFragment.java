package com.srishti.shsfw_v00_01_01.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.srishti.shsfw_v00_01_01.interfaces.SHSFW_InputInterface;
import com.srishti.shsfw_v00_01_01.util.SHSFW_OperationMap.SHSFW_Operation;
import com.srishti.ui.message_routines.SDISP_MessageRoutines;

public class SHSFW_VolumeFragment extends Fragment{

	SHSFW_InputInterface passer;
	private SDISP_MessageRoutines SHSFW_msgRoutine;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		RelativeLayout rl = new RelativeLayout(getActivity());
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		rl.setLayoutParams(params);
		
		SHSFW_msgRoutine = SDISP_MessageRoutines.SDISP_getInstance(
				getActivity(), rl, 0);
		
		SHSFW_msgRoutine.SDISP_message("txtVol", 3, 10, 10, 3, 1);
		TextView vol = (TextView) rl.findViewWithTag("txtVol");
		vol.setText("Volume fragment");
		


		return rl;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			passer = (SHSFW_InputInterface) activity;

		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement SHSFW_InputInterface");
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		//passer.SHSFW_configHotKey(SHSFW_Operation.MORE, new SHSFW_OperationMap(SHSFW_Operation.MORE, 45, false, true));
		passer.SHSFW_IConfigHotKey(SHSFW_Operation.MORE, false);
	}
}
