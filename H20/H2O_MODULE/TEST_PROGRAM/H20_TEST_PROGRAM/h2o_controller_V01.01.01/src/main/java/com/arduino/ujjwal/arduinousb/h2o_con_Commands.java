package com.arduino.ujjwal.arduinousb;

import android.app.Activity;
import java.util.Random;
import com.arduino.ujjwal.arduinousb.activity_call.*;


/**
 * Created by SrishtiESDM_vishwas on 11-Aug-16.
 */
public  class h2o_con_Commands extends Activity
{
    //Constructor
    h2o_con_Commands()
    {

    }

    //Start monitor
    public  void start_Monitor(int tap_no )
    {

            int refNumber = ref_gen();   //reference number
            String h2o_start_mon = Integer.toString(refNumber) + ",H2O,START,MONITOR," + Integer.toString(tap_no) + "\r";
            h2oController_MainActivity.scuc.SCuC_transmit_data(h2o_start_mon);

    }
    //Stop monitor
    public  void stop_Monitor(int tapNo )
    {


            int refNumber = ref_gen();   //reference number
            String h2o_stop_mon = Integer.toString(refNumber) + ",H2O,STOP,MONITOR," + Integer.toString(tapNo)+ "\r";
        h2oController_MainActivity.scuc.SCuC_transmit_data(h2o_stop_mon);


    }
    //Clear counters
    public  void clear_Counter(int tapNo )
    {
        int refNumber = ref_gen();   //reference number
        String h2o_clear = Integer.toString(refNumber)+",H2O,CLEAR,COUNTERS,"+Integer.toString(tapNo)+"\r";
        h2oController_MainActivity.scuc.SCuC_transmit_data(h2o_clear);

    }
    //Set litres
    public void set_Litre(int tapNo , Float litre )
    {
        int refNumber = ref_gen();   //reference number
        String h2o_sl = Integer.toString(refNumber)+",H2O,SL,LR,"+Integer.toString(tapNo)+","+Float.toString(litre)+"\r";
        h2oController_MainActivity.scuc.SCuC_transmit_data(h2o_sl);


    }

    //Emergency stop
    public  void emergency_stop(int tapNo )
    {
        int refNumber = ref_gen();   //reference number
        String h2o_es = Integer.toString(refNumber)+",H2O,SL,ES,"+Integer.toString(tapNo)+"\r";
        h2oController_MainActivity.scuc.SCuC_transmit_data(h2o_es);

    }

    //Read status
    public  void read_Status(int tapNo)
    {
        int refNumber = ref_gen();   //reference number
        String h2o_rs = Integer.toString(refNumber)+",H2O,SL,RS,"+Integer.toString(tapNo)+"\r";
        h2oController_MainActivity.scuc.SCuC_transmit_data(h2o_rs);

    }


    //Ref number generator
    public  int ref_gen()
    {
        int refNumber=0;
        int min = 0;
        int max = 10000;
        Random r = new Random();
        refNumber = r.nextInt(max - min + 1) + min;
        return(refNumber);
    }



}
