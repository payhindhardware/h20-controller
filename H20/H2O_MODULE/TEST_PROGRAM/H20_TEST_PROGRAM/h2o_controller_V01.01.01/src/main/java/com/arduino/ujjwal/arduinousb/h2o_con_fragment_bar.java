package com.arduino.ujjwal.arduinousb;


import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.achartengine.ChartFactory;
import org.achartengine.chart.BarChart;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import java.text.DecimalFormat;


public class h2o_con_fragment_bar extends android.support.v4.app.Fragment {
    //Dataset
     private XYSeries[] h2o_flown_tap;
     private XYSeriesRenderer[] renderer_bar;
     private View bar_graph; //Main view
     private XYMultipleSeriesRenderer multirenderer; //Main renderer
     private XYMultipleSeriesDataset msd; //Main data set

     private float[] bg_h2o_flown_litre;
     private float[] bg_h2o_req_litre;
     private int bg_no_of_taps;
     private String[] bg_tap_name;
     private float bg_h2o_set_ymax_bar;
     private DecimalFormat bg_precision = new DecimalFormat("0.00");  //To round number to two decimal digit

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

            ViewGroup graphContainer = (ViewGroup) getActivity().findViewById(R.id.f_fragment_graph);
            if( graphContainer != null)
            {
                graphContainer.removeAllViews();
            }




    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        bg_h2o_flown_litre = this.getArguments().getFloatArray("h2o_flown_litre");
        bg_h2o_req_litre = this.getArguments().getFloatArray("h2o_req_litre");
        bg_no_of_taps = this.getArguments().getInt("no_of_taps");
        bg_tap_name = this.getArguments().getStringArray("tap_name");
        bg_h2o_set_ymax_bar=this.getArguments().getFloat("h2o_set_ymax_bar");
        //After getting bg_no_of_taps
        h2o_flown_tap=new XYSeries[bg_no_of_taps];  //Data series
        renderer_bar = new XYSeriesRenderer[bg_no_of_taps]; //Renderer
        return (bar_graph());
    }


    private View bar_graph()
    {
        //Renderer color array
        String[] renderer_color = new String[bg_no_of_taps];
        renderer_color[0] = "#0000FF";
        renderer_color[1] = "#5555FF";
        renderer_color[2] = "#7777FF";
        renderer_color[3] = "#AAAAFF";

        //add data to the esries
        for (int temp = 0; temp < bg_no_of_taps; temp++) {
            //Create initial dataset
            h2o_flown_tap[temp] = new XYSeries("Tap" + (temp + 1));
            h2o_flown_tap[temp].clear();
            if( bg_h2o_flown_litre[temp] <= bg_h2o_set_ymax_bar )        //If more than max Y axis dont add , as it slows down the app in long run
            {
                h2o_flown_tap[temp].add(temp + 1, bg_h2o_flown_litre[temp]);
            }
            else
            {
                h2o_flown_tap[temp].add(temp + 1, bg_h2o_set_ymax_bar);
            }

        }
        //Create main dataset
        msd = new XYMultipleSeriesDataset();
        msd.clear();
        //add dataset to main data set
        for (int temp = 0; temp < bg_no_of_taps; temp++) {
            msd.addSeries(h2o_flown_tap[temp]);
        }

        //Renderer for bar chart
        for (int temp = 0; temp < bg_no_of_taps; temp++) {
            renderer_bar[temp] = new XYSeriesRenderer();
            renderer_bar[temp].setColor(Color.parseColor(renderer_color[temp]));
            renderer_bar[temp].setLineWidth(5);
            renderer_bar[temp].setDisplayChartValues(true);
            renderer_bar[temp].setChartValuesFormat(bg_precision);
            renderer_bar[temp].setGradientEnabled(true);
        }


        //Multi series renderer
        multirenderer = new XYMultipleSeriesRenderer();
        multirenderer.setChartTitle("Monitor");
        multirenderer.setXTitle("Tap Number");
        multirenderer.setYTitle("WaterFlown (L)");
        //X axis Labels
        multirenderer.addXTextLabel(-0.1, bg_tap_name[0]);
        multirenderer.addXTextLabel(1.85, bg_tap_name[1]);
        multirenderer.addXTextLabel(3.7, bg_tap_name[2]);
        multirenderer.addXTextLabel(5.6, bg_tap_name[3]);

        //Max and min limits
        multirenderer.setYAxisMax(bg_h2o_set_ymax_bar);
        multirenderer.setYAxisMin(0);
        multirenderer.setBackgroundColor(Color.parseColor("#ffffff"));
        multirenderer.setBarSpacing(1);
        multirenderer.setShowAxes(true);
        multirenderer.setPanEnabled(false, false);
        multirenderer.setXLabels(0);
        multirenderer.setLabelsColor(Color.parseColor("#000000"));
        multirenderer.setMarginsColor(Color.parseColor("#ffffff"));
        multirenderer.setBarWidth(35);
        multirenderer.setXAxisMin(-0.6); //for first bar overlaps y-axis
        multirenderer.setXAxisMax(6); //for Last bar overlaps y-axis

        //add renderer to main renderer
        for (int temp = 0; temp < bg_no_of_taps; temp++) {
            multirenderer.addSeriesRenderer(renderer_bar[temp]);//add to main renderer
            //Set colour of bar
            multirenderer.getSeriesRendererAt(temp).setGradientStart(0, Color.parseColor(renderer_color[temp]));//Setting color of graph
            multirenderer.getSeriesRendererAt(temp).setGradientStop(bg_h2o_req_litre[temp], Color.parseColor("#FF0000"));
        }

            bar_graph = ChartFactory.getBarChartView(getActivity(), msd, multirenderer, BarChart.Type.DEFAULT);
            return (bar_graph);


    }



    @Override
    public void onDestroyView()
    {
        super.onDestroyView();
        //Remove all views
      /*  ViewGroup graphContainer = (ViewGroup) getActivity().findViewById(R.id.f_fragment_graph);
        if( graphContainer != null)
        {
            graphContainer.removeAllViews();
        }*/
    }

    @Override
    public void onStop()
    {
        super.onStop();
        return;
    }
}









