package com.srishti.ask;


import android.app.Application;
import android.os.Environment;


import com.srishti.shsfw_v00_01_01.util.SHSFW_SCFG;
import com.srishti.suires.SUIRES_ROUTINE_DB;
import com.srishti.ui.message_routines.SDISP_MessageRoutines;

public class AS_AppConfiguration extends Application {

	public static SUIRES_ROUTINE_DB AS_SUIRES_DB_instance = null;
	public static SDISP_MessageRoutines AS_SDISP_msgRoutine = null;


	@Override
	public void onCreate() {
		super.onCreate();


		// Usage: Create instance of dependency module
		// SDISP_ROUTINE_db_resources
		AS_SUIRES_DB_instance = SUIRES_ROUTINE_DB
				.SUIRES_getInstanceDb(getApplicationContext());

		SHSFW_SCFG.SCFG_DB_instance = AS_SUIRES_DB_instance;
		AS_SDISP_msgRoutine = SDISP_MessageRoutines.SDISP_getInstance(
				getBaseContext(), null, 0);

		String SDISP_path = Environment.getExternalStorageDirectory()
				.getAbsolutePath();
		SDISP_MessageRoutines.SDISP_configure(SHSFW_SCFG.SHSFW_SDISP_languageCode,
				SDISP_path);

		// Usage: to initialize the resources required in the SDISP Module
		SDISP_MessageRoutines.SDISP_initialize(AS_SUIRES_DB_instance);
		// Login database



	}

}
