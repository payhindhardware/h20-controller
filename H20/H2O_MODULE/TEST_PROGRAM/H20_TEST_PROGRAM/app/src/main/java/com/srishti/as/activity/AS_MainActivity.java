package com.srishti.as.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import com.srishti.shsfw_v00_01_01.SHSFW_FrameWorkActivity;
import com.srishti.shsfw_v00_01_01.interfaces.SHSFW_InputInterface;
import com.srishti.shsfw_v00_01_01.interfaces.SHSFW_OutputInterface;
import com.srishti.shsfw_v00_01_01.util.SHSFW_OperationMap.SHSFW_Operation;

public class AS_MainActivity extends SHSFW_FrameWorkActivity implements SHSFW_InputInterface, SHSFW_OutputInterface {




	@Override
	public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);

		SHSFW_configureCallback(this);
		parent.setBackgroundColor(Color.LTGRAY);
		header.setBackgroundColor(Color.BLUE);



		SHSFW_configureMessages("", "Frame Work", "Test", 2);
		SHSFW_configureHotKey(SHSFW_Operation.HOME, false);
		SHSFW_configureHotKey(SHSFW_Operation.LOCK, false);
		SHSFW_configureHotKey(SHSFW_Operation.CUSTOM, false);
		SHSFW_configureHotKey(SHSFW_Operation.PRIMARY, false);
		SHSFW_configureHotKey(SHSFW_Operation.LOGOUT,false);
		SHSFW_configureHotKey(SHSFW_Operation.LOGIN,false);
		SHSFW_configureHotKey(SHSFW_Operation.MORE,true);


		TestModule();


	}


	@Override
	public void SHSFW_onClick(SHSFW_Operation SHSFW_opType) {

		if (SHSFW_opType == SHSFW_Operation.REPORT) {
			// do something
		} else if (SHSFW_opType == SHSFW_Operation.CUSTOM) {
			// do something
		} else if (SHSFW_opType == SHSFW_Operation.PRIMARY) {
			// do something
		}
	}

	@Override
	public void SHSFW_IConfigMessages(Object SHSFW_msg1, Object SHSFW_msg2,
			Object SHSFW_msg3, int SHSFW_fontColor) {
		SHSFW_configureMessages(SHSFW_msg1, SHSFW_msg2, SHSFW_msg3, SHSFW_fontColor);
	}

	@Override
	public void SHSFW_IConfigHotKey(SHSFW_Operation SHSFW_hotKey,
			boolean SHSFW_isEnabled) {
		SHSFW_configureHotKey(SHSFW_hotKey, SHSFW_isEnabled);
	}

	@Override
	public void SHSFW_IConfigLogo(int SHSFW_logo, int SHSFW_logoBGColor) {
		SHSFW_configureLogo(SHSFW_logo, SHSFW_logoBGColor);
	}

	@Override
	public void SHSFW_IConfigureCustomHotKeyIcon(int SHSFW_iconID) {
		SHSFW_configureCustomHotKeyIcon(SHSFW_iconID);
	}
	//Call  Intent Of your module
	public  void TestModule(){


		Intent intent = new Intent(getApplicationContext(), com.arduino.ujjwal.arduinousb.activity_call.h2oController_MainActivity.class);
		startActivity(intent);
	}

}
