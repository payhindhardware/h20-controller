//********************************Massage handler constants**************************************//
String asiotpf_msg[3][8]= {{"h2o     ","rd      ","wr      ","null    ","null    ","null    ","null    ","null    "},                //<msg1>  //for no msg line replace null by 8 space
                           {"start   ","stop    ","clear   ","sl      ","di      ","ai      ","do      ","ao      "},                 //<msg2>
                           {"monitor ","counters","rs      ","lr      ","es      ","        ","null    ","null    "}};               //<msg3>
//*********************************************************************************************//

//********************************global variables of massage handler functions****************//
//********************Global variable defination********************************//                           
char asiotpf_host_msg[150]={'\0'};                                                              //Bufer to store massage from host
byte asiotpf_index=1;                                                                           //asiotpf_index to address each character of the massage from host
bool asiotpf_stringComplete=false;                                                              //Variable to indiacte the end of massage
String asiotpf_msg_parse[20]={"\0"};                                                            //Buffer to store chunks of massage
byte asiotpf_count_chunk=0;                                                                     //index for chunks of massage
String asiotpf_sc_msgid="\0";                                                                   //To hold the massage id formed during search call
bool asiotpf_busy_flag=false;                                                                   //Busy flag so that commands wont interrupt work going
//******************************************************************************//
//******************Status and error code defination***************************//

#define StatusOK    00
#define msgError    01
#define Timeout     02
#define pinError    05
#define NoCarrRet   03

#define NoWater       01
#define TapFault      02
#define NoCurrent     03 
#define UnknownError  04
#define PartialFilled 06
#define Emergencystop 07
#define invalidtapno 8
//******************************************************************************//


//***************************************Blink time(ms) ***************************************//
#define uf_on_time 100     //Ultra fast mode 2       //Max : 2550ms Min : 10ms // mode 0 = off mode 1 = on mode
#define uf_off_time 100
#define f_on_time 140      //Fast  mode 3
#define f_off_time 140
#define m_on_time 200      //Medium  mode 4
#define m_off_time 200
#define s_on_time 330      //Slow   mode 5
#define s_off_time 330 
#define us_on_time 500     //Ultra slow   mode 6
#define us_off_time 500
//*********************************************************************************************//

//*************************************Constants **********************************************//
#define asiotpfhr_no_of_led  4
#define asiotpfhr_no_of_taps 4
#define asiotpf_debounce_const 50     //approxmately debounces maximum of 1.8ms pulse at 50. more number more width of pulse
#define asiotpf_h2o_calibrator 450    
#define asiotpf_h2o_check_interval 100 //(ms)
#define asiotpf_timeout_period 20000 //(ms)  
#define asiotpf_timeout_h2o_flow 0 //(ltrs)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
//*********************************************************************************************//

//*****************************Solenoid - tap pin map******************************************//
#define sol_tap1 2
#define sol_tap2 3
#define sol_tap3 8
#define sol_tap4 9

//***********************************ledblink variables****************************************//
                                                           //{mode,pre_state,counter,ledpin,singlepulse) {0-6,0-1,0,2-13,0-1)
volatile uint8_t asiotpf_led_blink[asiotpfhr_no_of_led][5] = {{0,0,0,10,0}, // led 1
                                                             {0,0,0,11,0}, // led 2
                                                             {0,0,0,12,0}, // led 3
                                                             {0,0,0,13,0}};// led 4
                                                            
//*********************************************************************************************//

//************************display for a 2 second **********************************************//
volatile uint16_t asiotpf_display[2]={0,0};       //Can be used , not use currently

//*************************Debounce read variables********************************//
byte asiotpf_last_change_state= B11110000;                             //last valid input state 
byte asiotpf_cur_in_state=B11110000;                                   //current temparary state
uint16_t asiotpf_debounce_count[asiotpfhr_no_of_taps]={0,0,0,0};       //Counter for debouncing
unsigned long asiotpf_pulse_count[asiotpfhr_no_of_taps]={0,0,0,0};     //Input pusle counter from the sensor
byte asiotpf_bit_mask=B10000000;                                       // Mask to check indiviual bit
byte asiotpf_input_msak= B11110000;                                   //To mask the input pins
//*******************************************************************************//

//********************Global variables of h2o**************************************************//
bool asiotpf_monitor_flag[4]={0,0,0,0};                               //Flag for each tap
String asiotpf_ref_no_h2o_tap[4]={"\0"};                              // buffer to store reference number for each tap
volatile bool asiotpf_h2o_sl_lr_flag[4]={0,0,0,0};                    // flag to indiacate last responce atcive state for each tap
//*********************************************************************************************//

//******************* led vs completion of water flow constats********************************//
#define asiotpf_fist_inc 0.95
#define asiotpf_second_inc 0.90
#define asiotpf_third_inc 0.85
#define asiotpf_fourth_inc 0.8
//********************************************************************************************//
//********************global variables of asiotpf_sl_lr***************************************//
uint8_t asiotpf_h2o_sl_lr_100ms_timer=0;
volatile double asiotpf_h2o_req[asiotpfhr_no_of_taps]={0,0,0,0};
volatile double asiotpf_h2o_flown[asiotpfhr_no_of_taps]={0,0,0,0};
double asiotpf_h2o_flown_20s_ago[asiotpfhr_no_of_taps]={0,0,0,0};
uint16_t asiotpf_h2o_sl_lr_20s_timer[asiotpfhr_no_of_taps]={0,0,0,0};
//********************************************************************************************//

//*************************Input Output Pin read**********************************************//
bool asiotpf_in_digi[14]={0,0,0,0,0,0,0,0,0,0,0,0,0,0};                                          //Digial input buffer
int asiotpf_in_ana[5]={0,0,0,0,0};                                                             //Analog input buffer
//********************************************************************************************//

//**************************Macros***********************************************************//
#define array_length( x )  ( sizeof( x ) / sizeof( *x ) )       //Macro to find array length

//************************************setup()**************************************************//
void setup()
{
 Serial.begin(9600);
 TCCR0A = 0x02;          //Start timer 0 with 10ms interval
 TCCR0B = 0x05;
 OCR0A  = 0x9D;
 TIMSK0 = 0x02;

 DDRD= B00001111;        //Input ports //*** Depends on number of taps
 PORTD=B11111111;

 asiotpf_last_change_state = PIND & asiotpf_input_msak; //Initial state

 for(int temp_sol=1; temp_sol<=asiotpfhr_no_of_taps; temp_sol++)
 {
  asiotpf_turnoff_sol(temp_sol);
 }
}
//*********************************************************************************************//
void loop() 
{
  asiotpf_bit_mask = B10000000 ;                                                      // Reset bit mask
  asiotpf_cur_in_state=PIND & asiotpf_input_msak;                                     // Read the current state

  for(uint8_t asiotpf_db_lindex=0;asiotpf_db_lindex<asiotpfhr_no_of_taps;asiotpf_db_lindex++) //Loop throgh all four inputs
  {
    if(((asiotpf_last_change_state ^ asiotpf_cur_in_state) & asiotpf_bit_mask) && asiotpf_monitor_flag[asiotpf_db_lindex])         //Check for change in the state
    {
       asiotpf_debounce_count[asiotpf_db_lindex]++;                                    //Increment debounce count
      
    }
    else
    {
     asiotpf_debounce_count[asiotpf_db_lindex]=0;                                     //Reset debounce count
    }
     if(asiotpf_debounce_count[asiotpf_db_lindex] >= asiotpf_debounce_const)         //If debounce count reach 50 ,check for valid change
       {
         asiotpf_debounce_count[asiotpf_db_lindex]=0;                                    //Reset debounce count
         asiotpf_pulse_count[asiotpf_db_lindex]++;                                      //Increment the pulse count
         asiotpf_last_change_state =asiotpf_last_change_state ^ asiotpf_bit_mask;      //Register change state
       }
  
    asiotpf_bit_mask =  asiotpf_bit_mask >> 1;                                        //Shift the bit to mask next input
  }

/*  if(asiotpf_display[1])
  {
    Serial.print("Tap1  : ");
    Serial.println(asiotpf_pulse_count[0]);          //To display no of pulses counted on the console
    Serial.print("Tap2  : ");
    Serial.println(asiotpf_pulse_count[1]);
    Serial.print("Tap3  : ");
    Serial.println(asiotpf_pulse_count[2]);
    Serial.print("Tap4  : ");
    Serial.println(asiotpf_pulse_count[3]);
    asiotpf_display[1]=0;
  }*/
}





//***********************************Timer routines********************************************//
ISR (TIMER0_COMPA_vect )
{
  asiotpf_led_op_routine();
  //asiotpf_display_routine();
  asiotpf_h2o_sl_lr_timer();
}  
//********************************************************************************************//

//*********************************Led operation routine**************************************//
void asiotpf_led_op_routine()
{
  for(uint8_t as_lor_lindex=0;as_lor_lindex<asiotpfhr_no_of_led;as_lor_lindex++)              //Loop through all the leds
  {
    asiotpf_led_blink[as_lor_lindex][2] ++ ;                             //Increment the indiviual counter
    switch(asiotpf_led_blink[as_lor_lindex][0])                          // Check for the mode
    {
      case 0 : pinMode(asiotpf_led_blink[as_lor_lindex][3],OUTPUT);      //Continues off
               digitalWrite(asiotpf_led_blink[as_lor_lindex][3],LOW);
               break;
      case 1 : pinMode(asiotpf_led_blink[as_lor_lindex][3],OUTPUT);      //Cpntinues on
               digitalWrite(asiotpf_led_blink[as_lor_lindex][3],HIGH);  
               break;
      case 2 :
      case 3 :
      case 4 : 
      case 5 :        
      case 6 : asiotpf_led_blink_routine(as_lor_lindex);                 //Blink the led
               break;

      default :; //Serial.println("invalid led mode");                      // invalid mode              
    }
  }
}
//*******************************************************************************************************//

//***********************************led blink routine **************************************************//
void asiotpf_led_blink_routine(uint8_t as_lbr_index)
{
  int as_lbr_ontime=0;
  int as_lbr_offtime=0;
  switch (asiotpf_led_blink[as_lbr_index][0])                           // Check for which flashing mode
  {
    case 2 : as_lbr_ontime = uf_on_time;
             as_lbr_offtime = uf_off_time;
             break;
    case 3 : as_lbr_ontime = f_on_time;
             as_lbr_offtime =f_off_time;
             break;
    case 4 : as_lbr_ontime = m_on_time;
             as_lbr_offtime =m_off_time;
             break;
    case 5 : as_lbr_ontime = s_on_time;
             as_lbr_offtime =s_off_time;
             break;
    case 6 : as_lbr_ontime = us_on_time;
             as_lbr_offtime =us_off_time;
             break;                                        
  }
  
  switch((asiotpf_led_blink[as_lbr_index][1]))                           //Check for the previous state
  {                                                                      //Change the state whenever timer hits defined time
    case 1 : if(asiotpf_led_blink[as_lbr_index][2] * 10 >= as_lbr_ontime)
             {
              pinMode(asiotpf_led_blink[as_lbr_index][3],OUTPUT);
              digitalWrite(asiotpf_led_blink[as_lbr_index][3], LOW);
              asiotpf_led_blink[as_lbr_index][2]=0;
              asiotpf_led_blink[as_lbr_index][1]=0;
              if(asiotpf_led_blink[as_lbr_index][4] == 1)
              {
                asiotpf_led_blink[as_lbr_index][0] = 0;
                asiotpf_led_blink[as_lbr_index][2] = 0;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
              }
             }
    case 0 : if(asiotpf_led_blink[as_lbr_index][2] * 10 >= as_lbr_offtime)
             {
              pinMode(asiotpf_led_blink[as_lbr_index][3],OUTPUT);
              digitalWrite(asiotpf_led_blink[as_lbr_index][3], HIGH);
              asiotpf_led_blink[as_lbr_index][2]=0;
              asiotpf_led_blink[as_lbr_index][1]=1;
             }           
  }
}
//**********************************************************************************************************//

//********************************************asiotpf_display_routine()*************************************//
void asiotpf_display_routine()
{
  asiotpf_display[0] ++;
  if(asiotpf_display[0]>=500)
  {
   asiotpf_display[1]=1; 
   asiotpf_display[0]=0;
  }
}
//*********************************************************************************************************//

//**************************** asiotpf_h2o_sl_lr_timer();**************************************************//
void asiotpf_h2o_sl_lr_timer()
{
  asiotpf_h2o_sl_lr_100ms_timer ++;
  if((asiotpf_h2o_sl_lr_100ms_timer >= (asiotpf_h2o_check_interval/10)))                                                                  //check for 100ms interval
  {
    asiotpf_h2o_sl_lr_100ms_timer=0;
    for(uint8_t as_h_sl_lr_temp=0;as_h_sl_lr_temp< asiotpfhr_no_of_taps; as_h_sl_lr_temp++)
    {
      if(asiotpf_h2o_sl_lr_flag[as_h_sl_lr_temp])
      {
      asiotpf_h2o_flown[as_h_sl_lr_temp] = (double)asiotpf_pulse_count[ as_h_sl_lr_temp]/(double)(asiotpf_h2o_calibrator*2);
      if((asiotpf_h2o_flown[ as_h_sl_lr_temp]>=asiotpf_h2o_req[ as_h_sl_lr_temp])&&(asiotpf_led_blink[as_h_sl_lr_temp][0] != 2))       //Set litre reached and above
      {
        asiotpf_turnoff_sol(as_h_sl_lr_temp+1);  //Turn off the solenoid//tap
        asiotpf_led_blink[as_h_sl_lr_temp][0]=2; //Ultras fast blinking
        asiotpf_h2o_sl_lr_flag[as_h_sl_lr_temp]=0;  //Turn off set ltre as set litre limit is reached
        asiotpf_h2o_sl_lr_status(as_h_sl_lr_temp);  // Send limit reached status to android
      }
      else if((asiotpf_h2o_flown[ as_h_sl_lr_temp]>=asiotpf_fist_inc*asiotpf_h2o_req[ as_h_sl_lr_temp])&&(asiotpf_led_blink[ as_h_sl_lr_temp][0] != 3))
      {
          asiotpf_led_blink[as_h_sl_lr_temp][0]=3;
      }
      else if((asiotpf_h2o_flown[ as_h_sl_lr_temp]>=asiotpf_second_inc*asiotpf_h2o_req[ as_h_sl_lr_temp])&&(asiotpf_led_blink[ as_h_sl_lr_temp][0] != 4))
      {
          asiotpf_led_blink[as_h_sl_lr_temp][0]=4;
      }
       else if((asiotpf_h2o_flown[ as_h_sl_lr_temp]>=0.85*asiotpf_h2o_req[ as_h_sl_lr_temp])&&(asiotpf_led_blink[ as_h_sl_lr_temp][0] != 5))
      {
          asiotpf_led_blink[as_h_sl_lr_temp][0]=5;
      }
       else if((asiotpf_h2o_flown[ as_h_sl_lr_temp]>=0.8*asiotpf_h2o_req[ as_h_sl_lr_temp])&&(asiotpf_led_blink[ as_h_sl_lr_temp][0] != 6))
      {
          asiotpf_led_blink[as_h_sl_lr_temp][0]=6;
      }
      
   /* if((asiotpf_h2o_flown[as_h_sl_lr_temp])-(asiotpf_h2o_flown_20s_ago[as_h_sl_lr_temp]) <=asiotpf_timeout_h2o_flow)
    {   
       asiotpf_h2o_sl_lr_20s_timer[as_h_sl_lr_temp] ++;                                                             //Time out monitoring
       if(asiotpf_h2o_sl_lr_20s_timer[as_h_sl_lr_temp] >= ((double)asiotpf_timeout_period/(double)100))             //check for 20s interval
       {
           asiotpf_turnoff_sol(as_h_sl_lr_temp);  //Turn off the solenoid//tap
           asiotpf_h2o_flown_20s_ago[as_h_sl_lr_temp]=0;                                        //Time out happened
           asiotpf_h2o_sl_lr_flag[as_h_sl_lr_temp]=0;
           asiotpf_h2o_sl_lr_20s_timer[as_h_sl_lr_temp] =0;
           asiotpf_time_out(as_h_sl_lr_temp);
       }
    }
    else
    {
        asiotpf_h2o_flown_20s_ago[as_h_sl_lr_temp]=asiotpf_h2o_flown[as_h_sl_lr_temp];      
        asiotpf_h2o_sl_lr_20s_timer[as_h_sl_lr_temp] =0;
    }*/
   }
  }
 }
  
}
//*******************************************Massage handler***********************************************//
//******************SERIALEVENT() INTERRUPT ROUTINE***************************//
void serialEvent() 
{
  
  while (Serial.available() && asiotpf_stringComplete == false)                                 // Read while we have data
  {
    asiotpf_host_msg[asiotpf_index] = Serial.read();                                            // Read the character and Store it in char array
   
    asiotpf_host_msg[asiotpf_index+1] = '\0';                                                   //Clear the next location
    if (asiotpf_host_msg[asiotpf_index] == (char) 13)                                           // Check for termination character
    {
      asiotpf_index = 1;                                                                        // Reset the asiotpf_index
      asiotpf_stringComplete = true;                                                            // Set completion of read to true
      asiotpf_se_ParseMassage();                                                                //Call function to parse the massage intoo chunks
    }
    else
    {
       asiotpf_index++;                                                                         // Increment,where to write next
    }
  }
}
//*****************************************************************************//

//*****************PARSEMASSAGE()**********************************************//
//Function to parse the massage
void asiotpf_se_ParseMassage()
{
  String asiotpf_pm_str= "";                                                                        // Temp store for each data chunk(chunk buffer)
  
  int asiotpf_pm_count_char = 0;                                                                    // Id ref for each character

  asiotpf_count_chunk=0;                                                                            //Reset the chunk index                                                                              
                                                                                                                                                                      
  int asiotpf_pm_length_chunk=0;                                                                    //To store length of chunk

  boolean asiotpf_pm_eoMassage = false;                                                             //Variable detect end of string
  
  do
  {
    asiotpf_pm_count_char++;                                                                        //Increment character
    
    if((asiotpf_host_msg[asiotpf_pm_count_char] == NULL))                                           //Check end of massage and set eoMassage
    {
      asiotpf_pm_eoMassage= true;
    }
    
    if((asiotpf_host_msg[asiotpf_pm_count_char] != ',') && !asiotpf_pm_eoMassage )                  //Separate string intoo chunks at comma
    {
    asiotpf_pm_str +=asiotpf_host_msg[asiotpf_pm_count_char] ;
    }
    else
    {                                                                                                   
      asiotpf_msg_parse[asiotpf_count_chunk] = asiotpf_pm_str;                                      //Add Spaces to make chunk 8bytes only for 3 massage chunks
      
      if(asiotpf_count_chunk>0 && asiotpf_count_chunk<4)                                            //Check whether massage<msg> chunks only not <refno> or <op1 -- op16>
      {
          asiotpf_pm_length_chunk = (asiotpf_msg_parse[asiotpf_count_chunk]).length();              //Length of chunk(<msg>)
          for(int asiotpf_pm_temp_i=0; asiotpf_pm_temp_i<(8-asiotpf_pm_length_chunk); asiotpf_pm_temp_i++)      //Add spaces to <msg> to make it 8 bytes
          {
         asiotpf_msg_parse[asiotpf_count_chunk] += " ";
          }
      }
      asiotpf_count_chunk++;                                                                         //Increment chunk and clear chunk buffer
      asiotpf_pm_str = "";
    }
    
  } while ((asiotpf_host_msg[asiotpf_pm_count_char] != NULL));                                       // Loop until end of character array
    asiotpf_stringComplete = false;                                                                   //Set falg to true , to recieve next massage from host
    asiotpf_msg_parse[asiotpf_count_chunk+1] = "\0";                                                  //set last location as null
    asiotpf_pm_count_char=0;
    asiotpf_busy_flag=true;
    asiotpf_lp_searchcall();                                                                           //Call search call fuction
}     

//*********************************************************************************************************//

//***********************SEARCHCALL()**********************************************************************//
void asiotpf_lp_searchcall()
{
  for(byte as_lp_sc_tempj=0;as_lp_sc_tempj<3;as_lp_sc_tempj++)                                         //Loop index for <msg line> select , ie <msg1>,<msg2>,<msg3>
  {
     for(byte as_lp_sc_tempi=0;as_lp_sc_tempi<=array_length( asiotpf_msg[1] );as_lp_sc_tempi++)             //Loop index for avalaible <msg> in each line set.
      {
        if((asiotpf_msg_parse[as_lp_sc_tempj+1]).equalsIgnoreCase (asiotpf_msg[as_lp_sc_tempj][as_lp_sc_tempi]))   //Compare with massage sets
           {
             asiotpf_sc_msgid += as_lp_sc_tempi+1;                                                                                                      
           }
      }      
   }
  // Serial.println("msgid :"+ asiotpf_sc_msgid);                                                     //To dosplay msg id
   switch((asiotpf_sc_msgid.toInt()))                                                                 //Map msg id to functions
   {
    case 111: asiotpf_monitor(1,HIGH);    // 1 - start , 0 - stop
              asiotpf_busy_flag=false;    //High - led on , low- led off
              break;
    case 121: asiotpf_monitor(0,LOW);   
              asiotpf_busy_flag=false;                                                               
              break;   
    case 132: asiotpf_clear_counter();
              asiotpf_busy_flag=false;
              break; 
    case 143: asiotpf_read_status();
              asiotpf_busy_flag=false;
              break;        
    case 144: asiotpf_h2o_sl_lr_flag_set();
              asiotpf_busy_flag=false;
              break;  
    case 145: asiotpf_h2o_sl_es_flag_set();
              asiotpf_busy_flag=false;
              break;
    case 256: asiotpf_lsc_read_digi();
              asiotpf_busy_flag=false;
              break; 
    case 266: asiotpf_lsc_read_ana();
              asiotpf_busy_flag=false;
              break;           
    case 376: asiotpf_lsc_write_digi();
              asiotpf_busy_flag=false;
              break;
    case 386: asiotpf_lsc_write_ana();
              asiotpf_busy_flag=false;
              break;                            
    default : asiotpf_msg_error();
              asiotpf_busy_flag=false;
   }
   asiotpf_sc_clr_parse_buffer();                                               //Clear the parse buffer  & msgid 
}
//**********************************************************************************************************//

//**************************************asiotpf_monitor****************************************************//
void asiotpf_monitor(volatile uint8_t asiotpf_blink_speed , bool asiotpf_temp_flag)
{
  if(asiotpf_msg_parse[4].toInt() >0 && asiotpf_msg_parse[4].toInt() <asiotpfhr_no_of_taps+1)
  {
    asiotpf_led_blink[asiotpf_msg_parse[4].toInt()-1][0]=asiotpf_blink_speed;           //led
    asiotpf_monitor_flag[asiotpf_msg_parse[4].toInt()-1]=asiotpf_temp_flag;             //Monitor flag
    asiotpf_ref_no_h2o_tap[asiotpf_msg_parse[4].toInt()-1]=asiotpf_msg_parse[0];        //Store the reference number of perticular tap
    asiotpf_status_ok();
  }
  else
    asiotpf_invalid_tap();
}
//**********************************************************************************************************//

//*************************************asiotpf_clear_counter()**********************************************//
void asiotpf_clear_counter()
{
   if(asiotpf_msg_parse[4].toInt() >0 && asiotpf_msg_parse[4].toInt() <asiotpfhr_no_of_taps+1)
   {
    asiotpf_pulse_count[asiotpf_msg_parse[4].toInt()-1]=0;                  //clear counter
    if(asiotpf_monitor_flag[(asiotpf_msg_parse[4].toInt()-1)])                                 //check monitor flag
    {
    asiotpf_led_blink[asiotpf_msg_parse[4].toInt()-1][0]=1;                                      //set led on if monitor is on
    }
    else
    {
    asiotpf_led_blink[asiotpf_msg_parse[4].toInt()-1][0]=0;                                     //reset led if monitor is off
    }
    asiotpf_status_ok();
   }
   else
    asiotpf_invalid_tap();
}
//**********************************************************************************************************//

//*************************************asiotpf_read_status()**********************************************//
void asiotpf_read_status()
{
   if(asiotpf_msg_parse[4].toInt() >0 && asiotpf_msg_parse[4].toInt() <asiotpfhr_no_of_taps+1)
   {
    
    asiotpf_read_count();
   }
   else
    asiotpf_invalid_tap();
}
//**********************************************************************************************************//

//***********************************asiotpf_h2o_sl_lr_flag_set()************************************************//
void asiotpf_h2o_sl_lr_flag_set()
{
   if(asiotpf_msg_parse[4].toInt() >0 && asiotpf_msg_parse[4].toInt() <asiotpfhr_no_of_taps+1)
   {
    asiotpf_h2o_req[asiotpf_msg_parse[4].toInt()-1]=(double)(asiotpf_msg_parse[5].toFloat());    //store required amount of water
    asiotpf_ref_no_h2o_tap[asiotpf_msg_parse[4].toInt() - 1] = asiotpf_msg_parse[0];           //store the reference number
    asiotpf_pulse_count[asiotpf_msg_parse[4].toInt() - 1]=0;                                   //clear the pulse count
    asiotpf_h2o_sl_lr_flag[asiotpf_msg_parse[4].toInt() - 1]=1;                                // set last responce flag
    asiotpf_h2o_flown_20s_ago[asiotpf_msg_parse[4].toInt() - 1]=0;                             // reset amount of water flown in last 20s
     asiotpf_turnon_sol(asiotpf_msg_parse[4].toInt());                                         //Turn on the solenoid//tap
    if(asiotpf_monitor_flag[(asiotpf_msg_parse[4].toInt()-1)])                                 //check monitor flag
    {
    asiotpf_led_blink[asiotpf_msg_parse[4].toInt()-1][0]=1;                                      //set led on if monitor is on
    }
    else
    {
    asiotpf_led_blink[asiotpf_msg_parse[4].toInt()-1][0]=0;                                     //reset led if monitor is off
    }
    asiotpf_status_ok(); 
   }
   else
    asiotpf_invalid_tap();
}
//**********************************************************************************************************//

//**************************************asiotpf_h2o_sl_es_flag_set();***************************************//
void asiotpf_h2o_sl_es_flag_set()
{
    if(asiotpf_msg_parse[4].toInt() >0 && asiotpf_msg_parse[4].toInt() <asiotpfhr_no_of_taps+1)
   {
    asiotpf_emergency_stop();
     asiotpf_turnoff_sol(asiotpf_msg_parse[4].toInt());                                        //turn the solenoid off \ tap
    asiotpf_pulse_count[asiotpf_msg_parse[4].toInt() - 1]=0;                                   //clear the pulse count
    asiotpf_h2o_sl_lr_flag[asiotpf_msg_parse[4].toInt() - 1]=0;                                // set last responce flag
    asiotpf_h2o_flown_20s_ago[asiotpf_msg_parse[4].toInt() - 1]=0;                             // reset amount of water flown in last 20s
    asiotpf_h2o_req[asiotpf_msg_parse[4].toInt() - 1]=0;
    asiotpf_h2o_flown[asiotpf_msg_parse[4].toInt() - 1]=0;
    if(asiotpf_monitor_flag[(asiotpf_msg_parse[4].toInt()-1)])                                 //check monitor flag
    {
    asiotpf_led_blink[asiotpf_msg_parse[4].toInt()-1][0]=1;                                      //set led on if monitor is on
    }
    else
    {
    asiotpf_led_blink[asiotpf_msg_parse[4].toInt()-1][0]=0;                                     //reset led if monitor is off
    }

   }
   else
    asiotpf_invalid_tap();
}





//***************************************asiotpf_status_ok*************************************************//
void asiotpf_status_ok()
{
             Serial.print(asiotpf_msg_parse[0]);                                 //statusok
             asiotpf_msg_sep();
             Serial.print(StatusOK);
             asiotpf_msg_sep();
             Serial.print(StatusOK);
             asiotpf_msg_sep();
             asiotpf_msg_sep();
             Serial.print("\r");
             Serial.println();  
}
//**********************************************************************************************************//

//***************************************asiotpf_invalid_tap*************************************************//
void asiotpf_invalid_tap()
{
             Serial.print(asiotpf_msg_parse[0]);                                 //invalid tap
             asiotpf_msg_sep();
             Serial.print(StatusOK);
             asiotpf_msg_sep();
             Serial.print(invalidtapno);
             asiotpf_msg_sep();
             asiotpf_msg_sep();
             Serial.print("\r");
             Serial.println();  
}
//**********************************************************************************************************//

//***************************************asiotpf_invalid_tap*************************************************//
void asiotpf_invalid_pin()
{
             Serial.print(asiotpf_msg_parse[0]);                                 //invalid tap
             asiotpf_msg_sep();
             Serial.print(StatusOK);
             asiotpf_msg_sep();
             Serial.print(pinError);
             asiotpf_msg_sep();
             asiotpf_msg_sep();
             Serial.print("\r");
             Serial.println();  
}
//**********************************************************************************************************//

//***************************************asiotpf_msg_error*************************************************//
void asiotpf_msg_error()
{
             Serial.print(asiotpf_msg_parse[0]);                                 //massage error
             asiotpf_msg_sep();
             Serial.print(msgError);
             asiotpf_msg_sep();
             asiotpf_msg_sep();
             asiotpf_msg_sep();
             Serial.print("\r");
             Serial.println();  
}
//**********************************************************************************************************//

//***************************************asiotpf_read_status*************************************************//
void asiotpf_read_count()
{
             Serial.print(asiotpf_msg_parse[0]);                                 //statusok
             asiotpf_msg_sep();
             Serial.print(StatusOK);
             asiotpf_msg_sep();
             Serial.print(StatusOK);
             asiotpf_msg_sep();
             asiotpf_msg_sep();
             Serial.print(asiotpf_msg_parse[4]);
             asiotpf_msg_sep();
             Serial.print((double)(asiotpf_pulse_count[asiotpf_msg_parse[4].toInt()-1])/(double)(asiotpf_h2o_calibrator*2));
             Serial.print("\r");
             Serial.println();  
}
//**********************************************************************************************************//

//*************************************asiotpf_h2o_sl_lr_status()******************************************//
void asiotpf_h2o_sl_lr_status(uint8_t lr_temp_index)
{
             Serial.print(asiotpf_ref_no_h2o_tap[lr_temp_index]);
             asiotpf_msg_sep();
             Serial.print(StatusOK);
             asiotpf_msg_sep();
             Serial.print(StatusOK);
             asiotpf_msg_sep();
             asiotpf_msg_sep();
             Serial.print(lr_temp_index+1);
             asiotpf_msg_sep();
             Serial.print(asiotpf_h2o_flown[lr_temp_index]);
             Serial.print("\r");
             Serial.println();  
}

//*********************************************************************************************************//

//*************************************asiotpf_time_out()******************************************//
void asiotpf_time_out(uint8_t lr_temp_index)
{
             Serial.print(asiotpf_ref_no_h2o_tap[lr_temp_index]);
             asiotpf_msg_sep();
             Serial.print(Timeout);
             asiotpf_msg_sep();
             asiotpf_msg_sep();
             asiotpf_msg_sep();
             Serial.print(lr_temp_index+1);
             asiotpf_msg_sep();
             Serial.print(asiotpf_h2o_flown[lr_temp_index]);
             Serial.print("\r");
             Serial.println();  
}

//*********************************************************************************************************//

//***************************************asiotpf_emergency_stop*************************************************//
void asiotpf_emergency_stop()
{
             Serial.print(asiotpf_msg_parse[0]);                                 //statusok
             asiotpf_msg_sep();
             Serial.print(StatusOK);
             asiotpf_msg_sep();
             Serial.print(Emergencystop);
             asiotpf_msg_sep();
             asiotpf_msg_sep();
             Serial.print(asiotpf_msg_parse[4]);
             asiotpf_msg_sep();
             Serial.print(((double)(asiotpf_pulse_count[asiotpf_msg_parse[4].toInt()-1])/(double)(asiotpf_h2o_calibrator*2)));
             Serial.print("\r");
             Serial.println();  
}
//**********************************************************************************************************//





//**************************************asiotpf_sc_clr_parse_buffer()***************************************//
void asiotpf_sc_clr_parse_buffer()
{
  for(byte asiotpf_sc_clr_pb_tempi=0;asiotpf_sc_clr_pb_tempi<20;asiotpf_sc_clr_pb_tempi++)             //Clear the parse buffer
  { 
    asiotpf_msg_parse[asiotpf_sc_clr_pb_tempi]="\0";
  }
  asiotpf_sc_msgid="\0";                                                                               //Clear the massage buffer
}
//***********************************************************************************************************//

//****************************************msg_seperator()****************************************************/
void asiotpf_msg_sep()
{
  Serial.print(F(","));
}
//***********************************************************************************************************//

//*************************************INPUT/OUTPUT/CommonPlatform*******************************************//


//*****************************************asiotpf_lsc_read_digi()***********************************************//
void asiotpf_lsc_read_digi()                                                                  //Digital read
{
  if((asiotpf_msg_parse[4]).toInt() == 99)                                                    //Check pin == 99?
  {
    for(char as_lsc_r_d_tempi=1;as_lsc_r_d_tempi<=14;as_lsc_r_d_tempi++)
    {
      pinMode(as_lsc_r_d_tempi-1 , INPUT);
      asiotpf_in_digi[as_lsc_r_d_tempi-1] = digitalRead(as_lsc_r_d_tempi-1);
    }
    
             Serial.print(asiotpf_msg_parse[0]);                                             //Digital all pin read , Responce
             asiotpf_msg_sep();
             Serial.print(StatusOK);
             asiotpf_msg_sep();
             asiotpf_msg_sep();
             asiotpf_msg_sep();
             for(char asiotf_sm_tempi=1;asiotf_sm_tempi<=14;asiotf_sm_tempi++)
             {
              Serial.print(asiotpf_in_digi[asiotf_sm_tempi-1]);
              asiotpf_msg_sep();
             }
              Serial.println();
              asiotpf_busy_flag=false;                                                        //Status mode=2 ,read all pins
  }  
    else if((asiotpf_msg_parse[4]).toInt() >-1 && (asiotpf_msg_parse[4]).toInt() <14)         //Check pin valid?
    {
      pinMode( (asiotpf_msg_parse[4]).toInt(), INPUT);
      asiotpf_in_digi[0]= digitalRead((asiotpf_msg_parse[4]).toInt());
      
             Serial.print(asiotpf_msg_parse[0]);                                              //Digital single pin read , Responce
             asiotpf_msg_sep();
             Serial.print(StatusOK);
             asiotpf_msg_sep();
             asiotpf_msg_sep();
             asiotpf_msg_sep();
             Serial.println(asiotpf_in_digi[0]);
             asiotpf_busy_flag=false;                                                        //status mode=1 , read a single pin
    }
    else
    {
   asiotpf_invalid_pin();                                                                    // Status mode=10 to report pin error
    }
}
//***********************************************************************************************************//

//***************************************asiotpf_lsc_read_ana()**********************************************//
void asiotpf_lsc_read_ana()                                                                   //Analog read
{
  if((asiotpf_msg_parse[4]).toInt() == 99)                                                    //Check pin == 99?
  {
    for(char as_lsc_r_a_tempi=0;as_lsc_r_a_tempi<6;as_lsc_r_a_tempi++)
    {
      asiotpf_in_ana[as_lsc_r_a_tempi] = analogRead(as_lsc_r_a_tempi);
    }
             Serial.print(asiotpf_msg_parse[0]);                                 //Analog all pin read , Responce
             asiotpf_msg_sep();
             Serial.print(StatusOK);
             asiotpf_msg_sep();
             asiotpf_msg_sep();
             asiotpf_msg_sep();
             for(char asiotf_sm_tempi=0;asiotf_sm_tempi<6;asiotf_sm_tempi++)
             {
              Serial.print(asiotpf_in_ana[asiotf_sm_tempi]);
              asiotpf_msg_sep();
             }
              Serial.println();
              asiotpf_busy_flag=false;
  }  
    else if((asiotpf_msg_parse[4]).toInt() >-1 && (asiotpf_msg_parse[4]).toInt() <6)         //Check pin valid?
    {
             asiotpf_in_ana[0]= analogRead((asiotpf_msg_parse[4]).toInt());
             Serial.print(asiotpf_msg_parse[0]);                                 //Analog single pin read , Responce
             asiotpf_msg_sep();
             Serial.print(StatusOK);
             asiotpf_msg_sep();
             asiotpf_msg_sep();
             asiotpf_msg_sep();
             Serial.println(asiotpf_in_ana[0]);
             asiotpf_busy_flag=false;
    }
    else
    {
    asiotpf_invalid_pin();                                                                  // Status mode=10 to report pin error
    }

   
}
//***********************************************************************************************************//

//*****************************************asiotpf_lsc_write_ana()******************************************//
void asiotpf_lsc_write_digi()                                                                 //Digital write
{
  if((asiotpf_msg_parse[4]).toInt() == 99)                                                    //Check pin == 99?
  {
    for(char as_lsc_w_d_tempi=1;as_lsc_w_d_tempi<=14;as_lsc_w_d_tempi++)
    {
      pinMode(as_lsc_w_d_tempi-1 , OUTPUT);
      digitalWrite(as_lsc_w_d_tempi-1,(asiotpf_msg_parse[3+as_lsc_w_d_tempi]).toInt());
    }
  asiotpf_status_ok();                                                                      //Status ok
  }  
    else if((asiotpf_msg_parse[4]).toInt() >-1 && (asiotpf_msg_parse[4]).toInt() <14)         //Check pin valid?
    {
      pinMode( (asiotpf_msg_parse[4]).toInt(), OUTPUT);
      digitalWrite((asiotpf_msg_parse[4]).toInt(),(asiotpf_msg_parse[5]).toInt());
      asiotpf_status_ok();                                                                  //status ok
    }
    else
    {
    asiotpf_invalid_pin();                                                                  // report pin error
    }
  
}
//***********************************************************************************************************//

//*****************************************asiotpf_lsc_write_ana()******************************************//
void asiotpf_lsc_write_ana()                                                                  //Analog write
{
  char as_lsc_w_a_tempj=1;
  if((asiotpf_msg_parse[4]).toInt() == 99)                                                    //Check pin == 99?
  {
    for(char as_lsc_w_a_tempi=3;as_lsc_w_a_tempi<=11;as_lsc_w_a_tempi++)
    {
      switch(as_lsc_w_a_tempi)
      {
      case 3 :
      case 5 :
      case 6 :
      case 9 :
      case 10 : 
      case 11 : pinMode(as_lsc_w_a_tempi , OUTPUT);
                analogWrite(as_lsc_w_a_tempi,(asiotpf_msg_parse[4+as_lsc_w_a_tempj]).toInt());
                asiotpf_status_ok();                           //Status ok
                as_lsc_w_a_tempj++;
                break;
 
      }
    }
                                                                      
  }
  else
  {  
     switch((asiotpf_msg_parse[4]).toInt())
      {
      case 3 :
      case 5 :
      case 6 :
      case 9 :
      case 10 : 
      case 11 :  pinMode( (asiotpf_msg_parse[4]).toInt(), OUTPUT);
                 analogWrite((asiotpf_msg_parse[4]).toInt(),(asiotpf_msg_parse[5]).toInt());
                 asiotpf_status_ok();                           //Status ok                                            
                 break;   
      default :  asiotpf_invalid_pin(); break;                                                     //report pin error
    {
   }                                                                       
  }
 }
}
//***********************************************************************************************************//

//************************************Solenoid on off********************************************************//
void asiotpf_turnon_sol(int tap_no)
{
  switch(tap_no)
  {
    case 1 : pinMode(sol_tap1 , OUTPUT);
             digitalWrite(sol_tap1 , 0);
             break;
    case 2 : pinMode(sol_tap2 , OUTPUT);
             digitalWrite(sol_tap2 , 0);
             break;
    case 3 : pinMode(sol_tap3 , OUTPUT);
             digitalWrite(sol_tap3 , 0);
             break;
    case 4 : pinMode(sol_tap4 , OUTPUT);
             digitalWrite(sol_tap4 , 0);   
             break;                        
  }
}

void asiotpf_turnoff_sol(int tap_no)
{
  switch(tap_no)
  {
    case 1 : pinMode(sol_tap1 , OUTPUT);
             digitalWrite(sol_tap1 , 1);
             break; 
    case 2 : pinMode(sol_tap2 , OUTPUT);
             digitalWrite(sol_tap2 , 1);
             break; 
    case 3 : pinMode(sol_tap3 , OUTPUT);
             digitalWrite(sol_tap3 , 1);
             break; 
    case 4 : pinMode(sol_tap4 , OUTPUT);
             digitalWrite(sol_tap4 , 1);  
             break;                          
  }
}

//***********************************************************************************************************//


