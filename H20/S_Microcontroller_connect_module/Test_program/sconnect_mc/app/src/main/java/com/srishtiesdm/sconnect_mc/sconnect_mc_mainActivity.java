package com.srishtiesdm.sconnect_mc;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.srishti.sprn.SPRN_Attributes;
import com.srishti.sprn.SPRN_InitializePrinter;
import com.srishti.sprn.SPRN_Print;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class sconnect_mc_mainActivity extends Activity {
    S_CuC scuc = null;   //Create microcontroller library object

    SPRN_Print h2o_print =null; //Printer initialisation
    SPRN_Attributes stp;
    ArrayList<SPRN_Attributes> h2o_print_AL;

    //Timer
    Timer recieve_msg;
    boolean split_time=false;
    boolean on_off_toggle=false;

    //Expected device product ID
    private int SCuC_expected_device_product_id= 67 ; // product id of arduino UNO

    private int status_microcontroller=-10;

    Button b_on_off,b_init_tp,b_init_mc;
    Boolean toggle_switch=false;
    String on_led = "on"+"\r";
    String off_led = "off"+"\r";
    String display_string=null;
    TextView t_responce ;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        //Set layout
        setContentView(asiotpf_main_layout());
        //Set on click listener
        set_on_click_listener();
        //setup timers
        setup_timers();
    }


    //View with buttons
    private View asiotpf_main_layout() {
        Log.d("d_lst_act_mod", "asiotpf_main_layout");
        //Main view
        RelativeLayout ml_relativeLayout = new RelativeLayout(this);
        ml_relativeLayout.setBackgroundColor(Color.parseColor("#ffffff"));
        RelativeLayout.LayoutParams ml_size = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT, RelativeLayout.LayoutParams.FILL_PARENT);
        ml_relativeLayout.setId(R.id.ml_relative);
        ml_relativeLayout.setLayoutParams(ml_size);
        Log.d("d_lst_act_mod", "after relative layout");
        //Button on off
        Button b_on_off = new Button(this);
        b_on_off.setText("Turn on led");
        b_on_off.setId(R.id.b_on_off);


        //Button initialise
        Button init_tp = new Button(this);
        RelativeLayout.LayoutParams mkl = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,RelativeLayout.LayoutParams.WRAP_CONTENT);
        mkl.addRule(RelativeLayout.RIGHT_OF,R.id.b_on_off);
        init_tp.setText("init_TP");
        init_tp.setId(R.id.b_init_tp);
        init_tp.setLayoutParams(mkl);

        //Button initialise
        Button init_mc = new Button(this);
        RelativeLayout.LayoutParams mcl = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,RelativeLayout.LayoutParams.WRAP_CONTENT);
        mcl.addRule(RelativeLayout.RIGHT_OF,R.id.b_init_tp);
        init_mc.setText("init_MC");
        init_mc.setId(R.id.b_init_MC);
        init_mc.setLayoutParams(mcl);

        //Text view
        TextView t_responce = new TextView(this);
        RelativeLayout.LayoutParams tdr = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,RelativeLayout.LayoutParams.WRAP_CONTENT);
        tdr.addRule(RelativeLayout.BELOW,R.id.b_init_tp);
        t_responce.setTextColor(Color.parseColor("#000000"));
        t_responce.setText("Your text displays here");
        t_responce.setId(R.id.t_resp_rec);
        t_responce.setLayoutParams(tdr);


        Log.d("d_lst_act_mod", "after button");

        ml_relativeLayout.addView(b_on_off);
        ml_relativeLayout.addView(init_tp);
        ml_relativeLayout.addView(init_mc);
        ml_relativeLayout.addView(t_responce);
        Log.d("d_lst_act_mod", "after adding button to main layout");
        return ml_relativeLayout;
    }

    private void set_on_click_listener()
    {

        b_on_off=(Button)findViewById(R.id.b_on_off);
        b_init_tp=(Button)findViewById(R.id.b_init_tp);
        b_init_mc=(Button)findViewById(R.id.b_init_MC) ;
        t_responce=(TextView)findViewById(R.id.t_resp_rec);

         View.OnClickListener onoffListener = new View.OnClickListener() {
        public void onClick(View v) {
            toggle_switch=!toggle_switch;
            if(toggle_switch)
            {
                b_on_off.setText("turnOff");

                  send_to_printer("turn on");


                scuc.SCuC_transmit_data(on_led);
                Log.d("D_SCMC", "Command sent(string)(byte)  : "+on_led );

            }
            else
            {
                b_on_off.setText("turnON");
                 send_to_printer("turn off");
                scuc.SCuC_transmit_data(off_led);
                Log.d("D_SCMC", "Command sent(string)(byte)  : "+off_led );



                Log.d("D_SCMC", "Command sent(string)(byte)  : "+off_led );
            }
        }
    };

        View.OnClickListener b_initpListener = new View.OnClickListener() {
            public void onClick(View v)
            {
                new Thread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        init_thrmal_printer();  //Initialise thermal printer
                    }

                }).start();
            }

        };

        View.OnClickListener b_inimcListener = new View.OnClickListener() {
            public void onClick(View v)
            {
                new Thread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        scuc = scuc.SCuC_getInstance(getApplicationContext()); //Get the instance
                        scuc.SCuC_preInitialise(67,9025,false,"\r",64); //Pre initialise

                        status_microcontroller=scuc.SCuC_initialise(); //Initialise microcontroller
                        display_microcontroller_status(status_microcontroller); //Display the status of microcontroller
                    }

                }).start();
            }

        };
        b_on_off.setOnClickListener(onoffListener);
        b_init_tp.setOnClickListener(b_initpListener);
        b_init_mc.setOnClickListener(b_inimcListener);
    }

    private void init_thrmal_printer()
    {
        Log.d("d_lst_act_mod","entered_functrion_init_thermal_printer");
        //Get the instance
        h2o_print= SPRN_Print.SPRN_getInstance(this);

                Log.d("d_lst_act_mod","before_init_thermal_printer");
                int h2o_printer_resp = h2o_print.SPRN_initializePrinter();

                Log.d("d_lst_act_mod","after_init_thermal_printer");
                if(h2o_printer_resp == 0)
                {
                    display_toast("Printer succesfully connected");
                }
                else if(h2o_printer_resp == -1 )
                {
                    display_toast("Printer cannot be connected,TRY AGAIN");
                }
                else
                {
                    display_toast("Unknown error");
                }

    }

    private void de_init_thermal_printer()
    {

    }
    //To print on printer
    private void send_to_printer(String msg)
    {
        Log.d("d_print","before stp attribute");
        stp = new SPRN_Attributes(2,msg,null,null,0,false,Color.BLACK,0,10,10);
        h2o_print_AL = new ArrayList<SPRN_Attributes>();
        h2o_print_AL.add(stp);
        Log.d("d_print","before adding arraylist to print"+ h2o_print_AL.toString());
        h2o_print.SPRN_printText(h2o_print_AL);
        Log.d("d_print","after adding arraylist to print");
    }

    private void display_toast(final String msg)
    {
        //Handler to disply the toast
        Handler Toast_Handler = new Handler(this.getMainLooper());
        //Run On UI thread
        Toast_Handler.post(new Runnable()
        {
            @Override
            public void run()
            {
                Toast toast = Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT);
                toast.show();
            }
        });
    }

    private void display_textview(final String msg_dt)
    {
        //Handler to disply the toast
        Handler Toast_Handler = new Handler(this.getMainLooper());
        //Run On UI thread
        Toast_Handler.post(new Runnable()
        {
            @Override
            public void run()
            {
               t_responce.setText(msg_dt);
            }
        });
    }

    private void display_microcontroller_status(int status)
    {
        switch(status)
        {
            case 0 : display_toast("H2o_controller_succcesfully connnected");
                      break;
            case -10 : display_toast("No usb devices found");
                      break;
            case -9 : display_toast("Could not find H2O customer unit");
                       break;
            case -8  : display_toast("H2O Customer unit connected");
                        break;
            case -7  : display_toast("Device usb port is not open");
                          break;
            case -6  : display_toast("Device usb port is null");
                        break;
            case -5  : display_toast("Device Permission not granted");
                           break;
            case -4  : display_toast("Error in opening the microcontroller port");
                          break;
        }
    }

    private void setup_timers()
    {
        recieve_msg = new Timer();   //Create a timer
        recieve_msgs recieve_msgs_t = new recieve_msgs();
        recieve_msg.schedule(recieve_msgs_t,0,200);  //200ms
    }




    class recieve_msgs extends TimerTask
    {
        public void run()
        {

            new Thread(new Runnable()
            {
                @Override
                public void run() {
                    if (scuc != null) {
                        split_time = !split_time;
                        if (split_time) {
                            on_off_toggle = !on_off_toggle;
                            if (on_off_toggle) {
                                scuc.SCuC_transmit_data(on_led);
                            } else {
                                scuc.SCuC_transmit_data(off_led);
                            }
                        }
                        String temp = null;
                        int tem_int = 0;
                        temp = scuc.SCuC_read_data_buffer();
                        tem_int = scuc.SCuC_get_msg_count_in_buffer();
                        if (temp != null) {
                            display_textview(temp + "   " + Integer.toString(tem_int));
                            Log.d("D_SCuC_data", Integer.toString(tem_int));
                        }


                    }
                }
            }).start();




        }

    }



}
