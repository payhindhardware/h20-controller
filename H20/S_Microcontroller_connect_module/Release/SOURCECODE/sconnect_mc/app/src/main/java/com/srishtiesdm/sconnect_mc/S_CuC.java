package com.srishtiesdm.sconnect_mc;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.felhr.usbserial.*;
import com.srishti.sprntp.MainActivity;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by SrishtiESDM on 04-Oct-16.
 */
public class S_CuC
{
    //Context variable
    Context SCuC_context=null;
    
    //Usb variables
    private UsbManager SCuC_usbManager;                       //USB manager
    private UsbDevice SCuC_usbDevice=null;            //Object to hold arduino device
    private UsbSerialDevice SCuC_serialPort=null;     //Static becuase it is used in commands class.
    private UsbDeviceConnection SCuC_connection=null; //usb device connection
    private String SCuC_ask_permission = "com.android.example.USB_PERMISSION"; //String used in intent
    private boolean SCuC_flag_is_device_connected=false;
    
    //Expected device product ID
    private int SCuC_expected_device_product_id= 67 ; //Default product id of arduino UNO
    private int SCuC_expected_device_vendor_id= 9025 ; //Default vendor id of arduino UNO

    //Status variable
    private int SCuC_status_var=-10;               //Default
    private boolean  SCuC_initialise_on_device_attach=false; //As the namse says :D


    //To hold data and counts
    private String[] SCuC_msgs_in;            //Buffer to hold input msgs ,from device
    private int SCuC_inmsgs_count=0;        //Input msgs count
    private int SCuC_in_msgs_buffer_size=64; //By default
    private String SCuC_massage_break_character="\r";  //Character to determine the massage break
    private String SCuC_temp_make_string=null; //To make string from recieved characters
    private boolean SCuC_flag_buffer_full=false; // To indicate the overflow of buffer to hold input massages
    private static S_CuC SCuC_new_instance=null;           //New object/instance of class SCuC
    
    
    
    
    //Empty constructor
    public S_CuC()
    {
        Log.d("D_SCuC", "SCuC object created");
        //Empty constructor
    }

    //Constructor to get Context
    private S_CuC(Context context)
    {
        Log.d("D_SCuC", "SCuC object created with cpying context");
        SCuC_context = context;         //Get the context from main activity
    }

    //Get instance
    public static  S_CuC SCuC_getInstance(Context context)
    {
        Log.d("D_SCuC", "getting singleton instance of SCUC");

        if(SCuC_new_instance==null)
        {
            SCuC_new_instance=new S_CuC(context);  //Create a object
        }
        return SCuC_new_instance;
    }

    //  //Get instance
    public static void SCuC_clearInstance()
    {
        if(SCuC_new_instance!=null)
        {
            SCuC_new_instance=null;
        }
    }



    // Pre initialise
    public void SCuC_preInitialise( int expected_device_product_id,int expected_device_vendor_id,boolean initialise_on_device_attach,String massage_break_character
     ,int in_msgs_buffer_size)
    {
        SCuC_expected_device_product_id=expected_device_product_id; //Copy the product id
        SCuC_expected_device_vendor_id= expected_device_vendor_id; //Copy the vendor id
        SCuC_initialise_on_device_attach=initialise_on_device_attach; //Get initialise on device attach flag
        SCuC_massage_break_character = massage_break_character; //Get msg break character
        SCuC_in_msgs_buffer_size=in_msgs_buffer_size+1;        //Input massage from the device buffer size ,+1 to compansate index zero
        SCuC_msgs_in = new String[ SCuC_in_msgs_buffer_size];  //Get the Memory for imput buffer
    }

    
    //Create a new instance
    public int SCuC_initialise()
    {
        Log.d("D_SCuC", "SCuC initialisation");
        SCuC_setup_usb_variables(); //Set up intent filters , get usb manager
        SCuC_connect_usb(); //Discover the usb devices connected and connect to valid device
        SCuC_init_array(); //Initialise the arrays
        return SCuC_status_var;
    }
    
   //Initial setup of usb variables
    private void SCuC_setup_usb_variables()
    {
        Log.d("D_SCuC", "[private]SCuC setup usb variables");
        SCuC_usbManager = (UsbManager) SCuC_context.getApplicationContext().getSystemService(SCuC_context.USB_SERVICE); //Get usb manager
        IntentFilter filter = new IntentFilter();  //Create a intent filter
        filter.addAction(SCuC_ask_permission);    //Add usb permission intend
        filter.addAction(SCuC_usbManager.ACTION_USB_DEVICE_ATTACHED); //usb intent device attached
        filter.addAction(SCuC_usbManager.ACTION_USB_DEVICE_DETACHED); //usb intent device detached
        SCuC_context.getApplicationContext().registerReceiver(broadcastReceiver, filter); //Add broad cast reciever
        return;
    }

    //Broadcasts a signal open for all the USB devices to get connect.
    private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() { //Broadcast Receiver to automatically start and stop the Serial connection.
        @Override
        public void onReceive(Context context, Intent intent)   //On recieving usb intents
        {
            try {
                if (intent.getAction().equals(SCuC_ask_permission))  //If intent was asking permission
                {
                    boolean granted = intent.getExtras().getBoolean(SCuC_usbManager.EXTRA_PERMISSION_GRANTED);  //Check permission granted or denied
                    if (granted)
                    {

                        Log.d("D_SCuC", "[private]SCuC extra usb permission granted");
                        SCuC_connection = SCuC_usbManager.openDevice(SCuC_usbDevice); //Get the usb connection
                        Log.d("D_SCuC", "[private]SCuC extra usb permission granted, device details"+SCuC_usbDevice.toString());
                        Log.d("D_SCuC", "[private]SCuC extra usb permission granted , Vid , Pid"+ Integer.toString(SCuC_usbDevice.getProductId())+ Integer.toString(SCuC_usbDevice.getVendorId()));

                        SCuC_serialPort = UsbSerialDevice.createUsbSerialDevice(SCuC_usbDevice, SCuC_connection); //Get the serial port

                        if (SCuC_serialPort != null)     //If serial port has obtained
                        {
                            Log.d("D_SCuC", "[private]SCuC extra usb permission granted , Port is not null");
                            if (SCuC_serialPort.open())   //If serial port is open
                            {
                                //Set Serial Connection Parameters.
                                Log.d("D_SCuC", "[private]SCuC extra usb permission granted , Port is open");
                                SCuC_serialPort.setBaudRate(9600);  //Set the baud rate
                                SCuC_serialPort.setDataBits(UsbSerialInterface.DATA_BITS_8);
                                SCuC_serialPort.setStopBits(UsbSerialInterface.STOP_BITS_1);
                                SCuC_serialPort.setParity(UsbSerialInterface.PARITY_NONE);
                                SCuC_serialPort.setFlowControl(UsbSerialInterface.FLOW_CONTROL_OFF);
                                SCuC_serialPort.read(mCallback);
                                SCuC_flag_is_device_connected = true;  //Arduino is succesfully connected
                                Log.d("D_SCuC", "[private]SCuC Device ready to use");
                                SCuC_status_var=0;  //0-->  device connnected Succesfully
                                SCuC_flag_is_device_connected=true;// device connnected Succesfully
                               //Device connected succesfully
                            }
                            else
                            {
                                Log.d("D_SCuC", "[private]SCuC extra usb permission granted , Port Not open");
                                SCuC_status_var=-7;  //-7-->  Port not open
                            }
                        }
                        else
                        {
                            Log.d("D_SCuC", "[private]SCuC extra usb permission granted , Port is NUll");
                            SCuC_status_var=-6;  //-6-->  Port is null
                        }
                    }
                    else
                    {
                        Log.d("D_SCuC", "[private]SCuC extra usb permission not granted ");
                        SCuC_status_var=-5;  //-5-->  Permission not granted
                    }
                }
                else if (intent.getAction().equals(SCuC_usbManager.ACTION_USB_DEVICE_ATTACHED))
                {
                    Log.d("D_SCuC", "[private]SCuC Device atached ");
                    //USB device attached
                    if(!SCuC_flag_is_device_connected &&  SCuC_initialise_on_device_attach) //Only user sets the flag and device is not already conected
                    {
                        SCuC_connect_usb();
                    }


                }
                else if (intent.getAction().equals(SCuC_usbManager.ACTION_USB_DEVICE_DETACHED))
                {
                    Log.d("D_SCuC", "[private]SCuC Device detached ");
                    //USB Device detached
                    if(SCuC_flag_is_device_connected) {   //Double check the expected device detachment
                        if (SCuC_is_device_disconnected()) {
                            SCuC_disconnect_usb();
                        }
                    }

                }
            }
            catch (Exception e)
            {
                Log.d("D_SCuC", "[private]SCuC Error in usb connection ",e);
                SCuC_status_var=-4;  //-4-->  error in opening port
            }
        }
    };

    // method to establish the connection between the app and Device
    private void SCuC_connect_usb()
    {
        Log.d("D_SCuC", "[private]SCuC COnnect_usb ");
        HashMap<String, UsbDevice> usbDevices = SCuC_usbManager.getDeviceList();  //Get the device lists
        if (!usbDevices.isEmpty())   //If there are device available
        {
            boolean keep = true;
            for (Map.Entry<String, UsbDevice> entry : usbDevices.entrySet())  //Loop throgh all the devices
            {
                SCuC_usbDevice = entry.getValue();   //Get thhe device
                int PID = SCuC_usbDevice.getProductId();  //Get the product id
                int VID = SCuC_usbDevice.getVendorId();  //Get the vendor id
                Log.d("VID",Integer.toString(VID));

                if (PID == SCuC_expected_device_product_id && VID==SCuC_expected_device_vendor_id)//Check with expected device id
                {
                    Log.d("D_SCuC", "[private]SCuC found expected device ");
                    SCuC_status_var=-8;   //-8 --> expected device found

                    PendingIntent pi = PendingIntent.getBroadcast(SCuC_context, 0, new Intent(SCuC_ask_permission), 0); //Ask the permission
                    SCuC_usbManager.requestPermission(SCuC_usbDevice, pi); //Ask permission

                    keep = false;
                }
                else
                {
                    Log.d("D_SCuC", "[private]SCuC could not find expected device ");
                    SCuC_status_var=-9;   //-9 --> no expected device found
                    SCuC_connection = null;    //Null the eobjects
                    SCuC_usbDevice = null;
                }

                if (!keep)
                {
                    break;
                }

            }
        }
        else
        {
            Log.d("D_SCuC", "[private]SCuC No devices are connected ");
            SCuC_status_var=-10;   //-10 --> No devices connected
        }
    }

    //Usb call back to recieve data
     private UsbSerialInterface.UsbReadCallback mCallback = new UsbSerialInterface.UsbReadCallback() { //Defining a Callback which triggers whenever data is read.

        //This function receives data from the arduino, combine the bytes into a string.
        public void onReceivedData(byte[] arg0) {   //On the event of data recieved by the device


            try 
            {
                if(!SCuC_flag_buffer_full)            //check buffer ,If masssage buffer is full do nothing
                {
                    String data = null;    //To tempararily hold character
                    data = new String(arg0, "UTF-8");   //Convert byte to string

                    SCuC_temp_make_string = SCuC_temp_make_string + data;  //Build string from the characters

                    if (SCuC_temp_make_string.contains(SCuC_massage_break_character))  //Check for massage break character
                    {
                        SCuC_inmsgs_count++;               //Increment the buffer index location //Start stroring from index 1

                        SCuC_msgs_in[SCuC_inmsgs_count] = SCuC_temp_make_string;//Pass tempararily stored string to buffer
                        SCuC_temp_make_string = null;
                        if (SCuC_inmsgs_count == SCuC_in_msgs_buffer_size - 1)
                        {
                            SCuC_flag_buffer_full=true; //Buffer full
                        }
                    }
                }

            } catch (UnsupportedEncodingException e) 
            {
                Log.d("D_SCuC", "unspported format byte recieved",e);
            }

        }
    };

    public String SCuC_read_data_buffer()  //Read the massages in last in first out fashion
    {
        String temp_for_ret = null;  //To return the string value
        if(SCuC_inmsgs_count>0)  //msg counter is greter than 0 theen only read the array
        {
            temp_for_ret = SCuC_msgs_in[SCuC_inmsgs_count];  //Pass the buffer value to temp var
            SCuC_msgs_in[SCuC_inmsgs_count] = null; //Clear the buffer

            SCuC_flag_buffer_full = false; //As one data has with drawn from buffer
        }
        if(SCuC_inmsgs_count>0)  //iF MSG_COUNTer is 0, dont decrement further
        {
            SCuC_inmsgs_count--;  //Decrement the buffer index
        }
        return temp_for_ret; //Return the last in fist out data
    }

    public int SCuC_get_msg_count_in_buffer()
    {
        return SCuC_inmsgs_count;
    }


    public void SCuC_transmit_data(final String data) {
        new Thread(new Runnable()   //New thread to transmit the data to the device
        {
            @Override
            public void run() {
                try {
                    if (SCuC_serialPort != null && SCuC_flag_is_device_connected)   //Check if serial port is null
                    {
                        SCuC_serialPort.write(data.getBytes());
                        Log.d("D_SCuC", "data transimmtted");
                    } else                          //If serial port is null
                    {
                        Log.d("D_SCuC", "data not sent , No connection avalaible");
                    }
                } catch (Exception e) {
                    Log.d("D_SCuC", "Exception in serial port", e);
                }

            }

        }).start();
    }

    private boolean SCuC_is_device_disconnected()   //To check the expected device disconnected ??? returns true if so
    {
        boolean temp_device_detached=true;
        HashMap<String, UsbDevice> usbDevices = SCuC_usbManager.getDeviceList();  //Get the device lists
        if (!usbDevices.isEmpty())   //If there are device available
        {
            for (Map.Entry<String, UsbDevice> entry : usbDevices.entrySet())  //Loop throgh all the devices
            {
                UsbDevice temp_usb_device = entry.getValue();   //Get the device
                int PID = temp_usb_device.getProductId();  //Get the product id
                int VID = temp_usb_device.getVendorId();  //Get the vendor id
                if (PID == SCuC_expected_device_product_id && VID == SCuC_expected_device_vendor_id)//Check with expected device id
                {
                    temp_device_detached=false;
                }
            }
        }
        return temp_device_detached;
    }


    private void SCuC_disconnect_usb()
    {
        SCuC_usbManager=null;
        SCuC_usbDevice=null;
        SCuC_serialPort=null;
        SCuC_connection=null;
        SCuC_flag_is_device_connected=false;
    }

    private void SCuC_init_array()
    {
        for(int temp=0; temp<SCuC_in_msgs_buffer_size; temp++)  //Add null character to all locations of input massage buffer
        {
            SCuC_msgs_in[temp]="";
        }
    }




}


